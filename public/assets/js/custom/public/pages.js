document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $.validator.addMethod("alphanumericdashesunderscore", function (value, element) {
            return this.optional(element) || /^[a-z0-9\-\_]+$/i.test(value);
        }, "Must contain only letters, numbers, dashes, or underscore.");
        $("#page_form").validate({
            rules: {
                pageTitle: {
                    maxlength: 255,
                    required: true
                },
                pageUrl: {
                    alphanumericdashesunderscore: true,
                    maxlength: 255,
                    required: true
                },
                pageBannerImage: {
                    maxlength: 255,
                    required: true
                },

            },
            submitHandler: function (form) {
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}

function upload_profile_image(elem) {
    // $(elem).val('');
    if ($("#imageForm input[type=file]").val()) {

        $('#imageForm').ajaxSubmit({
            success: function (response) {
                response = jQuery.parseJSON(response);
                $("#page_form").find('.no-img').html("<img src='" + mediaPath + response.message + "' />");
                $("#page_form").find('input[name=pageBannerImage]').val(response.message);
                ajax_end_function();
                $("#imageForm input[type=file]").val('');
            },
            beforeSubmit: function () {
                ajax_start_function('#page_form');
            },
            error: function (response) {
                show_request_failed_alert('#page_form');
                ajax_end_function();
                $("#imageForm input[type=file]").val('');
            }
        });
    }

}
function open_banner_image() {
    $("#imageForm input[type=file]").trigger('click');
}
