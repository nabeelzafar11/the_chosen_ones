document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $(".slide-class-4").slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000
        });
        $(".slide-class-4-nodot").slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 1500
        });
        $(".slide-class-3").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000
        });
        $("#newsletter_form-1").validate({
            rules: {
                subscriberEmail: {
                    required: true,
                    email: true
                },
                subscriberName: {
                    required: true
                },
                subscriberPostcode: {
                    required: true
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
};

function upload_profile_image(elem) {
    // $(elem).val('');
    if ($("#imageForm input[type=file]").val()) {
        $("#imageForm").ajaxSubmit({
            success: function (response) {
                response = jQuery.parseJSON(response);
                imageElement
                        .find(".no-img")
                        .html("<img src='" + mediaPath + response.message + "' />");
                imageElement.find("input[type=hidden]").val(response.message);
                ajax_end_function();
                $("#imageForm input[type=file]").val("");
            },
            beforeSubmit: function () {
                ajax_start_function(imageElement.closest("form").attr("id"));
            },
            error: function (response) {
                show_request_failed_alert(imageElement.closest("form").attr("id"));
                ajax_end_function();
                $("#imageForm input[type=file]").val("");
            }
        });
    }
}

var imageElement;

function open_banner_image(elem) {
    imageElement = $(elem).closest(".edit-profile-photo");
    $("#imageForm input[type=file]").trigger("click");
}
