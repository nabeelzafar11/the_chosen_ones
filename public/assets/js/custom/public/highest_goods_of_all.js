var statusSubmit = true;
document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#goodList").on("click", ".goodListItem", function () {
            var goodId = $(this).data("id");
            $("#id").val(goodId);
        });
    }

    $("#modal-add-item-button").on("click", function () {
        var comment = $.trim($("#comment").val());
        if (comment.length > 0) {
            if (statusSubmit) {
                statusSubmit = false;
                $("#myLoader").show();
                $.ajax({
                    type: "POST",
                    url: base_url + "api/user/add-custom-highestgoodofall-item",
                    data: {message: comment},
                    success: function (response) {
                        statusSubmit = true;
                        var resp = JSON.parse(response);
                        $("#alert").html(resp.message);
                        if (resp.status == "success") {
                            recordsCount = recordsCount + 1;
                            highest_goods_of_all_item_id = resp.id;
                            $("#alert")
                                    .removeClass("alert-danger")
                                    .removeClass("alert-success")
                                    .addClass("alert-success")
                                    .show();
                            $("#remove-001").remove();
                            $("#goodList").append(`
								<li style="text-align: start;" id="remove-${highest_goods_of_all_item_id}">
									<a href="#" class="goodListItem" data-id="${highest_goods_of_all_item_id}" data-message="${comment}" data-toggle="modal" data-target="#itemModel">
										<div class="notes not2" style="height: auto;display: flex;color:black">&#x2B24; &nbsp;${comment}</div>
									</a>
								</li>`);
                        } else {
                            $("#alert")
                                    .removeClass("alert-danger")
                                    .removeClass("alert-success")
                                    .addClass("alert-danger")
                                    .show();
                        }
                        $("#exampleModalScrollable").modal("hide");
                        $("#myLoader").hide();
                    }
                });
            }
        } else {
            alert("Please write some message.");
        }
    });
};

function delete_list_item() {
    var goodId = $.trim($("#id").val());
    if (goodId) {
        $("#myLoader").show();
        $.ajax({
            type: "POST",
            url: base_url + "api/user/delete_user_highestgoodofall_list",
            data: {id: goodId},
            success: function (response) {
                var resp = JSON.parse(response);
                $("#alert").html(resp.message);
                if (resp.status == "success") {
                    recordsCount = recordsCount - 1;
                    if (recordsCount == 0) {
                        $("#goodList").append(`
								<li style="text-align: start;" id="remove-001">
									<a href="#" class="goodListItem">
										<div class="notes not2" style="height: auto;display: flex;color:black">&#x2B24; &nbsp; Value all life equally</div>
									</a>
								</li>`);
                    }
                    $("#alert")
                            .removeClass("alert-danger")
                            .removeClass("alert-success")
                            .addClass("alert-success")
                            .show();
                } else {
                    $("#alert")
                            .removeClass("alert-danger")
                            .removeClass("alert-success")
                            .addClass("alert-danger")
                            .show();
                }
                $("#remove-" + goodId).remove();
                $("#itemModel").modal("hide");
                $("#myLoader").hide();
            }
        });
    }
}
