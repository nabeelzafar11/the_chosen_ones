document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#testimonial_form").validate({
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}

function trigger_is_published(elem, id) {
    if($(elem).is(":checked")){
        $("#testimonial_form input[name=testimonialIsPublished]").val(1);
    }else{
        $("#testimonial_form input[name=testimonialIsPublished]").val(0);
    }
    $("#testimonial_form input[name=testimonialId]").val(id);
    $("#testimonial_form").submit();
}

