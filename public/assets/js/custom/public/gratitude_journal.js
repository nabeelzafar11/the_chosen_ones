var internalHtml = "";
var data_id = 0;
var event_id = 0;
var dateandtime = 0;
var eventId = 0;
document.onreadystatechange = function() {
	if (document.readyState == "complete") {
		$("#edit-icon-h1").on("click", function() {
			$("#h1_date").val(h1_date);
			$("#h1_heading").val(h1_title);
			$("#h1_message").val(h1_message);
		});

		$("#edit-icon-h2").on("click", function() {
			$("#h2_date").val(h2_date);
			$("#h2_heading").val(h2_title);
			$("#h2_message").val(h2_message);
		});

		$("#edit-icon-h3").on("click", function() {
			$("#h3_date").val(h3_date);
			$("#h3_heading").val(h3_title);
			$("#h3_message").val(h3_message);
		});

		$("#modal-1-submit-button").on("click", function() {
			var new_h1_date = $.trim($("#h1_date").val());
			var new_h1_heading = $.trim($("#h1_heading").val());
			var new_h1_message = $.trim($("#h1_message").val());

			if (new_h1_date.length > 0 && new_h1_heading.length > 0) {
				$("#myLoader").show();
				$.ajax({
					type: "POST",
					url: base_url + "api/user/add-custom-gratitudejournal-item",
					data: {
						h1_title: new_h1_heading,
						h1_message: new_h1_message,
						h1_date: new_h1_date,

						h2_title: h2_title,
						h2_message: h2_message,
						h2_date: h2_date,
						h3_title: h3_title,
						h3_message: h3_message,
						h3_date: h3_date
					},
					success: function(response) {
						var resp = JSON.parse(response);
						$("#alert").html(resp.message);
						if (resp.status == "success") {
							h1_date = new_h1_date;
							h1_title = new_h1_heading;
							h1_message = new_h1_message;

							$("#feild_h1_heading").html(
								`<span class='gratitude_h'>${new_h1_heading}</span><br />${new_h1_message}`
							);
							var date = new Date(new_h1_date);
							result =
								(date.getMonth() > 8
									? date.getMonth() + 1
									: "0" + (date.getMonth() + 1)) +
								"/" +
								(date.getDate() > 9 ? date.getDate() : "0" + date.getDate()) +
								"/" +
								date.getFullYear();
							$(".feild_h1_date").val(result);

							$("#alert")
								.removeClass("alert-danger")
								.removeClass("alert-success")
								.addClass("alert-success")
								.show();
						} else {
							$("#alert")
								.removeClass("alert-danger")
								.removeClass("alert-success")
								.addClass("alert-danger")
								.show();
						}
						$("#h1modal").modal("hide");
						$("#myLoader").hide();
					}
				});
			} else {
				alert("Please fill all fields.");
			}
		});

		$("#modal-2-submit-button").on("click", function() {
			var new_h2_date = $.trim($("#h2_date").val());
			var new_h2_heading = $.trim($("#h2_heading").val());
			var new_h2_message = $.trim($("#h2_message").val());

			if (new_h2_date.length > 0 && new_h2_heading.length > 0) {
				$("#myLoader").show();
				$.ajax({
					type: "POST",
					url: base_url + "api/user/add-custom-gratitudejournal-item",
					data: {
						h2_title: new_h2_heading,
						h2_message: new_h2_message,
						h2_date: new_h2_date,

						h1_title: h1_title,
						h1_message: h1_message,
						h1_date: h1_date,
						h3_title: h3_title,
						h3_message: h3_message,
						h3_date: h3_date
					},
					success: function(response) {
						var resp = JSON.parse(response);
						$("#alert").html(resp.message);
						if (resp.status == "success") {
							h2_date = new_h2_date;
							h2_title = new_h2_heading;
							h2_message = new_h2_message;

							$("#feild_h2_heading").html(
								`<span class='gratitude_h'>${new_h2_heading}</span><br />${new_h2_message}`
							);
							var date = new Date(new_h2_date);
							result =
								(date.getMonth() > 8
									? date.getMonth() + 1
									: "0" + (date.getMonth() + 1)) +
								"/" +
								(date.getDate() > 9 ? date.getDate() : "0" + date.getDate()) +
								"/" +
								date.getFullYear();
							$(".feild_h2_date").val(result);
							$("#alert")
								.removeClass("alert-danger")
								.removeClass("alert-success")
								.addClass("alert-success")
								.show();
						} else {
							$("#alert")
								.removeClass("alert-danger")
								.removeClass("alert-success")
								.addClass("alert-danger")
								.show();
						}
						$("#h2modal").modal("hide");
						$("#myLoader").hide();
					}
				});
			} else {
				alert("Please fill all fields.");
			}
		});

		$("#modal-3-submit-button").on("click", function() {
			var new_h3_date = $.trim($("#h3_date").val());
			var new_h3_heading = $.trim($("#h3_heading").val());
			var new_h3_message = $.trim($("#h3_message").val());

			if (new_h3_date.length > 0 && new_h3_heading.length > 0) {
				$("#myLoader").show();
				$.ajax({
					type: "POST",
					url: base_url + "api/user/add-custom-gratitudejournal-item",
					data: {
						h3_title: new_h3_heading,
						h3_message: new_h3_message,
						h3_date: new_h3_date,

						h1_title: h1_title,
						h1_message: h1_message,
						h1_date: h1_date,
						h2_title: h2_title,
						h2_message: h2_message,
						h2_date: h2_date
					},
					success: function(response) {
						var resp = JSON.parse(response);
						$("#alert").html(resp.message);
						if (resp.status == "success") {
							h3_date = new_h3_date;
							h3_title = new_h3_heading;
							h3_message = new_h3_message;
							$("#feild_h3_heading").html(
								`<span class='gratitude_h'>${new_h3_heading}</span><br />${new_h3_message}`
							);

							var date = new Date(new_h3_date);
							result =
								(date.getMonth() > 8
									? date.getMonth() + 1
									: "0" + (date.getMonth() + 1)) +
								"/" +
								(date.getDate() > 9 ? date.getDate() : "0" + date.getDate()) +
								"/" +
								date.getFullYear();
							$(".feild_h3_date").val(result);

							$("#alert")
								.removeClass("alert-danger")
								.removeClass("alert-success")
								.addClass("alert-success")
								.show();
						} else {
							$("#alert")
								.removeClass("alert-danger")
								.removeClass("alert-success")
								.addClass("alert-danger")
								.show();
						}
						$("#h3modal").modal("hide");
						$("#myLoader").hide();
					}
				});
			} else {
				alert("Please fill all fields.");
			}
		});

		$(".datepicker").datepicker({
			prevText: '<i class="fa fa-fw fa-angle-left"></i>',
			nextText: '<i class="fa fa-fw fa-angle-right"></i>',
			dateFormat: "mm-dd-yy",
			defaultDate: new Date()
		});
	}
};
