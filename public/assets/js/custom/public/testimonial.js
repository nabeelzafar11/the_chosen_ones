document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#testimonial_form").validate({
            rules: {
                testimonialName: {
                    required:true
                },
                testimonialComment: {
                    required:true
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}

function upload_profile_image(elem) {
    // $(elem).val('');
    if ($("#imageForm input[type=file]").val()) {

        $('#imageForm').ajaxSubmit({
            success: function (response) {
                response = jQuery.parseJSON(response);
                $("#testimonial_form").find('img').attr('src', mediaPath + response.message);
                $("#testimonial_form").find('input[name=testimonialImage]').val(response.message);
                ajax_end_function();
                $("#imageForm input[type=file]").val('');
            },
            beforeSubmit: function () {
                ajax_start_function('#testimonial_form');
            },
            error: function (response) {
                show_request_failed_alert('#testimonial_form');
                ajax_end_function();
                $("#imageForm input[type=file]").val('');
            }
        });
    }

}
function open_testimonial_image() {
    $("#imageForm input[type=file]").trigger('click');
}
