document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#popular_studio_form").validate({
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
        $("#most_visited_studio_form").validate({
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}

function trigger_popular_studio(elem, id) {
    if($(elem).is(":checked")){
        $("#popular_studio_form input[name=studioIsPopular]").val(1);
    }else{
        $("#popular_studio_form input[name=studioIsPopular]").val(0);
    }
    $("#popular_studio_form input[name=studioId]").val(id);
    $("#popular_studio_form").submit();
}

function trigger_most_visited_studio(elem, id) {
    if($(elem).is(":checked")){
        $("#most_visited_studio_form input[name=studioIsMostVisited]").val(1);
    }else{
        $("#most_visited_studio_form input[name=studioIsMostVisited]").val(0);
    }
    $("#most_visited_studio_form input[name=studioId]").val(id);
    $("#most_visited_studio_form").submit();
}
