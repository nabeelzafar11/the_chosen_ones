var internalHtml = "";
var data_id = 0;
var event_id = 0;
var dateandtime = 0;
var eventId = 0;
document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        var d = new Date();
        call_events_function(d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate());
        $(".eventsfordate").on("click", ".editcalendaritem", function (e) {
            e.preventDefault();
            data_id = $(this).data("id");
            event_id = $(this).data("eventid");
            dateandtime = $(this).data("dateandtime");
            eventId = $(this).data("eventid");
            $("#dateandtime").val(dateandtime);
            $("#eventId").val(eventId);
        });

        $("#deleteallfutureeventsButton").on("click", function () {
            $("#delete_all_future_events").submit();
        });

        $("#deletethisevent").on("click", function () {
            $("#delete_event").submit();
        });

        $("#delete_event").validate({
            rules: {
                eventId: {
                    required: true
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        $(".remove-" + eventId).remove();
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });


        $("#delete_all_future_events").validate({
            rules: {
                dateandttime: {
                    required: true
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                        location.reload();
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });

        $(".canceltimemodal").click(function () {
            $("#timemodal").modal('hide');
        });
    }
    $(".datepicker").datepicker({

        prevText: '<i class="fa fa-fw fa-angle-left"></i>',
        nextText: '<i class="fa fa-fw fa-angle-right"></i>',
        dateFormat: "yy-mm-dd",
        dateFormat: 'yy-mm-dd',
        beforeShowDay: function (d) {
            var dmy = d.getFullYear() + "-";
            if (d.getMonth() < 9)
                dmy += "0" + (d.getMonth() + 1);
            else
                dmy += (d.getMonth() + 1);

            dmy += "-";

            if (d.getDate() < 10)
                dmy += "0";
            dmy += d.getDate();

            if ($.inArray(dmy, availableDates) != -1) {
                return [true, "chosen-dates", dmy];
            } else {
                return [true, "", ""];
            }
        },
        onSelect: function (dateText) {
            call_events_function(dateText);
        }
    });

    function call_events_function(dateText) {
        $.ajax({
            type: "post",
            url: base_url + "api/user/fetch-current-date-events",
            data: {date: dateText},
            success: function (response) {
                var resp = JSON.parse(response);
                if (resp.status == "success") {
                    respArray = resp.message;
                    console.log(respArray);
                    internalHtml = "";
                    for (var i = 0; i < respArray.length; i++) {
                        var timeString = respArray[i].time;
                        var H = +timeString.substr(0, 2);
                        var h = H % 12 || 12;
                        var ampm = (H < 12 || H === 24) ? "AM" : "PM";
                        timeString = h + timeString.substr(2, 3) + ampm;
                        internalHtml += "<li class='remove-" + respArray[i].eventId + "'><span class=' mr-5'>" + timeString + "</span><span class='  text-clr ff-bold'>" + respArray[i].message + "</span><span class='float-right'><a href='#' class='editcalendaritem ff-bold text-dark' data-toggle='modal' data-target='#exampleModalScrollable' data-eventid='" + respArray[i].eventId + "' data-dateandtime='" + respArray[i].dateandtime + "' data-id='" + respArray[i].id + "' data-eventid='" + respArray[i].eventId + "'>edit</a></span></li>";
                    }
                    $(".eventsfordate").html(internalHtml);
                }
            }
        });
    }
}
