document.onreadystatechange = function() {
	if (document.readyState == "complete") {
		$("#abundanceList").on("click", ".abundanceListItem", function() {
			var abundanceId = $(this).data("id");
			$("#abundanceID").val(abundanceId);
		});
		$("#abundanceNewList").on("click", ".abundanceNewListItem", function() {
			var newAbundanceItemId = $(this).data("id");
			var newAbundanceItemMessage = $(this).data("message");
			if (newAbundanceItemId) {
				$("#myLoader").show();
				$.ajax({
					type: "POST",
					url: base_url + "api/user/add-new-healthandbeauty-item",
					data: { id: newAbundanceItemId },
					success: function(response) {
						var resp = JSON.parse(response);
						$("#alert").html(resp.message);
						if (resp.status == "success") {
							new_abundance_list_item_id = resp.id;
							$("#alert")
								.removeClass("alert-danger")
								.removeClass("alert-success")
								.addClass("alert-success")
								.show();
							$("#remove-newlist" + newAbundanceItemId).remove();
							$("#abundanceList").append(`
							<li class="bg-modle-li-1" id="remove-list${new_abundance_list_item_id}">
								<a href="#" data-id="${new_abundance_list_item_id}" data-toggle="modal" data-target="#popup" class="text-dark abundanceListItem">
									${newAbundanceItemMessage}
									<span class="float-right "><img src="${ASSETS_PATH}newtheme/img/Remove Circle.png" class="img-fluid  "   alt=""></span>
								</a>
							</li>`);
						} else {
							$("#alert")
								.removeClass("alert-danger")
								.removeClass("alert-success")
								.addClass("alert-danger")
								.show();
						}
						$("#Abundance1").modal("hide");
						$("#myLoader").hide();
					}
				});
			}
		});
	}
};

function delete_abundance_item() {
	var abundanceId = $.trim($("#abundanceID").val());
	if (abundanceId) {
		$("#myLoader").show();
		$.ajax({
			type: "POST",
			url: base_url + "api/user/delete_user_healthandbeauty_list",
			data: { id: abundanceId },
			success: function(response) {
				var resp = JSON.parse(response);
				if (resp.status == "success") {
					window.location.reload();
				} else {
					$("#alert").html(resp.message);
					$("#alert")
						.removeClass("alert-danger")
						.removeClass("alert-success")
						.addClass("alert-danger")
						.show();
				}
				$("#popup").modal("hide");
				$("#myLoader").hide();
			}
		});
	}
}

function add_abundance_item() {
	var abundanceNewItem = $.trim($("#pwd").val());
	if (abundanceNewItem) {
		$("#myLoader").show();
		$.ajax({
			type: "POST",
			url: base_url + "api/user/add-custom-healthandbeauty-item",
			data: { message: abundanceNewItem },
			success: function(response) {
				var resp = JSON.parse(response);
				$("#alert").html(resp.message);
				if (resp.status == "success") {
					new_abundance_list_item_id = resp.id;
					$("#alert")
						.removeClass("alert-danger")
						.removeClass("alert-success")
						.addClass("alert-success")
						.show();
					$("#abundanceList").append(`
					<li class="bg-modle-li-1" id="remove-list${new_abundance_list_item_id}">
						<a href="#" data-id="${new_abundance_list_item_id}" data-toggle="modal" data-target="#popup" class="text-dark abundanceListItem">
							${abundanceNewItem}
							<span class="float-right "><img src="${ASSETS_PATH}newtheme/img/Remove Circle.png" class="img-fluid  "   alt=""></span>
						</a>
					</li>`);
				} else {
					$("#alert")
						.removeClass("alert-danger")
						.removeClass("alert-success")
						.addClass("alert-danger")
						.show();
				}
				$("#custom").modal("hide");
				$("#Abundance1").modal("hide");
				$("#myLoader").hide();
			}
		});
	}
}
