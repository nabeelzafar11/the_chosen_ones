var selectedColor = "";
var selectedType = "";
var selectedSize = "";
var selectedFit = "";
$(document).ready(function() {
	setTimeout(function() {
		$(".alert").fadeOut("slow");
	}, 3000);

	$("#newsletter_form").validate({
		rules: {
			subscriberEmail: {
				required: true,
				email: true
			},
			subscriberName: {
				required: true
			},
			subscriberPostcode: {
				required: true
			}
		},
		submitHandler: function(form) {
			$(form).ajaxSubmit({
				success: function(response) {
					ajax_success_function(form, response);
				},
				beforeSubmit: function() {
					ajax_start_function(form);
				},
				error: function(response) {
					show_request_failed_alert(form);
					ajax_end_function();
				}
			});
		}
	});

	$("#email_verification_form").validate({
		rules: {
			token: {
				required: true
			}
		},
		submitHandler: function(form) {
			$(form).ajaxSubmit({
				success: function(response) {
					ajax_success_function(form, response);
				},
				beforeSubmit: function() {
					ajax_start_function(form);
				},
				error: function(response) {
					show_request_failed_alert(form);
					ajax_end_function();
				}
			});
		}
	});

	$("#news_letter_form").validate({
		rules: {
			newsletterEmail: {
				required: true,
				email: true
			}
		},
		submitHandler: function(form) {
			$(form).ajaxSubmit({
				success: function(response) {
					ajax_success_function(form, response);
				},
				beforeSubmit: function() {
					ajax_start_function(form);
				},
				error: function(response) {
					show_request_failed_alert(form);
					ajax_end_function();
				}
			});
		}
	});
	$("#studio_table").on("draw.dt", function() {
		$("#studio_table_paginate").addClass("pull-right");
		$("#studio_table_previous").html(
			'<a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>'
		);
		$("#studio_table_next").html(
			'<a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>'
		);
	});
	$(".filter-box input").on("keyup click", function() {
		$("#studio_table")
			.DataTable()
			.search($(".filter-box input").val())
			.draw();
	});
	$("#studio_table").DataTable();
	$("body").on("click", function(e) {
		if (
			!$(".admin-menu").is(e.target) &&
			$(".admin-menu").has(e.target).length === 0
		) {
			$(".admin-menu").removeClass("active");
		}
	});
});

function resend_verification_email(token) {
	$("#token").val(token);
	$("#email_verification_form").submit();
}

function ajax_start_function(form) {
	if (form) {
		$(form)
			.find(".alert")
			.remove();
	}
	$(':input[type="submit"]').attr("disabled", true);
	$("#myLoader").show();
}

function ajax_end_function() {
	$("#myLoader").hide("hide");
	$(':input[type="submit"]').removeAttr("disabled");
}

function ajax_success_function(form, response) {
	response = jQuery.parseJSON(response);
	if (response.status == "refresh") {
		location.reload();
	} else {
		show_alert(form, response.status, response.message);
		ajax_end_function();
	}
}

function show_request_failed_alert(form) {
	show_alert(form, "danger", "Request Failed (Internet Problem).");
}

function show_alert(form, status, message) {
	$(".alert").remove();
	$(form)
		.find(".alert")
		.remove();
	var alert =
		'<div class="alert alert-' +
		status +
		' alert-dismissible m-2" role="alert">' +
		message +
		"</div>";
	$(form).prepend(alert);
	if (
		$(form).attr("id") !== "newsletter_form-1" &&
		$(form).attr("id") !== "news_letter_form" &&
		$(form).attr("id") !== "review_form" &&
		$(form).attr("id") !== "book_studio_form" &&
		$(form).attr("id") !== "contact_form" &&
		$(form).attr("id") !== "popular_studio_form" &&
		$(form).attr("id") !== "most_visited_studio_form"
	) {
		$("html, body, .modal").animate({ scrollTop: 0 }, "slow");
	}
}

function add_to_wishlist(elem, slug) {
	ajax_start_function("");
	$.post(apiPath + "api/add_to_wishlist", { productSlug: slug }, function(
		response
	) {
		response = jQuery.parseJSON(response);
		if (response.status && response.status == "success") {
			$(".productDiv[data-product=" + slug + "] .addToWishlistLink").addClass(
				"hide"
			);
			$(
				".productDiv[data-product=" + slug + "] .removeFromWishlistLink"
			).removeClass("hide");
		}
	})
		.done(function() {
			ajax_end_function();
		})
		.fail(function() {
			ajax_end_function();
		})
		.always(function() {
			ajax_end_function();
		});
}

function remove_from_wishlist(elem, slug) {
	ajax_start_function("");
	$.post(apiPath + "api/remove_from_wishlist", { productSlug: slug }, function(
		response
	) {
		response = jQuery.parseJSON(response);
		if (response.status && response.status == "success") {
			$(
				".productDiv[data-product=" + slug + "] .addToWishlistLink"
			).removeClass("hide");
			console.log(".productDiv[data-product=" + slug + "] .addToWishlistLink");
			$(
				".productDiv[data-product=" + slug + "] .removeFromWishlistLink"
			).addClass("hide");
		}
	})
		.done(function() {
			ajax_end_function();
		})
		.fail(function() {
			ajax_end_function();
		})
		.always(function() {
			ajax_end_function();
		});
}

function delete_item(elem, id, url) {
	var result = confirm("Do you really want to delete?");
	if (result) {
		$("#delete_form input[name=id]").val(id);
		$("#delete_form").attr("action", url);
		$("#delete_form").ajaxSubmit({
			success: function(response) {
				ajax_success_function("#alert_div", response);
				response = jQuery.parseJSON(response);
				if (response.status == "success") {
					$(elem)
						.closest("li")
						.remove();
				}
			},
			beforeSubmit: function() {
				ajax_start_function("#alert_div");
			},
			error: function(response) {
				show_request_failed_alert("#alert_div");
				ajax_end_function();
			}
		});
	}
}

function trigger_is_published(elem, id) {
	if ($(elem).is(":checked")) {
		$("#testimonial_form input[name=testimonialIsPublished]").val(1);
	} else {
		$("#testimonial_form input[name=testimonialIsPublished]").val(0);
	}
	$("#testimonial_form input[name=testimonialId]").val(id);
	$("#testimonial_form").submit();
}

function trigger_is_active(elem, id, url) {
	if ($(elem).is(":checked")) {
		isActive = 1;
	} else {
		isActive = 0;
	}
	var result = confirm("Do you really want to change status?");
	if (result) {
		$("#trigger_is_active_form input[name=id]").val(id);
		$("#trigger_is_active_form input[name=isActive]").val(isActive);
		$("#trigger_is_active_form").attr("action", url);
		$("#trigger_is_active_form").ajaxSubmit({
			success: function(response) {
				ajax_success_function("#alert_div", response);
				response = jQuery.parseJSON(response);
			},
			beforeSubmit: function() {
				ajax_start_function("#alert_div");
			},
			error: function(response) {
				show_request_failed_alert("#alert_div");
				ajax_end_function();
			}
		});
	} else {
		if (isActive) {
			$(elem).prop("checked", false);
		} else {
			$(elem).prop("checked", true);
		}
	}
}

function delete_studio(id, url) {
	var result = confirm("Do you really want to delete?");
	if (result) {
		$("#delete_form input[name=id]").val(id);
		$("#delete_form").attr("action", url);
		$("#delete_form").ajaxSubmit({
			success: function(response) {
				ajax_success_function("#alert_div", response);
				response = jQuery.parseJSON(response);
				if (response.status == "success") {
					$(elem)
						.closest("li")
						.remove();
				}
			},
			beforeSubmit: function() {
				ajax_start_function("#alert_div");
			},
			error: function(response) {
				show_request_failed_alert("#alert_div");
				ajax_end_function();
			}
		});
	}
}

function approved_studio(id, url) {
	var result = confirm("Do you really want to approve?");
	if (result) {
		$("#delete_form input[name=id]").val(id);
		$("#delete_form").attr("action", url);
		$("#delete_form").ajaxSubmit({
			success: function(response) {
				ajax_success_function("#alert_div", response);
				response = jQuery.parseJSON(response);
				if (response.status == "success") {
					$(elem)
						.closest("li")
						.remove();
				}
			},
			beforeSubmit: function() {
				ajax_start_function("#alert_div");
			},
			error: function(response) {
				show_request_failed_alert("#alert_div");
				ajax_end_function();
			}
		});
	}
}

function available_studio(id, url) {
	var result = confirm("Do you really want to make available?");
	if (result) {
		$("#delete_form input[name=id]").val(id);
		$("#delete_form").attr("action", url);
		$("#delete_form").ajaxSubmit({
			success: function(response) {
				ajax_success_function("#alert_div", response);
				response = jQuery.parseJSON(response);
				if (response.status == "success") {
					$(elem)
						.closest("li")
						.remove();
				}
			},
			beforeSubmit: function() {
				ajax_start_function("#alert_div");
			},
			error: function(response) {
				show_request_failed_alert("#alert_div");
				ajax_end_function();
			}
		});
	}
}

function unavailable_studio(id, url) {
	var result = confirm("Do you really want to make unavailable?");
	if (result) {
		$("#delete_form input[name=id]").val(id);
		$("#delete_form").attr("action", url);
		$("#delete_form").ajaxSubmit({
			success: function(response) {
				ajax_success_function("#alert_div", response);
				response = jQuery.parseJSON(response);
				if (response.status == "success") {
					$(elem)
						.closest("li")
						.remove();
				}
			},
			beforeSubmit: function() {
				ajax_start_function("#alert_div");
			},
			error: function(response) {
				show_request_failed_alert("#alert_div");
				ajax_end_function();
			}
		});
	}
}

function not_approve_studio(id, url) {
	var result = confirm("Do you really want to not approve?");
	if (result) {
		$("#delete_form input[name=id]").val(id);
		$("#delete_form").attr("action", url);
		$("#delete_form").ajaxSubmit({
			success: function(response) {
				ajax_success_function("#alert_div", response);
				response = jQuery.parseJSON(response);
				if (response.status == "success") {
					$(elem)
						.closest("li")
						.remove();
				}
			},
			beforeSubmit: function() {
				ajax_start_function("#alert_div");
			},
			error: function(response) {
				show_request_failed_alert("#alert_div");
				ajax_end_function();
			}
		});
	}
}

function block_user(id, url) {
	var result = confirm("Do you really want to block?");
	if (result) {
		$("#delete_form input[name=id]").val(id);
		$("#delete_form").attr("action", url);
		$("#delete_form").ajaxSubmit({
			success: function(response) {
				ajax_success_function("#alert_div", response);
				response = jQuery.parseJSON(response);
				if (response.status == "success") {
					$(elem)
						.closest("li")
						.remove();
				}
			},
			beforeSubmit: function() {
				ajax_start_function("#alert_div");
			},
			error: function(response) {
				show_request_failed_alert("#alert_div");
				ajax_end_function();
			}
		});
	}
}

function unblock_user(id, url) {
	var result = confirm("Do you really want to unblock?");
	if (result) {
		$("#delete_form input[name=id]").val(id);
		$("#delete_form").attr("action", url);
		$("#delete_form").ajaxSubmit({
			success: function(response) {
				ajax_success_function("#alert_div", response);
				response = jQuery.parseJSON(response);
				if (response.status == "success") {
					$(elem)
						.closest("li")
						.remove();
				}
			},
			beforeSubmit: function() {
				ajax_start_function("#alert_div");
			},
			error: function(response) {
				show_request_failed_alert("#alert_div");
				ajax_end_function();
			}
		});
	}
}

function activate_user(id, url) {
	var result = confirm("Do you really want to activate?");
	if (result) {
		$("#delete_form input[name=id]").val(id);
		$("#delete_form").attr("action", url);
		$("#delete_form").ajaxSubmit({
			success: function(response) {
				ajax_success_function("#alert_div", response);
				response = jQuery.parseJSON(response);
				if (response.status == "success") {
					$(elem)
						.closest("li")
						.remove();
				}
			},
			beforeSubmit: function() {
				ajax_start_function("#alert_div");
			},
			error: function(response) {
				show_request_failed_alert("#alert_div");
				ajax_end_function();
			}
		});
	}
}

$(function() {
	if ($("#sortable").length > 0) {
		$("#sortable").sortable();
		// $("#sortable").disableSelection();
	}
});

$("#sortable").on("sortupdate", function(event, ui) {
	sort_items();
});

function sort_items() {
	var id = [];

	$("#sortable li").each(function() {
		id.push($(this).attr("data-id"));
	});
	ajax_start_function("#alert_div");
	var data = {
		id: id
	};
	var url = sortUrl;
	$.ajax({
		url: url,
		type: "POST",
		data: data,
		success: function(data) {
			ajax_end_function("#alert_div");
		}
	});
}

function search_studio(elem, category) {
	if (
		true ||
		$(elem)
			.closest("form")
			.find("input")
			.val()
	) {
		var element =
			'<input type="hidden" name="category[]" value="' + category + '"/>';
		$(elem)
			.closest("form")
			.prepend(element);
		$(elem)
			.closest("form")
			.submit();
	} else {
		$("#searchForm button[type=submit]").trigger("click");
		// $("#searchForm").valid();
	}
}

function update_filters(elem) {
	$(elem)
		.closest("form")
		.submit();
}

function bookmark_studio(elem, id) {
	ajax_start_function("#studio_form");
	var data = {
		id: id
	};
	$.post(apiPath + "api/bookmark_studio", data, function(data) {
		response = jQuery.parseJSON(data);
		if (response.status == "success") {
			$(elem)
				.closest(".like-listing")
				.find(".likedLink")
				.show();
			$(elem).hide();
		}
	})
		.done(function() {
			ajax_end_function();
		})
		.fail(function() {
			show_request_failed_alert("#studio_form");
		})
		.always(function() {
			ajax_end_function();
		});
}
function unmark_studio(elem, id) {
	ajax_start_function("#studio_form");
	var data = {
		id: id
	};
	$.post(apiPath + "api/unmark_studio", data, function(data) {
		response = jQuery.parseJSON(data);
		if (response.status == "success") {
			$(elem)
				.closest(".like-listing")
				.find(".unlikedLink")
				.show();
			$(elem).hide();
		}
	})
		.done(function() {
			ajax_end_function();
		})
		.fail(function() {
			show_request_failed_alert("#studio_form");
		})
		.always(function() {
			ajax_end_function();
		});
}
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$(input)
				.closest(".imageDiv")
				.find("img")
				.attr("src", e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	} else {
		$(input)
			.closest(".imageDiv")
			.find("img")
			.attr("src", defaultImage);
	}
}
