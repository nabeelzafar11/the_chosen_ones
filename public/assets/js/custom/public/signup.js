document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        
        $("#signup_form").validate({
            rules: {
                firstName: {
                    maxlength: 100,
                    required: true
                },
                lastName: {
                    maxlength: 100,
                    required: true
                },
                email: {
                    maxlength: 100,
                    email: true,
                    required: true
                },
                password: {
                    maxlength: 25,
                    required: true
                },
                confirmPassword: {
                    maxlength: 25,
                    required: true,
                    equalTo: "#password"
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}