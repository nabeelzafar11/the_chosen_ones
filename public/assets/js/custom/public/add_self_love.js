var id = 0;
var dates = new Array();
var dateandtimes = new Array();
var customEvents = false;
document.onreadystatechange = function() {
	if (document.readyState == "complete") {
		$(".canceltimemodal").click(function() {
			$("#timemodal").modal("hide");
		});
		$(".canceleventmodal").click(function() {
			dates = [];
			dateandtimes = [];
			$("#event").modal("hide");
		});
		$(".cancelcustom").click(function() {
			$("#custom").modal("hide");
		});

		$(".datepicker").multiDatesPicker({
			onSelect: function(dateText, inst) {
				addOrRemoveDate(dateText);
			},
			prevText: '<i class="fa fa-fw fa-angle-left"></i>',
			nextText: '<i class="fa fa-fw fa-angle-right"></i>',
			minDate: 0, // today
			dateFormat: "yy-mm-dd"
		});
		$(".additemselflove, .customitemselflove").on("click", function(e) {
			e.preventDefault();
			$(".datepicker").multiDatesPicker("resetDates");
		});

		$(".addeventbutton").on("click", function(e) {
			if (customEvents) {
				if ($.trim($("#customtitle").val()).length == 0) {
					alert("Please write your custom message");
					return false;
				}
			}
			e.preventDefault();
			if (dates.length > 0) {
				var thiscomeshere = `<div class="form-group">
                <label class='input-inline'>Default Time: 
                <input type='text' class='defaulttime' value='' >
				</label>
				<div style = "display: inline-block; position: relative;">
				<label class="container1 float-right">
					<input type="checkbox" id="defaultchecked" />
					<span class="checkmark-1 "></span>
				</label>
				</div>
				<div class="tooltip float-right">
					<h5>&#9432;</h5>
					<span class="tooltiptext">Please click on the checkbox if you want to use this as your default time against all time fields.</span>
				</div>
				</div>`;

				for (var i = 0; i < dates.length; i++) {
					thiscomeshere +=
						"<label class='input-inline'>" +
						dates[i] +
						"<input required data-time='' data-date='" +
						dates[i] +
						"' type='text' class='time'/></label>";
				}
				$(".modeltime").html(thiscomeshere);
				$(".time, .defaulttime").mdtimepicker({
					hourPadding: false,
					timeFormat: "hh:mm:ss",
					format: "hh:mm"
				});
				$("#timemodal").modal("show");
			}
		});

		$(".modeltime").on("change", ".defaulttime", function() {
			if (
				$("#defaultchecked").is(":checked") &&
				$.trim($(".defaulttime").val()) !== ""
			) {
				timewithout0 = $(".defaulttime").val();
				defaultTime = timewithout0 + ":00";
				$(".time").each(function() {
					date = $(this).data("date");
					date = date + " " + defaultTime;
					$(this).attr("data-time", date);
					$(this).val(timewithout0);
				});
			}
		});

		$(".modeltime").on("change", "#defaultchecked", function() {
			if (
				$("#defaultchecked").is(":checked") &&
				$.trim($(".defaulttime").val()) !== ""
			) {
				timewithout0 = $(".defaulttime").val();
				defaultTime = timewithout0 + ":00";
				$(".time").each(function() {
					date = $(this).data("date");
					date = date + " " + defaultTime;
					$(this).attr("data-time", date);
					$(this).val(timewithout0);
				});
			}
		});

		$("#saveevents").on("click", function() {
			var status = true;
			dateandtimes = [];
			$(".time").each(function() {
				if (
					typeof $(this).attr("data-time") === "undefined" ||
					$.trim($(this).attr("data-time")) === ""
				) {
					alert(
						"Please make sure that you have selected time against each of the dates!"
					);
					status = false;
					return false;
				} else {
					dateandtimes.push($(this).attr("data-time"));
				}
			});
			if (status) {
				calltheSaveEventsAPIFunction(dateandtimes);
			}
		});

		$(".item").on("click", function() {
			dates = [];
			dateandtimes = [];
			customEvents = false;
			$("#alert").hide();
			id = $(this).data("id");
			var message = $(this).data("message");
			$("#titleplaceholder").attr("placeholder", message);
			$("#event").modal("show");
		});

		$(".customitemselflove").on("click", function() {
			dates = [];
			dateandtimes = [];
			customEvents = true;
			$("#alert").hide();
			$("#custom").modal("show");
		});
	}
};
// Maintain array of dates
function calltheSaveEventsAPIFunction(dateandtimes) {
	$("#saveevents, .canceltimemodal").hide();
	$("#myLoader").show();
	if (customEvents) {
		id = 0;
		$.ajax({
			type: "POST",
			url: base_url + "api/user/add-custom-selflovecare-item",
			data: {
				dates: dateandtimes,
				message: $.trim($("#customtitle").val())
			},
			success: function(response) {
				var resp = JSON.parse(response);
				$("#alert").html(resp.message);
				if (resp.status == "success") {
					$("#alert")
						.removeClass("alert-danger")
						.removeClass("alert-success")
						.addClass("alert-success")
						.show();
				} else {
					$("#alert")
						.removeClass("alert-danger")
						.removeClass("alert-success")
						.addClass("alert-danger")
						.show();
				}
				$("#timemodal").modal("hide");
				$("#custom").modal("hide");
				$("#myLoader").hide();
				$("#saveevents, .canceltimemodal").show();
			}
		});
	} else {
		$.ajax({
			type: "POST",
			url: base_url + "api/user/add-new-selflovecare-item",
			data: { dates: dateandtimes, id: id },
			success: function(response) {
				var resp = JSON.parse(response);
				$("#alert").html(resp.message);
				if (resp.status == "success") {
					$(".removeItem-" + id).remove();
					$("#alert")
						.removeClass("alert-danger")
						.removeClass("alert-success")
						.addClass("alert-success")
						.show();
				} else {
					$("#alert")
						.removeClass("alert-danger")
						.removeClass("alert-success")
						.addClass("alert-danger")
						.show();
				}
				$("#timemodal").modal("hide");
				$("#event").modal("hide");
				$("#myLoader").hide();
				$("#saveevents, .canceltimemodal").show();
			}
		});
	}
}

function addDate(date) {
	if (jQuery.inArray(date, dates) < 0) dates.push(date);
}

function removeDate(index) {
	dates.splice(index, 1);
}

// Adds a date if we don't have it yet, else remove it
function addOrRemoveDate(date) {
	var index = jQuery.inArray(date, dates);
	if (index >= 0) removeDate(index);
	else addDate(date);
}

// Takes a 1-digit number and inserts a zero before it
function padNumber(number) {
	var ret = new String(number);
	if (ret.length == 1) ret = "0" + ret;
	return ret;
}
function GetTodayDate() {
	var tdate = new Date();
	var dd = tdate.getDate(); //yields day
	var MM = tdate.getMonth(); //yields month
	var yyyy = tdate.getFullYear(); //yields year
	var currentDate = yyyy + "-" + (MM + 1) + "-" + dd;

	return currentDate;
}
