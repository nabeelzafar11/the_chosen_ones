document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        
        $("#choose_new_password").validate({
            rules: {
                password: {
                    maxlength: 25,
                    required: true
                },
                confirmPassword: {
                    maxlength: 25,
                    required: true,
                    equalTo: "#password"
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}