document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#recover_password_form").validate({
            rules: {
                password: {
                    required: true,
                    maxlength: 100,
                },
                confirmPassword: {
                    required: true,
                    equalTo: "#re-password"
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                        response = jQuery.parseJSON(response);
                        if (response.status == 'success') {
                            $(form).find('[type=reset]').trigger('click');
                        }
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}
