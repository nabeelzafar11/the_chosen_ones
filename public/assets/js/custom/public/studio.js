document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $.validator.addMethod("alphanumericspacesdashesunderscore", function (value, element) {
            return this.optional(element) || /^[a-z0-9\-\_\s]+$/i.test(value);
        }, "Must contain only letters, numbers, space, dashes, or underscore.");
        $("#studio_form").validate({
            rules: {
                firstName: {
                    required: true,
                    maxlength: 100
                },
                lastName: {
                    required: true,
                    maxlength: 100

                },
                studioName: {
                    required: true,
                    maxlength: 255,
                    alphanumericspacesdashesunderscore: true
                },
                studioCategory: {
                    required: true

                },
                studioShortDescription: {
                    required: true
                },
                studioLongDescription: {
                    required: true
                },
                studioAddress: {
                    required: true,
                    maxlength: 255
                },
                studioCity: {
                    required: true,
                    maxlength: 255
                },
                studioState: {
                    required: true,
                    maxlength: 255

                },
                studioZipCode: {
                    required: true,
                    maxlength: 100
                },
                studioCountry: {
                    required: true,
                    maxlength: 255
                },
                studioMainImage: {
                    required: true
                },
                studioFullDayPrice: {
                    require_from_group: [1, ".price-group"],
                    number: true
                },
                studioHalfDayPrice: {
                    require_from_group: [1, ".price-group"],
                    number: true
                },
                studioPerHourPrice: {
                    require_from_group: [1, ".price-group"],
                    number: true
                }

            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                        response = jQuery.parseJSON(response);
                        if (response.status == 'success') {
                            $(form).find('[type=reset]').trigger('click');
                        }
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
        $('#studioDisabledDateDisableDate').multiDatesPicker({
            minDate: dateRangeMinDate, // today
        });
    }
}

function upload_image(elem) {
    // $(elem).val('');
    if ($("#imageForm input[type=file]").val()) {

        $('#imageForm').ajaxSubmit({
            success: function (response) {
                response = jQuery.parseJSON(response);
                if (imageMainElement.find('.imageDiv.mainImageDiv').length > 0) {
                    imageMainElement.find('.imageDiv').html("<img src='" + mediaPath + response.message + "' />");
                } else {
                    imageMainElement.find('.imageDiv').append("<a href='javascript:void(0);' style='cursor: default;' \n" +
                        "                               data-placement='top' data-toggle='tooltip'\n" +
                        "                               title='Delete Item' disabled><i class='ti-trash' onclick='remove_image_handler(this, \"" + response.message + ";\");'></i><img src='" + mediaPath + response.message + "' /></a>");
                }
                if (imageMainElement.find('.imageInput.mainImageInput').length > 0) {
                    imageMainElement.find('.imageInput input').val(response.message);
                } else {
                    imageMainElement.find('.imageInput input').val(imageMainElement.find('.imageInput input').val() + response.message + ";");
                }
                imageMainElement.find('.removeButton').removeAttr('disabled');
                ajax_end_function();
                $("#imageForm input[type=file]").val('');
                $("#studio_form").valid();
            },
            beforeSubmit: function () {
                ajax_start_function('#studio_form');
            },
            error: function (response) {
                show_request_failed_alert('#studio_form');
                ajax_end_function();
                $("#imageForm input[type=file]").val('');
            }
        });
    }

}

var imageMainElement;

function open_image_handler(elem) {
    imageMainElement = $(elem).closest('.studioSection');
    $("#imageForm input[type=file]").trigger('click');
}

function remove_image_handler(elem, image) {
    tempElement = $(elem).closest('.studioSection');
    if (tempElement.find('.imageInput.mainImageInput').length > 0) {
        tempElement.find('.imageInput input').val('');
    } else {
        console.log(tempElement.find('.imageInput input').val());
        tempString = tempElement.find('.imageInput input').val();
        tempElement.find('.imageInput input').val(tempString.replace(image, ""));
        console.log(tempElement.find('.imageInput input').val());
    }
    if (tempElement.find('.imageDiv.mainImageDiv').length > 0) {
        tempElement.find('.imageDiv').html("");
    } else {
        $(elem).closest('a').remove();
    }
    tempElement.find('.removeButton').attr('disabled', 'disabled');
}

function move_next_section(elem) {
    if ($("#studio_form").valid()) {
        $(elem).closest('.studioSection').hide();
        $(elem).closest('.studioSection').next().show();
    }
}

function submit_studio_form() {
    if ($("#studio_form").valid()) {
        $("#studio_form").submit();
    }
}

function move_previous_section(elem) {
    $(elem).closest('.studioSection').hide();
    $(elem).closest('.studioSection').prev().show();
}

function get_address_detail(elem) {
    if ($(elem).val()) {
        ajax_start_function("#studio_form")
        var data = {
            'address': $(elem).val()
        }
        $.post(apiPath + "api/get_address_details", data, function (data) {
            response = jQuery.parseJSON(data);
            if (response.status == 'success') {
                if(response.message && response.message.city && response.message.state && response.message.country && response.message.zipCode) {
                    console.log(response.message['city']);
                    console.log(response.message.city);
                    $('#studioCity').val(response.message.city);
                    $('#studioState').val(response.message.state);
                    $('#studioCountry').val(response.message.country);
                    $('#studioZipCode').val(response.message.zipCode);
                }else{
                    $('#studioCity').val('');
                    $('#studioState').val('');
                    $('#studioCountry').val('');
                    $('#studioZipCode').val('');
                    $(elem).val('');
                }
            } else {
                ajax_success_function("#studio_form", data);
                $(elem).val('');
                $('#studioCity').val('');
                $('#studioState').val('');
                $('#studioCountry').val('');
                $('#studioZipCode').val('');
            }
        }).done(function () {
            ajax_end_function();
        }).fail(function () {
            show_request_failed_alert("#studio_form");
            ajax_end_function();
            $(elem).val('');
            $('#studioCity').val('');
            $('#studioState').val('');
            $('#studioCountry').val('');
            $('#studioZipCode').val('');
        }).always(function () {
            ajax_end_function();
        });
    }
}
