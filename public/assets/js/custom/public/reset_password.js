document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#reset_password").validate({
            rules: {
                email: {
                    maxlength: 200,
                    email: true,
                    required: true
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}