document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#dashboard_form").validate({
            rules: {
                image: {
                    required: true
                },
                name: {
                    required: true
                },
                message: {
                    required: true
                },
                seo_keywords: {
                    required: true
                },
                seo_title: {
                    required: true
                },
                seo_description: {
                    required: true
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}