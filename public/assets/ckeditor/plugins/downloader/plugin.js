CKEDITOR.plugins.add( 'downloader', {
    icons: 'downloader',
    init: function( editor ) {
        //adicionando o comando
        editor.addCommand( 'downloderDialog', new CKEDITOR.dialogCommand( 'downloderDialog' ) );

        //setando o botão
        editor.ui.addButton('Downloader', {
            label: 'Add Downloader',
            command: 'downloderDialog',
            toolbar: 'insert'
        });

        //Adicionando a janela de dialogo
        CKEDITOR.dialog.add( 'downloderDialog', this.path + 'dialogs/accordion.js' );


    }
});
