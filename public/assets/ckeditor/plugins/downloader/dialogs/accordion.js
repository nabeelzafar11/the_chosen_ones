CKEDITOR.dialog.add( 'downloderDialog', function ( editor ) {
    return {
        title: 'Download Button',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'tab-basic',
                label: 'Basic Settings',
                elements: [
                    {
                        type: 'text',
                        id: 'downloadtext',
                        label: 'Button Text:',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Enter button text" )
                    },
                    {
                        type: 'text',
                        id: 'downloadurl',
                        label: 'Button Url:',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Enter button url" )
                    },
                ]
            }
        ],
        onOk: function() {
            var dialog = this;
            var downloadtext = (dialog.getValueOf('tab-basic','downloadtext')); //Número de seções que serão criadas
            var downloadurl = (dialog.getValueOf('tab-basic','downloadurl')); //Número de seções que serão criadas
            var dialog = this;
            editor.insertHtml( '<div><div class="specialBox withIcon" title=""><i class="icon icon-download">&nbsp;</i> <p> <a href="'+downloadurl+'" data-resource-category="" target="_blank" class="button primary-button rounded-button wow fadeInUp">'+downloadtext+'</a><span></span></p></div></div>');

        }
    };
});
