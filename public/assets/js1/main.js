var $ = jQuery.noConflict();

$.fn.inlineStyle = function (prop) {
    return this.prop("style")[$.camelCase(prop)];
};

$.fn.doOnce = function( func ) {
    this.length && func.apply( this );
    return this;
};

(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
            || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());



function debounce(func, wait, immediate) {
    var timeout, args, context, timestamp, result;
    return function() {
        context = this;
        args = arguments;
        timestamp = new Date();
        var later = function() {
            var last = (new Date()) - timestamp;
            if (last < wait) {
                timeout = setTimeout(later, wait - last);
            } else {
                timeout = null;
                if (!immediate) result = func.apply(context, args);
            }
        };
        var callNow = immediate && !timeout;
        if (!timeout) {
            timeout = setTimeout(later, wait);
        }
        if (callNow) result = func.apply(context, args);
        return result;
    };
}

var SEMICOLON = SEMICOLON || {};

$(function () {

    SEMICOLON.initialize = {
        init: function(){

			SEMICOLON.initialize.responsiveClasses();
			SEMICOLON.initialize.dataResponsiveClasses();
			SEMICOLON.initialize.dataResponsiveHeights();
        },

        responsiveClasses: function(){

			if( typeof jRespond === 'undefined' ) {
				console.log('responsiveClasses: jRespond not Defined.');
				return true;
			}

			var jRes = jRespond([
				{
					label: 'smallest',
					enter: 0,
					exit: 575
				},{
					label: 'handheld',
					enter: 576,
					exit: 767
				},{
					label: 'tablet',
					enter: 768,
					exit: 991
				},{
					label: 'laptop',
					enter: 992,
					exit: 1199
				},{
					label: 'desktop',
					enter: 1200,
					exit: 10000
				}
			]);
			jRes.addFunc([
				{
					breakpoint: 'desktop',
					enter: function() { $body.addClass('device-xl'); },
					exit: function() { $body.removeClass('device-xl'); }
				},{
					breakpoint: 'laptop',
					enter: function() { $body.addClass('device-lg'); },
					exit: function() { $body.removeClass('device-lg'); }
				},{
					breakpoint: 'tablet',
					enter: function() { $body.addClass('device-md'); },
					exit: function() { $body.removeClass('device-md'); }
				},{
					breakpoint: 'handheld',
					enter: function() { $body.addClass('device-sm'); },
					exit: function() { $body.removeClass('device-sm'); }
				},{
					breakpoint: 'smallest',
					enter: function() { $body.addClass('device-xs'); },
					exit: function() { $body.removeClass('device-xs'); }
				}
			]);
		},

        dataResponsiveClasses: function(){
			var $dataClassXs = $('[data-class-xs]'),
				$dataClassSm = $('[data-class-sm]'),
				$dataClassMd = $('[data-class-md]'),
				$dataClassLg = $('[data-class-lg]'),
				$dataClassXl = $('[data-class-xl]');

			if( $dataClassXs.length > 0 ) {
				$dataClassXs.each( function(){
					var element = $(this),
						elementClass = element.attr('data-class-xs'),
						elementClassDelete = element.attr('data-class-sm') + ' ' + element.attr('data-class-md') + ' ' + element.attr('data-class-lg') + ' ' + element.attr('data-class-xl');

					if( $body.hasClass('device-xs') ) {
						element.removeClass( elementClassDelete );
						element.addClass( elementClass );
					}
				});
			}

			if( $dataClassSm.length > 0 ) {
				$dataClassSm.each( function(){
					var element = $(this),
						elementClass = element.attr('data-class-sm'),
						elementClassDelete = element.attr('data-class-xs') + ' ' + element.attr('data-class-md') + ' ' + element.attr('data-class-lg') + ' ' + element.attr('data-class-xl');

					if( $body.hasClass('device-sm') ) {
						element.removeClass( elementClassDelete );
						element.addClass( elementClass );
					}
				});
			}

			if( $dataClassMd.length > 0 ) {
				$dataClassMd.each( function(){
					var element = $(this),
						elementClass = element.attr('data-class-md'),
						elementClassDelete = element.attr('data-class-xs') + ' ' + element.attr('data-class-sm') + ' ' + element.attr('data-class-lg') + ' ' + element.attr('data-class-xl');

					if( $body.hasClass('device-md') ) {
						element.removeClass( elementClassDelete );
						element.addClass( elementClass );
					}
				});
			}

			if( $dataClassLg.length > 0 ) {
				$dataClassLg.each( function(){
					var element = $(this),
						elementClass = element.attr('data-class-lg'),
						elementClassDelete = element.attr('data-class-xs') + ' ' + element.attr('data-class-sm') + ' ' + element.attr('data-class-md') + ' ' + element.attr('data-class-xl');

					if( $body.hasClass('device-lg') ) {
						element.removeClass( elementClassDelete );
						element.addClass( elementClass );
					}
				});
			}

			if( $dataClassXl.length > 0 ) {
				$dataClassXl.each( function(){
					var element = $(this),
						elementClass = element.attr('data-class-xl'),
						elementClassDelete = element.attr('data-class-xs') + ' ' + element.attr('data-class-sm') + ' ' + element.attr('data-class-md') + ' ' + element.attr('data-class-lg');

					if( $body.hasClass('device-xl') ) {
						element.removeClass( elementClassDelete );
						element.addClass( elementClass );
					}
				});
			}
		},

		dataResponsiveHeights: function(){
			var $dataHeightXs = $('[data-height-xs]'),
				$dataHeightSm = $('[data-height-sm]'),
				$dataHeightMd = $('[data-height-md]'),
				$dataHeightLg = $('[data-height-lg]'),
				$dataHeightXl = $('[data-height-xl]');

			if( $dataHeightXs.length > 0 ) {
				$dataHeightXs.each( function(){
					var element = $(this),
						elementHeight = element.attr('data-height-xs');

					if( $body.hasClass('device-xs') ) {
						if( elementHeight != '' ) { element.css( 'height', elementHeight ); }
					}
				});
			}

			if( $dataHeightSm.length > 0 ) {
				$dataHeightSm.each( function(){
					var element = $(this),
						elementHeight = element.attr('data-height-sm');

					if( $body.hasClass('device-sm') ) {
						if( elementHeight != '' ) { element.css( 'height', elementHeight ); }
					}
				});
			}

			if( $dataHeightMd.length > 0 ) {
				$dataHeightMd.each( function(){
					var element = $(this),
						elementHeight = element.attr('data-height-md');

					if( $body.hasClass('device-md') ) {
						if( elementHeight != '' ) { element.css( 'height', elementHeight ); }
					}
				});
			}

			if( $dataHeightLg.length > 0 ) {
				$dataHeightLg.each( function(){
					var element = $(this),
						elementHeight = element.attr('data-height-lg');

					if( $body.hasClass('device-lg') ) {
						if( elementHeight != '' ) { element.css( 'height', elementHeight ); }
					}
				});
			}

			if( $dataHeightXl.length > 0 ) {
				$dataHeightXl.each( function(){
					var element = $(this),
						elementHeight = element.attr('data-height-xl');

					if( $body.hasClass('device-xl') ) {
						if( elementHeight != '' ) { element.css( 'height', elementHeight ); }
					}
				});
			}
		}
    };

    SEMICOLON.header = {
        init: function(){

			SEMICOLON.header.superfish();
            SEMICOLON.header.menufunctions();
            SEMICOLON.header.fullWidthMenu();
			SEMICOLON.header.logo();


        },

        superfish: function(){

			if( $body.hasClass('device-xl') || $body.hasClass('device-lg') ) {
				$('#primary-menu ul ul, #primary-menu ul .mega-menu').css('display', 'block');
				SEMICOLON.header.menuInvert();
				$('#primary-menu ul ul, #primary-menu ul .mega-menu').css('display', '');
			}

			if( !$().superfish ) {
				$body.addClass('no-superfish');
				console.log('superfish: Superfish not Defined.');
				return true;
			}

			$('body:not(.side-header) #primary-menu:not(.on-click) > ul, body:not(.side-header) #primary-menu:not(.on-click) > div > ul:not(.dropdown-menu), .top-links:not(.on-click) > ul').superfish({
				popUpSelector: 'ul,.mega-menu,.top-link-section',
				delay: 250,
				speed: 100,
				animation: {opacity:'show'},
				animationOut:  {opacity:'hide'},
				cssArrows: false,
				onShow: function(){
					var megaMenuContent = $(this);
					if( megaMenuContent.find('.owl-carousel.customjs').length > 0 ) {
						megaMenuContent.find('.owl-carousel').removeClass('customjs');
						SEMICOLON.widget.carousel();
					}

					if( megaMenuContent.hasClass('mega-menu-content') && megaMenuContent.find('.widget').length > 0 ) {
						if( $body.hasClass('device-xl') || $body.hasClass('device-lg') ) {
							setTimeout( function(){ SEMICOLON.initialize.commonHeight( megaMenuContent ); }, 200);
						} else {
							megaMenuContent.children().height('');
						}
					}
				}
			});

			$('body.side-header #primary-menu:not(.on-click) > ul').superfish({
				popUpSelector: 'ul',
				delay: 250,
				speed: 350,
				animation: {opacity:'show',height:'show'},
				animationOut:  {opacity:'hide',height:'hide'},
				cssArrows: false
			});

        },

        menufunctions: function(){

			$( '#primary-menu ul li:has(ul)' ).addClass('sub-menu');
			$( '.top-links ul li:has(ul) > a, #primary-menu.with-arrows > ul > li:has(ul) > a > div, #primary-menu.with-arrows > div > ul > li:has(ul) > a > div, #page-menu nav ul li:has(ul) > a > div' ).append( '<i class="icon-angle-down"></i>' );
			$( '.top-links > ul' ).addClass( 'clearfix' );

			if( $body.hasClass('device-xl') || $body.hasClass('device-lg') ) {
				$('#primary-menu.sub-title > ul > li').hover(function() {
					$(this).prev().css({ backgroundImage : 'none' });
				}, function() {
					$(this).prev().css({ backgroundImage : 'url("images/icons/menu-divider.png")' });
				});

				$('#primary-menu.sub-title').children('ul').children('.current').prev().css({ backgroundImage : 'none' });
			}

			$('#primary-menu.on-click li:has(ul) > a').on( 'click', function(e){
				$(this).parents('.sub-menu').siblings().find('ul,.mega-menu-content').removeClass('d-block');
				$(this).parent('li').children('ul,.mega-menu-content').toggleClass('d-block');
				e.preventDefault();
			});

			// var responsiveThreshold = $header.attr('data-responsive-under');
			// if( !responsiveThreshold ) { responsiveThreshold = 992; }

			// if( windowWidth < Number( responsiveThreshold ) ) {
			// 	$body.addClass('mobile-header-active');
			// } else {
			// 	$body.removeClass('mobile-header-active');
			// }

			if( SEMICOLON.isMobile.Android() ) {
				$( '#primary-menu ul li.sub-menu' ).children('a').on('touchstart', function(e){
					if( !$(this).parent('li.sub-menu').hasClass('sfHover') ) {
						e.preventDefault();
					}
				});
			}

			if( SEMICOLON.isMobile.Windows() ) {
				if( $().superfish ){
					$('#primary-menu > ul, #primary-menu > div > ul,.top-links > ul').superfish('destroy').addClass('windows-mobile-menu');
				} else {
					$('#primary-menu > ul, #primary-menu > div > ul,.top-links > ul').addClass('windows-mobile-menu');
					console.log('menufunctions: Superfish not defined.');
				}

				$( '#primary-menu ul li:has(ul)' ).append('<a href="#" class="wn-submenu-trigger"><i class="icon-angle-down"></i></a>');

				$( '#primary-menu ul li.sub-menu' ).children('a.wn-submenu-trigger').click( function(e){
					$(this).parent().toggleClass('open');
					$(this).parent().find('> ul, > .mega-menu-content').stop(true,true).toggle();
					return false;
				});
			}

        },

        menuInvert: function() {

			$('#primary-menu ul ul').each( function( index, element ){
				var $menuChildElement = $(element),
					menuChildOffset = $menuChildElement.offset(),
					menuChildWidth = $menuChildElement.width(),
					menuChildLeft = menuChildOffset.left;

				//console.log(menuChildOffset, menuChildLeft, menuChildWidth);

				if(windowWidth - (menuChildWidth + menuChildLeft) < 0) {
					$menuChildElement.addClass('menu-pos-invert');
				}
			});



        },

        fullWidthMenu: function(){

			if( $body.hasClass('stretched') ) {

				if( $header.hasClass('full-header') ) { $('.mega-menu .mega-menu-content').css({ 'width': $wrapper.width() - 60 }); }
			} else {

				if( $header.hasClass('full-header') ) { $('.mega-menu .mega-menu-content').css({ 'width': $wrapper.width() - 80 }); }
			}

            if( $body.hasClass('device-xl') || $body.hasClass('device-lg') ) {
                $('#primary-menu ul.mega-menu').css('width', $headerWrap.width());

                $('#primary-menu li.sub-menu > ul.mega-menu').each( function( index, element ){
                    var $menuChildElement = $(element),
                        menuChildLeft = $menuChildElement.parent().position().left;

                    $menuChildElement.css('left', -menuChildLeft)
                });
                //$('#primary-menu ul > li.sub-menu:has(ul.mega-menu)').addClass('mega-sub');
            } else {
                $('#primary-menu ul .mega-menu').css('width', '100%');
			}
        },

        stickyMenu: function( headerOffset ){
            if ($window.scrollTop() > headerOffset) {
                if( $body.hasClass('device-xl') || $body.hasClass('device-lg') ) {
                    $('body:not(.side-header) #header:not(.no-sticky)').addClass('sticky-header');
                    if( !$headerWrap.hasClass('force-not-dark') ) { $headerWrap.removeClass('not-dark'); }
                    SEMICOLON.header.stickyMenuClass();
                } else if( $body.hasClass('device-sm') || $body.hasClass('device-xs') || $body.hasClass('device-md') ) {
                    if( $body.hasClass('sticky-responsive-menu') ) {
                        $('#header:not(.no-sticky)').addClass('responsive-sticky-header');
                        SEMICOLON.header.stickyMenuClass();
                    }
                }
            } else {
                SEMICOLON.header.removeStickyness();
            }
        },

        stickyPageMenu: function( pageMenuOffset ){
            if ($window.scrollTop() > pageMenuOffset) {
                if( $body.hasClass('device-xl') || $body.hasClass('device-lg') ) {
                    $('#page-menu:not(.dots-menu,.no-sticky)').addClass('sticky-page-menu');
                } else if( $body.hasClass('device-sm') || $body.hasClass('device-xs') || $body.hasClass('device-md') ) {
                    if( $body.hasClass('sticky-responsive-pagemenu') ) {
                        $('#page-menu:not(.dots-menu,.no-sticky)').addClass('sticky-page-menu');
                    }
                }
            } else {
                $('#page-menu:not(.dots-menu,.no-sticky)').removeClass('sticky-page-menu');
            }
        },

        removeStickyness: function(){
            if( $header.hasClass('sticky-header') ){
                $('body:not(.side-header) #header:not(.no-sticky)').removeClass('sticky-header');
                $header.removeClass().addClass(oldHeaderClasses);
                $headerWrap.removeClass().addClass(oldHeaderWrapClasses);
                if( !$headerWrap.hasClass('force-not-dark') ) { $headerWrap.removeClass('not-dark'); }

            }
            if( $header.hasClass('responsive-sticky-header') ){
                $('body.sticky-responsive-menu #header').removeClass('responsive-sticky-header');
            }
            if( ( $body.hasClass('device-sm') || $body.hasClass('device-xs') || $body.hasClass('device-md') ) && ( typeof responsiveMenuClasses === 'undefined' ) ) {
                $header.removeClass().addClass(oldHeaderClasses);
                $headerWrap.removeClass().addClass(oldHeaderWrapClasses);
                if( !$headerWrap.hasClass('force-not-dark') ) { $headerWrap.removeClass('not-dark'); }
            }
        },

        logo: function(){
			if( ( $header.hasClass('dark') || $body.hasClass('dark') ) && !$headerWrap.hasClass('not-dark') ) {
				if( defaultDarkLogo ){ defaultLogo.find('img').attr('src', defaultDarkLogo); }
				if( retinaDarkLogo ){ retinaLogo.find('img').attr('src', retinaDarkLogo); }
			} else {
				if( defaultLogoImg ){ defaultLogo.find('img').attr('src', defaultLogoImg); }
				if( retinaLogoImg ){ retinaLogo.find('img').attr('src', retinaLogoImg); }
			}
			if( $header.hasClass('sticky-header') ) {
				if( defaultStickyLogo ){ defaultLogo.find('img').attr('src', defaultStickyLogo); }
				if( retinaStickyLogo ){ retinaLogo.find('img').attr('src', retinaStickyLogo); }
			}
			if( $body.hasClass('device-md') || $body.hasClass('device-sm') || $body.hasClass('device-xs') ) {
				if( defaultMobileLogo ){ defaultLogo.find('img').attr('src', defaultMobileLogo); }
				if( retinaMobileLogo ){ retinaLogo.find('img').attr('src', retinaMobileLogo); }
			}
		},

        stickyMenuClass: function(){
            if( stickyMenuClasses ) { var newClassesArray = stickyMenuClasses.split(/ +/); } else { var newClassesArray = ''; }
            var noOfNewClasses = newClassesArray.length;

            if( noOfNewClasses > 0 ) {
                var i = 0;
                for( i=0; i<noOfNewClasses; i++ ) {
                    if( newClassesArray[i] == 'not-dark' ) {
                        $header.removeClass('dark');
                        $headerWrap.addClass('not-dark');
                    } else if( newClassesArray[i] == 'dark' ) {
                        $headerWrap.removeClass('not-dark force-not-dark');
                        if( !$header.hasClass( newClassesArray[i] ) ) {
                            $header.addClass( newClassesArray[i] );
                        }
                    } else if( !$header.hasClass( newClassesArray[i] ) ) {
                        $header.addClass( newClassesArray[i] );
                    }
                }
            }
        },

        responsiveMenuClass: function(){
			if( $body.hasClass('device-sm') || $body.hasClass('device-xs') || $body.hasClass('device-md') ){
				if( responsiveMenuClasses ) { var newClassesArray = responsiveMenuClasses.split(/ +/); } else { var newClassesArray = ''; }
				var noOfNewClasses = newClassesArray.length;

				if( noOfNewClasses > 0 ) {
					var i = 0;
					for( i=0; i<noOfNewClasses; i++ ) {
						if( newClassesArray[i] == 'not-dark' ) {
							$header.removeClass('dark');
							$headerWrap.addClass('not-dark');
						} else if( newClassesArray[i] == 'dark' ) {
							$headerWrap.removeClass('not-dark force-not-dark');
							if( !$header.hasClass( newClassesArray[i] ) ) {
								$header.addClass( newClassesArray[i] );
							}
						} else if( !$header.hasClass( newClassesArray[i] ) ) {
							$header.addClass( newClassesArray[i] );
						}
					}
				}
				SEMICOLON.header.logo();
			}
		}
    };

    SEMICOLON.extras = function(){

        if( $().tooltip ) {
            $('[data-toggle="tooltip"]').tooltip({container: 'body'});
        } else {
            console.log('extras: Bootstrap Tooltip not defined.');
        }

        if( $().popover ) {
            $('[data-toggle=popover]').popover();
        } else {
            console.log('extras: Bootstrap Popover not defined.');
        }

        $('#primary-menu-trigger,#overlay-menu-close').click(function() {
            if( $('#primary-menu').find('ul.mobile-primary-menu').length > 0 ) {
                $( '#primary-menu > ul.mobile-primary-menu, #primary-menu > div > ul.mobile-primary-menu' ).toggleClass('d-block');
            } else {
                $( '#primary-menu > ul, #primary-menu > div > ul' ).toggleClass('d-block');
            }
            $body.toggleClass("primary-menu-open");
            return false;
        });

        if( SEMICOLON.isMobile.any() ){
            $body.addClass('device-touch');
        }

        $('[data-background]').each(function () {
            var background = $(this).data('background');

            $(this).css('background-image', 'url(' + background + ')');
        });

        $('[data-bg-color]').each(function () {
            var background = $(this).data('bg-color');

            $(this).css('background-color', background);
        });

        $('[data-overlay]').each(function () {
            var overlay = $(this).data('overlay');

            $(this).css('background', overlay);
        });

        new Swiper('#slider .swiper-container', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });

        new Swiper('#news .swiper-container', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });

        new Swiper('.testimonials .swiper-container', {
            slidesPerView: 2,
			autoplay: true,
			loop: true,
            speed: 600,
            parallax: true,
			spaceBetween: 0,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
			breakpoints: {
            	992: {
            		slidesPerView: 1
				}
			}
        });

        $('.contrast').on('click', function (e) {
			e.preventDefault();

            if ($(this).parent('li').hasClass('active')) {
                SEMICOLON.documentOnReady.reInvertColor();
                $(this).parent('li').removeClass('active');
			} else {
            	SEMICOLON.documentOnReady.invertColor();
            	$(this).parent('li').addClass('active');
			}
        });

        $('.shrink-text').on('click', function (e) {
			e.preventDefault();
            if ($(this).parent('li').hasClass('active')) {
                $(this).parent('li').removeClass('active');
                $('html').removeClass('shrink');
            } else {
            	if ($('.enlarge-text').parent('li').hasClass('active')) {
            		$('.enlarge-text').parent('li').removeClass('active');
                    $('html').removeClass('enlarge');
				}
                $(this).parent('li').addClass('active');
                $('html').addClass('shrink');
            }
        });

        $('.enlarge-text').on('click', function (e) {
            e.preventDefault();
            if ($(this).parent('li').hasClass('active')) {
                $(this).parent('li').removeClass('active');
                $('html').removeClass('enlarge');
            } else {
                if ($('.shrink-text').parent('li').hasClass('active')) {
                    $('.shrink-text').parent('li').removeClass('active');
                    $('html').removeClass('shrink');
                }
                $(this).parent('li').addClass('active');
				$('html').addClass('enlarge');
            }
        });

        $('.print').on('click', function (e) {
            e.preventDefault();
            window.print();
            window.close();
        });

        $('.search-icon').on('click', function (e) {
            e.preventDefault();
			$('.search-overlay').addClass('active');
            setTimeout(function() { $('.search-overlay input.search').focus() }, 300);

        });

        $('.overlay-close').on('click', function (e) {
        	e.preventDefault();
            $('.search-overlay').removeClass('active');
        });

        $('.text-only').on('click', function (e) {
            e.preventDefault();
            if ($(this).parent('li').hasClass('active')) {
                $(this).parent('li').removeClass('active');
                $('body').removeClass('remove-images');
            } else {
                $(this).parent('li').addClass('active');
                $('body').addClass('remove-images');
            }
        });

        $(document).on('click', 'a[href^="#"]', function(e) {
            var id = $(this).attr('href');

            var $id = $(id);
            if ($id.length === 0) {
                return;
            }

            e.preventDefault();

            var pos = $id.offset().top - 100;

            $('body, html').animate({scrollTop: pos});
        });

        wow = new WOW(
            {
                boxClass:     'wow',      // default
                animateClass: 'animated', // default
                offset:       0,          // default
                mobile:       true,       // default
                live:         true        // default
            }
        );
        wow.init();

        // var el = {
        //     darkLogo : $("<img>", {src: defaultDarkLogo}),
        //     darkRetinaLogo : $("<img>", {src: retinaDarkLogo})
        // };
        // el.darkLogo.prependTo("body");
        // el.darkRetinaLogo.prependTo("body");
        // el.darkLogo.css({'position':'absolute','z-index':'-100'});
        // el.darkRetinaLogo.css({'position':'absolute','z-index':'-100'});
    };

    SEMICOLON.widget = {
    	init: function () {
            SEMICOLON.widget.navTree();
        },

        navTree: function(){
            var $navTreeEl = $('.nav-tree');
            if( $navTreeEl.length > 0 ){
                $navTreeEl.each( function(){
                    var element = $(this),
                        elementSpeed = element.attr('data-speed'),
                        elementEasing = element.attr('data-easing');

                    if( !elementSpeed ) { elementSpeed = 250; }
                    if( !elementEasing ) { elementEasing = 'swing'; }

                    element.find( 'ul li:has(ul)' ).addClass('sub-menu');
                    element.find( 'ul li:has(ul) > a' ).append( ' <i class="icon-angle-down"></i>' );

                    if( element.hasClass('on-hover') ){
                        element.find( 'ul li:has(ul):not(.active)' ).hover( function(e){
                            $(this).children('ul').stop(true, true).slideDown( Number(elementSpeed), elementEasing);
                        }, function(){
                            $(this).children('ul').delay(250).slideUp( Number(elementSpeed), elementEasing);
                        });
                    } else {
                        element.find( 'ul li:has(ul) > a > i' ).click( function(e){
                            var childElement = $(this).parent('a');
                            element.find( 'ul li' ).not(childElement.parents()).removeClass('active');
                            childElement.parent().children('ul').slideToggle( Number(elementSpeed), elementEasing, function(){
                                $(this).find('ul').hide();
                                $(this).find('li.active').removeClass('active');
                            });
                            element.find( 'ul li > ul' ).not(childElement.parent().children('ul')).not(childElement.parents('ul')).slideUp( Number(elementSpeed), elementEasing );
                            childElement.parent('li:has(ul)').toggleClass('active');
                            e.preventDefault();
                        });
                    }

                });
            }
        }
    };

    SEMICOLON.isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (SEMICOLON.isMobile.Android() || SEMICOLON.isMobile.BlackBerry() || SEMICOLON.isMobile.iOS() || SEMICOLON.isMobile.Opera() || SEMICOLON.isMobile.Windows());
		}
	};

	SEMICOLON.documentOnReady = {

		init: function(){
			SEMICOLON.initialize.init();
            SEMICOLON.header.init();
            SEMICOLON.extras();
            SEMICOLON.widget.init();
            SEMICOLON.documentOnReady.windowscroll();
            SEMICOLON.documentOnReady.marginAdjust();


		},

		marginAdjust: function() {
            if ( $body.hasClass('device-md') || $body.hasClass('device-sm') || $body.hasClass('device-xs') ) {
                $('.blog-post').parent().css('margin-bottom', '1.75rem');
                $('.feature-box').parent().css('margin-bottom', '3rem');
            }

            $('.feature-box.style-3').parent().css('margin-bottom', '3rem');
		},

		invertColor: function () {
            //set up color properties to iterate through
            var colorProperties = ['color', 'background-color'];

//iterate through every element in reverse order...
            $("*").each(function() {
                var color = null;

                for (var prop in colorProperties) {
                    prop = colorProperties[prop];
                    color = new RGBColor($(this).css(prop));

                    if (color.ok) {
                        $(this).not('.overlay').css(prop, 'rgb(' + (255 - color.r) + ', ' + (255 - color.g) + ', ' + (255 - color.b) + ')');
                    }
                    color = null;
                }
            });
		},

        reInvertColor: function () {
            var colorProperties = ['color', 'background-color'];

            $("*").each(function() {
                var color = null;

                for (var prop in colorProperties) {
                    prop = colorProperties[prop];
                    color = new RGBColor($(this).css(prop));

                    if (color.ok) {
						$(this).not('.overlay').css(prop, '');
                    }
                    color = null;
                }
            });
		},

		windowscroll: function(){

			var headerOffset = 0,
				headerWrapOffset = 0,
				pageMenuOffset = 0;

			if( $header.length > 0 ) { headerOffset = $header.offset().top; }
			if( $header.length > 0 ) { headerWrapOffset = $headerWrap.offset().top; }

			var headerDefinedOffset = $header.attr('data-sticky-offset');
			if( typeof headerDefinedOffset !== 'undefined' ) {
				if( headerDefinedOffset == 'full' ) {
					headerWrapOffset = $window.height();
					var headerOffsetNegative = $header.attr('data-sticky-offset-negative');
					if( typeof headerOffsetNegative !== 'undefined' ) { headerWrapOffset = headerWrapOffset - headerOffsetNegative - 1; }
				} else {
					headerWrapOffset = Number(headerDefinedOffset);
				}
			}

			SEMICOLON.header.stickyMenu( headerWrapOffset );
			SEMICOLON.header.stickyPageMenu( pageMenuOffset );

			$window.on( 'scroll', function(){

				//SEMICOLON.initialize.goToTopScroll();
				SEMICOLON.header.stickyMenu( headerWrapOffset );
				SEMICOLON.header.stickyPageMenu( pageMenuOffset );
				SEMICOLON.header.logo();

			});
		}

    };

    SEMICOLON.documentOnResize = {

		init: function(){

			var t = setTimeout( function(){
				SEMICOLON.header.fullWidthMenu();
				SEMICOLON.initialize.dataResponsiveClasses();
				SEMICOLON.initialize.dataResponsiveHeights();
                SEMICOLON.header.logo();


                SEMICOLON.documentOnReady.marginAdjust();

				if( $body.hasClass('device-xl') || $body.hasClass('device-lg') ) {
                    $('#primary-menu').find('ul.mobile-primary-menu').removeClass('d-block');
                }
			}, 500 );

			windowWidth = $window.width();

		}

	};

    SEMICOLON.documentOnLoad = {

		init: function(){
			SEMICOLON.header.responsiveMenuClass();
		}

	};

    var $window = $(window),
		$body = $('body'),
		$wrapper = $('#wrapper'),
		$header = $('#header'),
		$headerWrap = $('#header-wrap'),
		windowWidth = $window.width(),
		oldHeaderClasses = $header.attr('class'),
		oldHeaderWrapClasses = $headerWrap.attr('class'),
		stickyMenuClasses = $header.attr('data-sticky-class'),
		responsiveMenuClasses = $header.attr('data-responsive-class'),
		defaultLogo = $('#logo').find('.standard-logo'),
		retinaLogo = $('#logo').find('.retina-logo'),
		defaultLogoImg = defaultLogo.find('img').attr('src'),
		retinaLogoImg = retinaLogo.find('img').attr('src'),
		defaultDarkLogo = defaultLogo.attr('data-dark-logo'),
		retinaDarkLogo = retinaLogo.attr('data-dark-logo'),
		defaultStickyLogo = defaultLogo.attr('data-sticky-logo'),
		retinaStickyLogo = retinaLogo.attr('data-sticky-logo'),
		defaultMobileLogo = defaultLogo.attr('data-mobile-logo'),
		retinaMobileLogo = retinaLogo.attr('data-mobile-logo');

    $(document).ready( SEMICOLON.documentOnReady.init );
    $window.on('load', SEMICOLON.documentOnLoad.init);
    $window.on( 'resize', SEMICOLON.documentOnResize.init );

});
