<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the admin guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
//$route['default_controller'] = 'home';
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


//HEADER ROUTES
$route['home'] = 'home';
$route['about'] = 'public/about';
$route['contact'] = 'public/contact';
$route['donate'] = 'public/donate';

//USER ROUTES

$route['signin'] = 'public/login/show_login';
$route['signup'] = 'public/login/show_signup';
$route['forgot-password'] = 'public/login/forgot_password';
$route['reset-password/(:any)'] = 'public/login/reset_password/$1';

//ADMIN ROUTES
$route['logout'] = 'home/logout';
$route['admin'] = 'admin/login/show_login';
$route['admin/dashboard'] = 'admin/dashboard/show_dashboard';

$route['admin/category_listings'] = 'admin/category/show_category_listings';
$route['admin/category_listings/sub_category/(:num)'] = 'admin/category/show_category_listings/$1';
$route['admin/category_listings/add_new_category'] = 'admin/category/show_new_category_form';
$route['admin/category_listings/edit_category/(:num)'] = 'admin/category/show_edit_category_form/$1';
$route['admin/category_listings/category_page/(:num)'] = 'admin/category/show_category_page/$1';
$route['admin/category_listings/add_new_category_page_section/(:num)'] = 'admin/category/show_add_new_category_page_section_form/$1';
$route['admin/category_listings/edit_category_page_section/(:num)'] = 'admin/category/show_edit_category_page_section_form/$1';
$route['admin/category_listings/category_page_section/(:num)'] = 'admin/category/show_category_page_section/$1';


$route['admin/image_media_listings'] = 'admin/media/show_image_media_listings';
$route['admin/image_media_listings/add_new_image_media'] = 'admin/media/show_new_image_media_form';

$route['admin/document_media_listings'] = 'admin/media/show_document_media_listings';
$route['admin/document_media_listings/add_new_document_media'] = 'admin/media/show_new_document_media_form';

$route['admin/newsletter_listings'] = 'admin/newsletter/show_newsletter_listings';
$route['admin/newsletter_export'] = 'admin/newsletter/newsletter_export';

$route['admin/show_first_section'] = 'admin/home/show_home_first_section';

$route['admin/home_servies_listings'] = 'admin/home/show_home_servies_listings';
$route['admin/home_servies_listings/add_new_home_service'] = 'admin/home/show_new_home_service_form';
$route['admin/home_servies_listings/edit_home_service/(:num)'] = 'admin/home/show_edit_home_service_form/$1';

$route['admin/home_what_we_do'] = 'admin/home/show_home_what_we_do';
$route['admin/home_what_we_do/add_new_feature'] = 'admin/home/show_new_feature_form';
$route['admin/home_what_we_do/edit_feature/(:num)'] = 'admin/home/show_edit_feature_form/$1';

$route['admin/show_home_third_section'] = 'admin/home/show_home_third_section';
$route['admin/home_contact_section'] = 'admin/home/show_home_contact_section';
$route['admin/home_video_section'] = 'admin/home/show_home_video_section';

$route['admin/about_content'] = 'admin/about/show_about_content';
$route['admin/about_counters'] = 'admin/about/show_about_counters';
$route['admin/add_new_counter_item'] = 'admin/about/show_add_new_counter_item';
$route['admin/edit_counter_item/(:num)'] = 'admin/about/edit_counter_item/$1';

$route['admin/contact_content'] = 'admin/contact/show_contact_content';
$route['admin/contact_form'] = 'admin/contact/show_contact_form';

$route['admin/donate_content'] = 'admin/donate/show_donate_content';

$route['admin/home_third_section_listings'] = 'admin/home/show_home_third_section_listings';
$route['admin/home_third_section_listings/add_new_testimonial'] = 'admin/home/show_add_new_third_section_form';
$route['admin/home_third_section_listings/edit_testimonial/(:num)'] = 'admin/home/show_edit_third_section_form/$1';


$route['admin/header_menus'] = 'admin/headermenus/show_all_header_menus';
$route['admin/header_menus/add'] = 'admin/headermenus/show_add_header_menus';
$route['admin/header_menus/(:num)'] = 'admin/headermenus/show_edit_header_menu/$1';

$route['admin/footer_menus'] = 'admin/footermenus/show_all_footer_menus';
$route['admin/footer_menus/add'] = 'admin/footermenus/show_add_footer_menus';
$route['admin/footer_menus/(:num)'] = 'admin/footermenus/show_edit_footer_menu/$1';

$route['admin/header'] = 'admin/settings/show_header';
$route['admin/footer'] = 'admin/settings/show_footer';

$route['api/add_new_category'] = 'api/admin/category/add_new_category';
$route['api/edit_category'] = 'api/admin/category/edit_category';
$route['api/delete_category'] = 'api/admin/category/delete_category';
$route['api/update_category_status'] = 'api/admin/category/update_category_status';
$route['api/sort_category'] = 'api/admin/category/sort_category';
$route['api/add_new_category_page_section'] = 'api/admin/category/add_new_category_page_section';
$route['api/edit_category_page_section'] = 'api/admin/category/edit_category_page_section';
$route['api/delete_category_page_section'] = 'api/admin/category/delete_category_page_section';
$route['api/sort_category_page_sections'] = 'api/admin/category/sort_category_page_sections';
$route['api/add_new_category_page_section_category'] = 'api/admin/category/add_new_category_page_section_category';
$route['api/delete_category_page_section_category'] = 'api/admin/category/delete_category_page_section_category';
$route['api/sort_category_page_section_categories'] = 'api/admin/category/sort_category_page_section_categories';
$route['api/edit_category_page_content'] = 'api/admin/category/edit_category_page_content';

$route['api/add_new_image_media'] = 'api/admin/media/add_new_image_media';
$route['api/delete_image_media'] = 'api/admin/media/delete_image_media';

$route['api/add_new_document_media'] = 'api/admin/media/add_new_document_media';
$route['api/delete_documnet_media'] = 'api/admin/media/delete_document_media';

$route['api/add_new_home_service'] = 'api/admin/home/add_new_home_service';
$route['api/edit_home_service'] = 'api/admin/home/edit_home_service';
$route['api/update_service_status'] = 'api/admin/home/update_service_status';
$route['api/delete_service'] = 'api/admin/home/delete_service';
$route['api/sort_service'] = 'api/admin/home/sort_service';

$route['api/add_new_feature_service'] = 'api/admin/home/add_new_feature_service';
$route['api/edit_home_feature'] = 'api/admin/home/edit_home_feature';
$route['api/update_feature_status'] = 'api/admin/home/update_feature_status';
$route['api/delete_feature'] = 'api/admin/home/delete_feature';

$route['api/update_what_we_do'] = 'api/admin/home/update_what_we_do';
$route['api/update_first_section'] = 'api/admin/home/update_first_section';
$route['api/update_home_third_section'] = 'api/admin/home/update_home_third_section';
$route['api/update_home_contact_section'] = 'api/admin/home/update_home_contact_section';
$route['api/update_video_section'] = 'api/admin/home/update_video_section';

$route['api/add_new_thirdsection'] = 'api/admin/home/add_new_thirdsection';
$route['api/edit_thirdsection'] = 'api/admin/home/edit_thirdsection';
$route['api/update_thirdsection_status'] = 'api/admin/home/update_thirdsection_status';
$route['api/delete_thirdsection'] = 'api/admin/home/delete_thirdsection';
$route['api/sort_thirdsection'] = 'api/admin/home/sort_thirdsection';


$route['api/update_about_content'] = 'api/admin/about/update_about_content';
$route['api/add_new_counter_item'] = 'api/admin/about/add_new_counter_item';
$route['api/edit_counter_item'] = 'api/admin/about/edit_counter_item';
$route['api/sort_counters'] = 'api/admin/about/sort_counters';
$route['api/update_counter_status'] = 'api/admin/about/update_counter_status';
$route['api/delete_counter'] = 'api/admin/about/delete_counter';

$route['api/update_contact_content'] = 'api/admin/contact/update_contact_content';
$route['api/update_contact_form'] = 'api/admin/contact/update_contact_form';

$route['api/update_donate_content'] = 'api/admin/donate/update_donate_content';

$route['api/update_home_blog_section'] = 'api/admin/home/update_home_blog_section';
$route['api/update_home_about_section'] = 'api/admin/home/update_home_about_section';
$route['api/update_home_about_section_two'] = 'api/admin/home/update_home_about_section_two';
$route['api/update_home_media_section'] = 'api/admin/home/update_home_media_section';
$route['api/update_home_interested_section'] = 'api/admin/home/update_home_interested_section';
$route['api/update_home_testimonial_section'] = 'api/admin/home/update_home_testimonial_section';
$route['api/update_home_facebook_section'] = 'api/admin/home/update_home_facebook_section';
$route['api/update_header'] = 'api/admin/settings/update_header';
$route['api/update_header_button'] = 'api/admin/settings/update_header_button';
$route['api/update_footer'] = 'api/admin/settings/update_footer';

$route['api/add_new_vacancy'] = 'api/admin/vacancy/add_new_vacancy';
$route['api/update_vacancy_status'] = 'api/admin/vacancy/update_vacancy_status';
$route['api/update_vacancy'] = 'api/admin/vacancy/edit_vacancy';
$route['api/delete_vacancy'] = 'api/admin/vacancy/delete_vacancy';
$route['api/sort_vacancy'] = 'api/admin/vacancy/sort_vacancy';
$route['api/forgot_password'] = 'api/public/login/forgot_password';
$route['api/add_footer_menu'] = 'api/admin/footermenus/add_footer_menu';
$route['api/change_footer_menu_state'] = 'api/admin/footermenus/change_menu_state';
$route['api/edit_footer_menu'] = 'api/admin/footermenus/edit_footer_menu';
$route['api/sort_footer_menus'] = 'api/admin/footermenus/sort_footer_menu';

$route['api/add_header_menu'] = 'api/admin/headermenus/add_header_menu';
$route['api/change_header_menu_state'] = 'api/admin/headermenus/change_menu_state';
$route['api/edit_header_menu'] = 'api/admin/headermenus/edit_header_menu';
$route['api/sort_header_menus'] = 'api/admin/headermenus/sort_header_menu';

$route['api/delete_footer_menus'] = 'api/admin/footermenus/delete_footer_menu';
$route['api/delete_header_menus'] = 'api/admin/headermenus/delete_header_menu';

//User PERSONAL SETTINGS AND RESET PASSWORD
$route['api/user/personal-settings'] = 'api/public/user/update_profile';
$route['api/user/reset-password'] = 'api/public/user/update_password';
$route['api/user/fcm-update-user'] = 'api/public/user/update_fcm_token';
$route['api/user/pushnotification'] = 'api/public/publicapi/push_notification';

$route['api/public-settings'] = 'api/public/publicapi/get_public_info';

$route['api/login'] = 'api/admin/login/do_login';
$route['api/user-login'] = 'api/public/login/do_login';
$route['api/register'] = 'api/public/register/do_register';
$route['api/resend-email-verification'] = 'api/public/login/resend_verification_email';
$route['api/choose_new_password'] = 'api/public/register/choose_new_password';

$route['verify-email/(:any)/(:any)'] = 'api/public/login/verify_email/$1/$2';

$route['api/add_newsletter'] = 'api/public/newsletter/add_new_newsletter';
$route['api/contactus_form'] = 'api/public/newsletter/contactus_form';

$route['(:any)'] = 'category/show_category/$1';
$route['(:any)/(:any)'] = 'category/show_category/$1/$2';
$route['(:any)/(:any)/(:any)'] = 'category/show_category/$1/$2/$3';
$route['(:any)/(:any)/(:any)/(:any)'] = 'category/show_category/$1/$2/$3/$4';
