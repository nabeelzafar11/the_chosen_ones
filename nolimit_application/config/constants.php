<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | admin, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') or define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and admin
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') or define('EXIT_USER_INPUT', 7); // invalid admin input
defined('EXIT_DATABASE') or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


define('SITE_TITLE', "The Chosen Ones");

define('SITE_EMAIL', "zeikhtech@gmail.com");
define('ADMIN_EMAIL_ADDRESS', "nabeelzafar.hsra@gmail.com");
define('DASHBOARD', 'Dashboard');
define('CATEGORY_LISTINGS', 'Category Listings');
define('ABOUT_CONTENT', 'About Content');
define('CONTACT_CONTENT', 'Contact Content');
define('DONATE_CONTENT', 'Donate Content');
define('CONTACT_FORM', 'Contact Form');
define('ABOUT_COUNTERS', 'About Counters');
define('BLOG_CATEGORY_LISTINGS', 'Blog Category Listings');
define('BLOG_LISTINGS', 'Blog Listings');
define('IMAGE_MEDIA_LISTINGS', 'Image Media Listings');
define('DOCUMENT_MEDIA_LISTINGS', 'Document Media Listings');
define('NEWSLETTER_LISTINGS', 'Newsletter Listings');
define('DASHBOARD_LISTINGS', 'Application Dashboard Listings');
define('DASHBOARD_ADD_ITEM', 'Application Dashboard Add Item');
define('DASHBOARD_EDIT_ITEM', 'Application Dashboard Edit Item');


define('POSITIVE_AFFIRMATION_DASHBOARD_LISTINGS', 'Application Positive Affirmation Dashboard Listings');
define('POSITIVE_AFFIRMATION_DASHBOARD_ADD_ITEM', 'Application Positive Affirmation Dashboard Add Item');
define('POSITIVE_AFFIRMATION_DASHBOARD_EDIT_ITEM', 'Application Positive Affirmation Dashboard Edit Item');

define('BUCKET_LIST', 'Bucket List Items');
define('BUCKETLIST_ADD_ITEM', 'Bucket List Add Item');
define('BUCKETLIT_EDIT_ITEM', 'Bucket List Edit Item');

define('ABUNDANCE_LIST', 'Abundance List Items');
define('ABUNDANCE_ADD_ITEM', 'Abundance List Add Item');
define('ABUNDANCE_EDIT_ITEM', 'Abundance List Edit Item');

define('HEALTHANDBEAUTY_LIST', 'Health & Beauty List Items');
define('HEALTHANDBEAUTY_ADD_ITEM', 'Health & Beauty List Add Item');
define('HEALTHANDBEAUTY_EDIT_ITEM', 'Health & Beauty List Edit Item');

define('LOVE_LIST', 'Love List Items');
define('LOVE_ADD_ITEM', 'Love List Add Item');
define('LOVE_EDIT_ITEM', 'Love List Edit Item');

define('SELLOVECARE_DASHBOARD_LISTINGS', 'Application Self Love/Self Care Dashboard');
define('SELLOVECARE_DASHBOARD_LISTINGS_EDIT_ITEM', 'Application Self Love/Self Care Dashboard Edit Item');

define('SELFLOVECARE_LIST', 'Self Love/Self Care List Items');
define('SELFLOVECARE_ADD_ITEM', 'Self Love/Self Care List Add Item');
define('SELFLOVECARE_EDIT_ITEM', 'Self Love/Self Care List Edit Item');

define('DAILYAFFIRMATION_LISTINGS', 'Calendar - Daily Affirmation Monthly Listing');
define('CALENDAR_DAILYAFFIRMATION_LISTINGS_EDIT_ITEM', 'Calendar - Daily Affirmation Monthly Listing / Edit');

define('PERSONAL_AFFIRMATION_LIST', 'My Personal Affirmation List Items');
define('PERSONAL_AFFIRMATION_ADD_ITEM', 'My Personal Affirmation List Add Item');
define('PERSONAL_AFFIRMATION_EDIT_ITEM', 'My Personal Affirmation List Edit Item');


define('CAREER_JOIN_NOW', 'Career Join Now');
define('CAREER_SUPPORT_ROLE', 'Career Support Role');
define('CAREER_WHY_WORK', 'Career Why Work');
define('CAREER_WORK_EXP', 'Career Work Expectations');
define('CAREER_RESOURCES', 'Career Resources');
define('VACANCY', 'Vacancy');
define('HEADER_MENUS', 'Header Menus');
define('FOOTER_MENUS', 'Footer Menus');
define('MENU_LISTINGS', 'Menu Listings');
define('CATEGORY_PAGE_LISTINGS', 'Category Page Listings');
define('HOME_PAGE', 'Home Page');
define('HOME_FIRST_SECTION', 'First Section');
define('HOME_SERVICES', 'Home Services');
define('HOME_WHAT_WE_DO', 'What We DO');
define('CONTACT_SECTION', 'Contact Section');
define('VIDEO_SECTION', 'Video Section');
define('HOME_SLIDER', 'Home Slider');
define('HOME_BLOG_SECTION', 'Home Blog Section');
define('HOME_ABOUT_SECTION', 'Home About Section');
define('HOME_ABOUT_SECTION_TWO', 'Home About Section Two');
define('HOME_MEDIA_SECTION', 'Home Media Section');
define('HOME_INTERESTED_SECTION', 'Home Interested Section');
define('HOME_TESTIMONIAL', 'Home Testimonial');
define('HOME_TESTIMONIAL_THIRD_SECTION', 'Home Third Section');

define('HOME_TESTIMONIAL_SECTION', 'Home Testimonial Section');
define('HOME_FOURTH_SECTION', 'Home Fourth Section');

define('HEADER', 'Header');
define('FOOTER', 'Footer');
define('PROFILE', 'Profile');

//user constants
define('DEVELOPER', '1');
define('ADMIN', '2');
define('USER', '3');

define('INVALID_REQUEST', 'Invalid request.');
define('INVALID_USERNAME_OR_PASSWORD', 'Invalid username or password.');
define('INACTIVE_ACCOUNT', 'Your account is disabled. Please contact to our support for more details.');
define('DELETED_ACCOUNT', 'Your account is deleted. Please contact to our support for more details.');
define('LOGIN_SOURCE_FORM', 'form');

define('ALREADY_EXISTS', 'Already exists.');
define('SUCCESSFULLY_ADDED', 'Successfully added.');
define('FAILED_ADDED', 'Failed to add.');
define('SUCCESSFULLY_UPDATED', 'Successfully updated.');
define('FAILED_UPDATED', 'Failed to update.');
define('SUCCESSFULLY_DELETED', 'Successfully deleted.');
define('FAILED_DELETED', 'Failed to delete.');
define('SUCCESSFULLY_DETELED', 'List item has been deleted.');
define('EVENT_SUCCESSFULLY_DETELED', 'Event has been deleted.');
define('FUTURE_EVENT_SUCCESSFULLY_DETELED', 'All future events have been deleted.');
define('SUCCESSFULLY_CHANGED_STATUS', 'Successfully changed status.');
define('FAILED_CHANGED_STATUS', 'Failed to change status.');

define('EMAIL_NOT_EXIST', 'No user with this email address exists in our system.');
define('SENT_RESET_PASSWORD_LINK', 'Reset password link is sent to your email.');
define('RESET_PASSWORD_FAILED', 'Failed to reset password.');

if ($_SERVER['HTTP_HOST'] == "localhost" || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
  define('ASSETS_PATH', '/the_chosen_ones/public/assets/');
  define('API_PATH', '/the_chosen_ones/public/');
  define('MEDIA_PATH', '/the_chosen_ones/public/assets/uploaded_media/');
  define('FACEBOOK_APP_ID', '1896495133798690');
  define('FACEBOOK_APP_SECRET', '6fce4c29d9302a3b06e6a4100d2e70d2');
} else {
  define('ASSETS_PATH', '/assets/');
  define('ASSETS_PATH2', '/assets2/');
  define('API_PATH', '/');
  define('MEDIA_PATH', '/assets/uploaded_media/');
  define('FACEBOOK_APP_ID', '1255816521218857');
  define('FACEBOOK_APP_SECRET', '2c6a970de654e00723a32dd6a739e7c0');
}
