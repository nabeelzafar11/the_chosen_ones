<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="<?php echo MEDIA_PATH . $header->favicon; ?>">
    <meta name="title" content="<?php echo $header->seoTitle; ?>">
    <title><?php echo $header->siteTitle; ?></title>
    <meta name="description" content="<?php echo $header->seoDescription; ?>">
    <meta name="keywords" content="<?php echo $header->seoKeywords; ?>">
    <meta charset="UTF-8">

    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>stomvtheme/css/vendor/vendor.min.css">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>stomvtheme/css/plugins/plugins.min.css">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>stomvtheme/css/style.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH; ?>stomvtheme/css/plugins/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH; ?>stomvtheme/css/plugins/slick/slick-theme.css" />
    <script>
        var base_url = "<?php echo base_url(); ?>";
        var ASSETS_PATH = "<?php echo ASSETS_PATH; ?>";
    </script>
</head>