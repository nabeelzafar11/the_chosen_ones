<body>
    <div id="myLoader" style="display: none;">
    </div>
    <!-- header start html -->
    <header class="header-area header-sticky" style="background: #ecf0f1;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 d-flex align-items-center">
                    <div class="header__logo">
                        <div class="logo">
                            <a href="<?php echo base_url(); ?>"><img width="40%" height="40%" src="<?php echo MEDIA_PATH . $header->headerLogo; ?>" alt=""></a>
                        </div>
                    </div>
                    <div class="header-right">
                        <div class="header__navigation menu-style-three d-none d-lg-block">
                            <nav class="navigation-menu">
                                <ul>
                                    <?php foreach ($headerMenus as $hm) { ?>
                                        <li class="has-children has-children--multilevel-submenu">
                                            <?php
                                            //if  $hm->footerMenuCategoryId == 0 then its not a dynamic page
                                            ?>
                                            <a href="<?php echo $hm->footerMenuCategoryId == 0 ? base_url() .  $hm->footerMenuHref : $hm->footerMenuHref; ?>"><span><?php echo $hm->footerMenuName; ?></span></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </nav>

                        </div>

                        <div class="header-btn text-right d-none d-sm-block ml-lg-4">
                            <a class="btn-circle btn-default btn" href="<?php echo base_url("donate"); ?>"><?php echo $header->menuButtonText; ?></a>
                        </div>

                        <!-- mobile menu -->
                        <div class="mobile-navigation-icon d-block d-lg-none" id="mobile-menu-trigger">
                            <i></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <?php
    if ($this->session->flashdata('notification')) {
        $notification = $this->session->flashdata('notification');
    ?>
        <div class="ff-bold alert alert-<?php echo $notification["status"] ?>">
            <p><?php echo $notification["message"]; ?></p>
        </div>
    <?php } ?>