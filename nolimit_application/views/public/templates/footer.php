<!-- footer start -->
<footer class="footer-area bg-footer">
    <div class="footer-top section-space--ptb_80 section-pb text-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="widget-footer mt-30">
                        <div class="footer-title">
                            <h6><?php echo $footer->addressText; ?></h6>
                        </div>
                        <div class="footer-contents">
                            <ul>
                                <li><span></span><?php echo $footer->address; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="widget-footer mt-30">
                        <div class="footer-title">
                            <h6><?php echo $footer->relatedLinksLeft; ?></h6>
                        </div>
                        <div class="footer-contents">
                            <ul>
                                <?php foreach ($headerMenus as $footerMenu) { ?>
                                    <li><a href="<?php echo!empty($footerMenu->footerMenuCategoryId) || $footerMenu->footerMenuCategoryId != "0" ? $footerMenu->footerMenuHref : base_url() . $footerMenu->footerMenuHref; ?>"><?php echo $footerMenu->footerMenuName; ?></a></li>
                                <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6"></div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="widget-footer mt-30">
                        <div class="footer-title">
                            <h6><?php echo $footer->relatedLinksRight; ?></h6>
                        </div>
                        <div class="footer-logo mb-15">
                            <a href="home"><img width="50%" src="<?php echo MEDIA_PATH . $footer->footerLogo; ?>" alt="" /></a>
                        </div>
                        <div class="footer-contents">
                            <p class="footer-contents"><?php echo $footer->subscribeNewsletterText; ?></p>
                            <form id="newsletter_form" action="<?php echo base_url('api/add_newsletter'); ?>" method="post" onsubmit="return false;">
                                <div class="newsletter-box">
                                    <input name="subscriberEmail" type="text" placeholder="<?php echo $footer->emailPlaceholder; ?>" />
                                    <button type="submit"><i class="flaticon-paper-plane"></i></button>
                                </div>
                            </form>

                            <ul class="footer-social-share mt-20">
                                <i class="flaticon-github"></i>
                                <?php if ($footer->facebook) { ?>
                                    <li>
                                        <a href="<?php echo $footer->facebook; ?>"><i class="flaticon-facebook"></i></a>
                                    </li>
                                <?php } ?>
                                <?php if ($footer->twitter) { ?>
                                    <li>
                                        <a href="<?php echo $footer->twitter; ?>"><i class="flaticon-twitter"></i></a>
                                    </li>
                                <?php } ?>
                                <?php if ($footer->youtube) { ?>
                                    <li>
                                        <a href="<?php echo $footer->youtube; ?>"><i class="flaticon-youtube"></i></a>
                                    </li>
                                <?php } ?>
                                <?php if ($footer->instagram) { ?>
                                    <li>
                                        <a href="<?php echo $footer->instagram; ?>"><i class="flaticon-instagram"></i></a>
                                    </li>
                                <?php } ?>
                                <?php if ($footer->pinterest) { ?>
                                    <li>
                                        <a href="<?php echo $footer->pinterest; ?>"><i class="flaticon-pinterest-social-logo"></i></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copy-right-box">
                        <p class="text-white footer-contents">
                            <?php echo str_replace("%YEAR%", date("Y"), $footer->copyright); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--====================  scroll top ====================-->
<a href="#" class="scroll-top" id="scroll-top">
    <i class="arrow-top flaticon-up-arrow"></i>
    <i class="arrow-bottom flaticon-up-arrow"></i>
</a>
<!--====================  End of scroll top  ====================-->


<!--====================  mobile menu overlay ====================-->
<div class="mobile-menu-overlay" id="mobile-menu-overlay">
    <div class="mobile-menu-overlay__inner">
        <div class="mobile-menu-overlay__header">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-md-6 col-8">
                        <!-- logo -->
                        <div class="logo">
                            <a href="<?php echo base_url();?>">
                                <img width="50%" src="<?php echo MEDIA_PATH . $header->headerLogo; ?>" class="img-fluid" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-4">
                        <!-- mobile menu content -->
                        <div class="mobile-menu-content text-right">
                            <span class="mobile-navigation-close-icon" id="mobile-menu-close-trigger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-menu-overlay__body">
            <nav class="offcanvas-navigation">
                <ul>
                    <?php foreach ($headerMenus as $hm) { ?>
                     <li class="has-children">
                        <a href="<?php echo $hm->footerMenuCategoryId == 0 ? base_url() .  $hm->footerMenuHref : $hm->footerMenuHref; ?>"><?php echo $hm->footerMenuName; ?></a>
                    </li>
                    <?php } ?>
                     <li class="has-children">
                        <a href="<?php echo base_url("donate"); ?>">Donate</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<!--====================  End of mobile menu overlay  ====================-->