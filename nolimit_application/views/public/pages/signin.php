<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="sign-in">
                <h1 class="text-center text-uppercase ff-exblod font-weight-bold">Sign In</h1>
            </div>
        </div>
    </div>
    <hr>
</section>
<!-- login section -->
<section class="login-form">
    <div class="woraper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6 ">
                    <form id="email_verification_form" method="post" action="<?php echo base_url("api/resend-email-verification"); ?>" onsubmit="return false;">
                        <input name="token" value="" id="token" style="display:none;" />
                    </form>
                    <form id="signin_form" name="signin_form" method="post" action="<?php echo base_url("api/user-login"); ?>" onsubmit="return false;">
                        <div class="form p-4">
                            <div class="alert"></div>
                            <div class="form-group">
                                <label for="email" class="ff-bold">Email</label>
                                <input placeholder="Please enter your email address" name="email" type="email" class="form-control-1" id="email">
                            </div>
                            <div class="form-group" style="position: relative;">
                                <label for="pwd" class="ff-bold">Password</label>
                                <input placeholder="Please enter your password" id="password-field" type="password" class="form-control-1" name="password">
                                <span toggle="#password-field" style="position: absolute; top: 50px;right: 20px" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            </div>
                            <a href="<?php echo base_url("forgot-password"); ?>"> <button type="button" class="btn btn1 float-right">Forgot password</button></a>

                        </div>
                        <div class="button text-center">
                            <input type="submit" class="btn btn-light btn-block mb-3" value="Login" />
                            <span class="butn-text" class="">New?<a href="<?php echo base_url("signup"); ?>" class="pt-3 text-p ml-2 ff-bold mt-5">Sign Up</a></span>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo ASSETS_PATH . "js/custom/public/signin.js" ?>"></script>