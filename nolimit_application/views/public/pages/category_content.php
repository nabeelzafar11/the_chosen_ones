<div class="breadcrumb-area bg-overlay-black">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="breadcrumb-title text-white"><?php echo $currentCategory->categoryName; ?></h3>
				<ul class="breadcrumb-list">
					<li class="breadcrumb-item"><a href="<?php echo base_url("home"); ?>"><?php echo $currentCategory->categoryHomeText; ?></a></li>
					<li class="breadcrumb-item active"><?php echo $currentCategory->categoryName; ?></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="site-wrapper-reveal">
	<div class="church-about-area section-space--ptb_120 ">
		<div class="container text-center">
			<div class="wraper py-5">
				<div class="row align-self-start">
					<?php if ($currentCategory->categoryShortDescription) { ?>
						<?php echo html_entity_decode($currentCategory->categoryShortDescription); ?>
						<div class="col-lg-9 col-md-12 text-left">
							<?php echo html_entity_decode($currentCategory->categoryDescription); ?>
						</div>
					<?php } else { ?>
						<div class="col-lg-12 col-md-12 text-left">
							<?php echo html_entity_decode($currentCategory->categoryDescription); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>