<div class="breadcrumb-area bg-overlay-black">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="breadcrumb-title text-white"><?php echo $homeData->donate_header; ?></h3>
                <ul class="breadcrumb-list">
                    <li class="breadcrumb-item"><a href="<?php echo base_url("home"); ?>"><?php echo $homeData->donate_homeText; ?></a></li>
                    <li class="breadcrumb-item active"><?php echo $homeData->donate_header; ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="site-wrapper-reveal">
    <div class="donation-area section-space--pb_120 section-space--pt_90">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-12 col-md-12">
                    <div class="single-event-wrap mt-40">
                        <div class="donation-content mt-30">
                            <div class="content-title">
                                <h4 class="mb-15"><?php echo $homeData->donate_title; ?></h4>
                            </div>
                            <div class="progress-wrap-muslim section-space--mt_40">
                                <div class="progress-bar--two">
                                    <div class="progress-charts">
                                        <div class="progress">
                                            <div class="progress-bar wow fadeInLeft animated" data-wow-duration="0.5s" data-wow-delay=".3s" role="progressbar" style="width: 60%; visibility: visible; animation-duration: 0.5s; animation-delay: 0.3s;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                                                <p class="percent-label">$4500.00</p>
                                            </div>
                                        </div>
                                        <div class="progress_sold_av">
                                            <p class="start-sold">$00.00</p>
                                            <!--<p class="sold-av">$8500.00 <br> <span>OUR GOAL</span></p>-->
                                        </div>
                                    </div>
                                </div>
                                <p><?php echo html_entity_decode($homeData->donate_description); ?></p>
                                <div class="donate-btn">
                                    <a href="#" class="btn donate-btn"><?php echo $homeData->donate_donateNow; ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo ASSETS_PATH; ?>js/custom/public/donate.js"></script>