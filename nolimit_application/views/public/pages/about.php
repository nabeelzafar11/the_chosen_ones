<div class="breadcrumb-area bg-overlay-black">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="breadcrumb-title text-white"><?php echo $homeData->aboutPageTitle; ?></h3>
                <ul class="breadcrumb-list">
                    <li class="breadcrumb-item"><a href="<?php echo base_url("home"); ?>"><?php echo $homeData->aboutHomeText; ?></a></li>
                    <li class="breadcrumb-item active"><?php echo $homeData->aboutPageTitle; ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="site-wrapper-reveal">
    <div class="church-about-area section-space--ptb_120 ">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="about-tai-content">
                        <div class="section-title-wrap">
                            <h3 class="section-title--two  left-style mb-30">
                                <?php echo $homeData->aboutTitle; ?>
                            </h3>
                        </div>
                        <p>
                            <?php echo html_entity_decode($homeData->aboutDescription); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if (count($counters) > 0) { ?>
        <div class="fun-fact-wrapper section-space--pb_90">
            <div class="container">
                <div class="fun-fact-style-one row slide-class-4-nodot">
                    <?php foreach ($counters as $counter) { ?>
                        <div class="single-fun-fact wow move-up">
                            <div class="fun-fact--one text-center" style="text-align: center; margin-bottom: 20px">
                                <img src="<?php echo MEDIA_PATH . $counter->counterImage; ?>" style="display: inline-block;" class="img-fluid" alt="Image" />
                                <div class="content mt-20">
                                    <h6 class="fun-fact__text mb-10"><?php echo $counter->counterTitle; ?></h6>
                                    <h2 class="fun-fact__count">
                                        <span class="counter"><?php echo $counter->counterCount; ?></span><?php echo $counter->counterOperator; ?>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="about-video-area section-space--pb_120">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title-wrap text-center section-space--mb_40">
                        <h3 class="section-title--two  center-style mb-30">
                            <?php echo $homeData->aboutDetailofText; ?>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="about-video-box church-testmonial-bg bg-overlay-black">
                <div class="col-lg-6 ml-auto mr-auto">
                    <div class="video-content-wrap text-center">
                        <div class="icon">
                            <a href="<?php echo $homeData->aboutVideoLink; ?>" class="video-link popup-youtube"><img src="<?php echo MEDIA_PATH . $homeData->home_video_play_icon; ?>" alt="Video Icon" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo ASSETS_PATH; ?>js/custom/public/home.js"></script>