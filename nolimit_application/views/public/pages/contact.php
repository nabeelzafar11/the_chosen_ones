<div class="breadcrumb-area bg-overlay-black">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="breadcrumb-title text-white"><?php echo $homeData->contact_pageTitle; ?></h3>
                <ul class="breadcrumb-list">
                    <li class="breadcrumb-item"><a href="<?php echo base_url("home"); ?>"><?php echo $homeData->contact_homeText; ?></a></li>
                    <li class="breadcrumb-item active"><?php echo $homeData->contact_pageTitle; ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="site-wrapper-reveal">

    <div class="contact-page-wrapper section-space--pt_120">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-contact-info">
                        <div class="contact-icon">
                            <i class="flaticon-placeholder"></i>
                        </div>
                        <div class="contact-info">
                            <h4>Address</h4>
                            <p><?php echo $homeData->contact_address; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-contact-info">
                        <div class="contact-icon">
                            <i class="flaticon-call"></i>
                        </div>
                        <div class="contact-info">
                            <h4>Phone</h4>
                            <p><?php echo $homeData->contact_phone1; ?><br>
                                <?php echo $homeData->contact_phone2; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-contact-info">
                        <div class="contact-icon">
                            <i class="flaticon-paper-plane-1"></i>
                        </div>
                        <div class="contact-info">
                            <h4>Mail</h4>
                            <p><?php echo $homeData->contact_mail1; ?><br>
                                <?php echo $homeData->contact_mail2; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="contact-form-area section-space--ptb_120">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <iframe src="<?php echo $homeData->contact_mapLoc; ?>" width="600" height="500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                    <div class="col-lg-6">
                        <div class="contact-form-wrap ml-lg-5">
                            <h3 class="title mb-40"><?php echo $homeData->contact_formTitle; ?></h3>
                            <form id="contact_form" action="<?php echo base_url('api/contactus_form'); ?>" method="post" onsubmit="return false;">
                                <div class="contact-form__one">
                                    <div class="contact-input">
                                        <label><?php echo $homeData->contact_name_l; ?></label>
                                        <div class="contact-inner">
                                            <input name="con_name" type="text" placeholder="<?php echo $homeData->contact_name_p; ?>">
                                        </div>
                                    </div>

                                    <div class="contact-input">
                                        <label><?php echo $homeData->contact_phone_l; ?></label>
                                        <div class="contact-inner">
                                            <input name="con_phone" type="text" placeholder="<?php echo $homeData->contact_phone_p; ?>">
                                        </div>
                                    </div>

                                    <div class="contact-input">
                                        <label><?php echo $homeData->contact_email_l; ?></label>
                                        <div class="contact-inner">
                                            <input name="con_email" type="email" placeholder="<?php echo $homeData->contact_email_p; ?>">
                                        </div>
                                    </div>
                                    <div class="contact-input">
                                        <label><?php echo $homeData->contact_message_l; ?></label>
                                        <div class="contact-inner">
                                            <textarea name="con_message" style="border-radius: 25px; border: 1px solid #ddd; padding: 10px 20px; width: 100%; font-style: italic;" rows="4" cols="50" placeholder="<?php echo $homeData->contact_message_p; ?>"></textarea>
                                        </div>
                                    </div>
                                    <div class="submit-input">
                                        <button class="submit-btn" type="submit"><?php echo $homeData->contact_submitBtn; ?></button>
                                        <p class="form-messege"></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo ASSETS_PATH; ?>js/custom/public/contact.js"></script>