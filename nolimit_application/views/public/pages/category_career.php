<section class="main-section" data-background="<?php echo MEDIA_PATH . $currentCategory->categoryImage; ?>">
    <div class="overlay" data-overlay="#6a419a"></div>
    <div class="container">
        <div class="section-content">
            <div class="title light mb-0 wow fadeInUp">
                <h2><?php echo $currentCategory->categoryName; ?></h2>
            </div>
        </div>
    </div>
</section>


<section class="career-join-team my-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="join-team-content">
                    <!-- <div class="join-team-content-fade-text">TEAM</div> -->
                    <h1 class="join-team-content-heading"><?php echo $careerPage->careerPageJoinNowHeading; ?></h1>
                    <p class="join-team-content-text">
                        <?php echo $careerPage->careerPageJoinNowText; ?>
                    </p>
                    <div class="btn-wrapper py-3">
                        <a href="#vacancies" class="button secondary-button rounded-button"><?php echo $careerPage->careerPageJoinNowBtnText; ?></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="d-flex align-items-center h-100">
                    <div class="join-team-image">
                        <img src="<?php echo MEDIA_PATH . $careerPage->careerPageJoinNowImage; ?>" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="career-support">
    <div class="container">
        <div class="career-support-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="support-heading">
                        <h1><?php echo $careerPage->careerPageSupportHeading; ?></h1>
                    </div>
                </div>

                <?php if ($careerPage->careerPageSupportRole1 != '') {?>
                    <div class="col-md-4 col-sm-6">
                        <div class="feature-box style-2 wow fadeInUp d-block">
                            <p class="">
                                <?php echo $careerPage->careerPageSupportRole1; ?>
                            </p>
                        </div>
                    </div>
                <?php }?>
                <?php if ($careerPage->careerPageSupportRole2 != '') {?>
                    <div class="col-md-4 col-sm-6">
                        <div class="feature-box style-2 wow fadeInUp d-block">
                            <p class="">
                                <?php echo $careerPage->careerPageSupportRole2; ?>
                            </p>
                        </div>
                    </div>
                <?php }?>
                <?php if ($careerPage->careerPageSupportRole3 != '') {?>
                    <div class="col-md-4 col-sm-6">
                        <div class="feature-box style-2 wow fadeInUp d-block">
                            <p class="">
                                <?php echo $careerPage->careerPageSupportRole3; ?>
                            </p>
                        </div>
                    </div>
                <?php }?>
                <?php if ($careerPage->careerPageSupportRole4 != '') {?>
                    <div class="col-md-4 col-sm-6">
                        <div class="feature-box style-2 wow fadeInUp mt-3 d-block">
                            <p class="">
                                <?php echo $careerPage->careerPageSupportRole4; ?>
                            </p>
                        </div>
                    </div>
                <?php }?>
                <?php if ($careerPage->careerPageSupportRole5 != '') {?>
                    <div class="col-md-4 col-sm-6">
                        <div class="feature-box style-2 wow fadeInUp mt-3 d-block">
                            <p class="">
                                <?php echo $careerPage->careerPageSupportRole5; ?>
                            </p>
                        </div>
                    </div>
                <?php }?>
                <?php if ($careerPage->careerPageSupportRole6 != '') {?>
                    <div class="col-md-4 col-sm-6">
                        <div class="feature-box style-2 wow fadeInUp mt-3 d-block">
                            <p class="">
                                <?php echo $careerPage->careerPageSupportRole6; ?>
                            </p>
                        </div>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
</section>

<section class="career-work-for-us my-4">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="swiper-container career-testimonial-slider">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <?php if (count($tes) > 0) { ?>
                            <?php foreach($tes as $t) { ?>
                                <div class="swiper-slide career-testimonial-slide">
                                    <div class="career-testimonial-text">
                                        <p><?php echo $t->testimonialComment; ?></p>
                                    </div>
                                    <div class="career-testimonial-person">
                                        <h4 class="font-weight-bold mb-0"><?php echo $t->testimonialName; ?></h4>
                                        <p clsas="" style="margin: 0px;"><?php echo $t->testimonialOccupation; ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>

                    <!-- If we need navigation buttons -->
                    <div class="career-swiper-button-prev swiper-button-prev">
                    </div>
                    <div class="career-swiper-button-next swiper-button-next">
                    </div>


                    <!-- If we need scrollbar -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="work-content">
                    <div class="work-content-heading">
                        <h1><?php echo $careerPage->careerPageWorkHeading; ?></h1>
                    </div>
                    <div class="work-content-text">
                        <p><?php echo $careerPage->careerPageWorkText; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="vacancies" id="vacancies">

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">We are Hiring in Melbourne, Darwin & Tiwi Islands.</h1>
            </div>
            <div class="col-sm-4">
                <div class="sidebar-widget sticky-top vertical-menu wow fadeInUp" style="visibility: visible; animation-name: fadeInUp; z-index: 1;">
                    <nav class="nav-tree">
                        <ul>
                            <li class="sub-menu active"><a id="melbournBtn">Melbourne</a></li>
                            <li class="sub-menu"><a id="darwinBtn">Darwin</a></li>
                            <li class="sub-menu"><a id="tiwiBtn">Tiwi Islands</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="col-sm-8">
                <div class="card shadow-sm p-3" style="border: none;">
                    <div class="vacancy-area-heading"><h1>Open Vacancies in <span class="gradient-text" id="vacancy-area-heading">Melbourne</span></h1></div>
                    <div id="vacancies">
                        <?php if(count($vac) > 0) { ?>
                            <?php $num = 1; ?>
                            <?php foreach($vac as $v) { ?>
                                <!--wow flipInX-->    <div class="vacancy  <?php echo 'area-'.$v->vacancyArea?>">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="vacancy-details">
                                                <h2 class="vacancy-num"><?php echo $num;?></h2>
                                                <h4 class="vacancy-heading"><?php echo $v->vacancyName; ?></h4>
                                                <p class="vacancy-exp">Experience: <?php echo $v->vacancyExp; ?></p>
                                                <p class="vacancy-qual">Qualification: <?php echo $v->vacancyQual; ?></p>
                                                <a href="mailto:<?php echo $v->vacancyApplyEmail; ?>" class=" btn btn-primary vacancy-apply-btn my-3">Apply</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="vacancy-skills">
                                                <h3 class="skills-heading">Skills</h3>
                                                <ul class="skill-list pl-3">
                                                    <?php echo ($v->vacancySkill1) ? '<li>' . $v->vacancySkill1 .'</li>' : ''; ?>
                                                    <?php echo ($v->vacancySkill2) ? '<li>' . $v->vacancySkill2 .'</li>' : ''; ?>
                                                    <?php echo ($v->vacancySkill3) ? '<li>' . $v->vacancySkill3 .'</li>' : ''; ?>
                                                    <?php echo ($v->vacancySkill4) ? '<li>' . $v->vacancySkill4 .'</li>' : ''; ?>
                                                    <?php echo ($v->vacancySkill5) ? '<li>' . $v->vacancySkill5 .'</li>' : ''; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                </div>
                                <?php $num++; ?>
                            <?php } ?>
                        <?php } ?>


                    </div>
                    <div class="no-vacancies">
                        <h1>No Vacancies Available</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>

</script>
<section class="career-join-team my-5 py-5" >
    <div class="container">
        <div class="row">
            <div class="col-md-6 mt-3">
                <div class="d-flex align-items-center h-100">
                    <div class="join-team-image">
                        <img src="<?php echo MEDIA_PATH . $careerPage->careerPageWorkExpImage; ?>" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="join-team-content mt-3">
                    <h1 class="join-team-content-heading"><?php echo $careerPage->careerPageWorkExpHeading; ?></h1>
                    <p class="join-team-content-text">
                        <?php echo $careerPage->careerPageWorkExpText; ?>
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="resources">
    <div class="container">
        <?php echo $careerPage->careerPageResourcesSection; ?>
    </div>
</section>


<section class="page-section">
    <div class="container">
        <div class="section-title">
            <h2>You Might Be Interested In</h2>
        </div>

        <div class="row">
            <?php if ($interestedCategoryOne) { ?>
                <div class="col-md-4 col-sm-12">
                    <div class="feature-box style-2 wow fadeInUp">
                        <div class="feature-title">
                            <h4><?php echo $interestedCategoryOne->categoryName; ?></h4>
                        </div>

                        <div class="feature-content">
                            <p><?php echo $interestedCategoryOne->categoryDescription; ?></p>
                        </div>

                        <div class="read-more"><a href="<?php echo $interestedCategoryOne->categorySlug; ?>">Read
                                More</a></div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($interestedCategoryTwo) { ?>
                <div class="col-md-4 col-sm-12">
                    <div class="feature-box style-2 wow fadeInUp">
                        <div class="feature-title">
                            <h4><?php echo $interestedCategoryTwo->categoryName; ?></h4>
                        </div>

                        <div class="feature-content">
                            <p><?php echo $interestedCategoryTwo->categoryDescription; ?></p>
                        </div>

                        <div class="read-more"><a href="<?php echo $interestedCategoryTwo->categorySlug; ?>">Read
                                More</a></div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($interestedCategoryThree) { ?>
                <div class="col-md-4 col-sm-12">
                    <div class="feature-box style-2 wow fadeInUp">
                        <div class="feature-title">
                            <h4><?php echo $interestedCategoryThree->categoryName; ?></h4>
                        </div>

                        <div class="feature-content">
                            <p><?php echo $interestedCategoryThree->categoryDescription; ?></p>
                        </div>

                        <div class="read-more"><a href="<?php echo $interestedCategoryThree->categorySlug; ?>">Read
                                More</a></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>