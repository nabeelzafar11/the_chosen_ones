
<section class="sign-in">
    <div class="container">
        <div class="main-tital">
            <div class="sign-in">
                <h1 class="text-center text-uppercase ff-exblod font-weight-bold">Choose a New Password</h1>
            </div>
        </div>
    </div>
    <hr>
</section>
<!-- main title -->
<!-- login section -->
<section class="login-form">
    <div class="woraper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6 ">
                    <?php if (!empty($results->email) && !empty($resetLink)) { ?>
                        <form id="choose_new_password" name="choose_new_password" method="post" action="<?php echo base_url("api/choose_new_password"); ?>" onsubmit="return false;">
                            <div class="form-signup p-4">
                                <div class="alert"></div>
                                <div class="form-group">
                                    <label for="pwd" class="ff-bold">New Password</label>
                                    <input type="password" class="form-control" id="password" name="password" >
                                </div>
                                <div class="form-group">
                                    <label for="pwd" class="ff-bold">Retype New Password</label>
                                    <input type="password" class="form-control" id="pwd1" name="confirmPassword" >
                                </div>
                            </div>
                            <div class="button text-center">
                                <input type="submit" class="btn btn-light ff-bold btn-block mb-3" value="Submit" />
                                <input type="hidden" value="<?php echo $results->email ?>" name="email" />
                                <input type="hidden" value="<?php echo $resetLink ?>" name="resetLink" />

                            </div>
                        </form>
                    <?php } else { ?>
                        <div class="alert alert-danger">
                            <p>Oops! Either the link is invalid or has been Expired!</p>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>
</section>
<!-- login secton -->
<script src="<?php echo ASSETS_PATH . "js/custom/public/choose_new_password.js" ?>"></script>