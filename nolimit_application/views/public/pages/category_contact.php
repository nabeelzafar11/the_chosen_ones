<section class="main-section" data-background="<?php echo MEDIA_PATH . $currentCategory->categoryImage; ?>">
    <div class="overlay" data-overlay="#6a419a"></div>
    <div class="container">
        <div class="section-content">
            <div class="title light mb-0 wow fadeInUp">
                <h2><?php echo $currentCategory->categoryName; ?></h2>
            </div>
        </div>
    </div>
</section>

<section class="page-section" data-bg-color="#f7f7f7">
    <div class="container">
        <div class="inner-section">
            <div class="sidebar">
                <div class="sidebar-widget vertical-menu wow fadeInUp">
                    <nav class="nav-tree">
                        <?php if (count($sideCategories)) { ?>
                            <ul>
                                <?php foreach ($sideCategories as $key => $value) { ?>
                                    <li <?php if(isset($categoryArray[0]) && $value->categoryId == $categoryArray[0]){ ?> class="active" <?php array_shift($categoryArray); } ?>>
                                        <a href="<?php echo $value->categorySlug; ?>"><?php echo $value->categoryName; ?></a>
                                        <?php if (count($value->sideCategories)) { ?>
                                            <ul>
                                                <?php foreach ($value->sideCategories as $key1 => $value1) { ?>
                                                    <li <?php if(isset($categoryArray[0]) && $value1->categoryId == $categoryArray[0]){ ?> class="active" <?php array_shift($categoryArray); } ?>>
                                                        <a href="<?php echo $value1->categorySlug; ?>"><?php echo $value1->categoryName; ?></a>
                                                        <?php if (count($value1->sideCategories)) { ?>
                                                            <ul>
                                                                <?php foreach ($value1->sideCategories as $key2 => $value2) { ?>
                                                                    <li <?php if(isset($categoryArray[0]) && $value2->categoryId == $categoryArray[0]){ ?> class="active" <?php array_shift($categoryArray); } ?>>
                                                                        <a href="<?php echo $value2->categorySlug; ?>"><?php echo $value2->categoryName; ?></a>
                                                                        <?php if (count($value2->sideCategories)) { ?>
                                                                            <ul>
                                                                                <?php foreach ($value2->sideCategories as $key3 => $value3) { ?>
                                                                                    <li <?php if(isset($categoryArray[0]) && $value3->categoryId == $categoryArray[0]){ ?> class="active" <?php array_shift($categoryArray); } ?>>
                                                                                        <a href="<?php echo $value3->categorySlug; ?>"><?php echo $value3->categoryName; ?></a>
                                                                                    </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                        <?php } ?>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </nav>
                </div>
            </div>

            <div class="inner-content">
                <div class="title">
                    <h1>Contact Us</h1>
                </div>

                <form class="contact-form">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Enter your Full Name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter your Email Address">
                    </div>
                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input type="text" class="form-control" id="subject" placeholder="Enter Subject">
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea class="form-control" id="message" rows="3" placeholder="Enter Message"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="button primary-button rounded-button">Submit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>

<section class="page-section">
    <div class="container">
        <div class="section-title">
            <h2>You Might Be Interested In</h2>
        </div>

        <div class="row">
            <?php if ($interestedCategoryOne) { ?>
                <div class="col-md-4 col-sm-12">
                    <div class="feature-box style-2 wow fadeInUp">
                        <div class="feature-title">
                            <h4><?php echo $interestedCategoryOne->categoryName; ?></h4>
                        </div>

                        <div class="feature-content">
                            <p><?php echo $interestedCategoryOne->categoryDescription; ?></p>
                        </div>

                        <div class="read-more"><a href="<?php echo $interestedCategoryOne->categorySlug; ?>">Read
                                More</a></div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($interestedCategoryTwo) { ?>
                <div class="col-md-4 col-sm-12">
                    <div class="feature-box style-2 wow fadeInUp">
                        <div class="feature-title">
                            <h4><?php echo $interestedCategoryTwo->categoryName; ?></h4>
                        </div>

                        <div class="feature-content">
                            <p><?php echo $interestedCategoryTwo->categoryDescription; ?></p>
                        </div>

                        <div class="read-more"><a href="<?php echo $interestedCategoryTwo->categorySlug; ?>">Read
                                More</a></div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($interestedCategoryThree) { ?>
                <div class="col-md-4 col-sm-12">
                    <div class="feature-box style-2 wow fadeInUp">
                        <div class="feature-title">
                            <h4><?php echo $interestedCategoryThree->categoryName; ?></h4>
                        </div>

                        <div class="feature-content">
                            <p><?php echo $interestedCategoryThree->categoryDescription; ?></p>
                        </div>

                        <div class="read-more"><a href="<?php echo $interestedCategoryThree->categorySlug; ?>">Read
                                More</a></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>