<div class="site-wrapper-reveal no-overflow">
    <!-- ======== Hero Area Start ========== -->
    <div class="hero-area hero-style-02 christian-hero-bg-two bg-overlay-black">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hero-content text-center">
                        <h1 class="text-white">
                            <?php echo $homeData->firstSectionText; ?>
                        </h1>

                        <div class="ht-btn-area section-space--mt_60">
                            <a href="<?php echo $homeData->firstButtonLink; ?>" class="hero-btn"><?php echo $homeData->firstButtonText; ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ======== Hero Area End ========== -->

    <!-- ======== Feature Area Start ========== -->
    <div class="feature-area section-space--pt_90">
        <div class="container">
            <div class="row slide-class-4">
                <?php foreach ($services as $service) { ?>
                    <div class="col-lg-3 col-md-6">
                        <div class="single-feature-two" style="margin-bottom: 30px">
                            <div class="feature-icon ">
                                <img src="<?php echo MEDIA_PATH . $service->homeServiceImage; ?>" alt="" />
                            </div>
                            <div class="feature-content">
                                <h5 class="feature-title"><?php echo $service->homeServiceTitle; ?></h5>
                                <p><?php echo $service->homeServiceText; ?></p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- ======== Feature Area End ========== -->

    <!-- ======== Church About Area Start ========== -->
    <div class="church-about-area section-space--pt_120">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="about-tai-image  small-mb__30 tablet-mb__30">
                        <img src="<?php echo MEDIA_PATH . $homeData->home_fourth_section_image_main; ?>" class="img-fluid" alt="church Images" />
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="about-tai-content">
                        <img src="<?php echo MEDIA_PATH . $homeData->home_fourth_section_image; ?>" alt="" />
                        <div class="section-title-wrap mt-20">
                            <h3 class="section-title-normal mb-30">
                                <?php echo $homeData->home_fourth_section_title; ?>
                            </h3>
                        </div>
                        <p>
                            <?php echo $homeData->home_fourth_section_text; ?>
                        </p>
                        <div class="about-us-button mt-40">
                            <a class="about-us-btn" href="<?php echo $homeData->home_fourth_section_href; ?>"><?php echo $homeData->home_fourth_section_btnText; ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ======== Church About Area End ========== -->

    <!-- ======== Service Area Start ========== -->
    <div class="service-area section-space--ptb_120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrap text-center">
                        <h3 class="section-title center-style"><?php echo $homeData->whatWeDO; ?></h3>
                    </div>
                </div>
            </div>
            <div class="row slide-class-3">
                <?php foreach ($features as $feature) { ?>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-service-wrap" style="text-align: center; margin-bottom: 40px">
                            <!-- <div class="service-image" style="display: inline-block;">
                                <img src="<?php echo MEDIA_PATH . $feature->featureImage; ?>" class="img-fluid" alt="Service image" />
                            </div> -->
                            <div class="service-content">
                                <h4 class="service-title"><?php echo $feature->featureTitle; ?></h4>
                                <p><?php echo $feature->featureText; ?></p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- ======== Service Area End ========== -->

    <!-- ======== Hindu Video Area Start ========== -->
    <div class="hindu-video-area">
        <div class="container-fluid container-fluid--cp-100">
            <div class="church-testmonial-bg section-space--ptb_90 bg-overlay-black">
                <div class="row align-items-center">
                    <div class="col-lg-7 ml-auto mr-auto">
                        <div class="video-content-wrap text-center">
                            <div class="content">
                                <h3 class="text-white mb-10">
                                    <?php echo $homeData->home_contact_main_text; ?>
                                </h3>
                                <p class="text-white"><?php echo $homeData->home_contact_small_text; ?></p>
                                <div class="contact-us-btn section-space--mt_60">
                                    <a class="contact-us-btn-white" href="<?php echo $homeData->home_contact_btnLink; ?>"><?php echo $homeData->home_contact_btnText; ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ========  Hindu Video Area End ========== -->

    <!-- ======== Newsletter Area Start ========== -->
    <div class="newsletter-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="newsletter-box-area newsletter-bg">
                        <div class="newsletter-title">
                            <h4>
                                <?php echo $homeData->home_contact_subscribeNewsletterText; ?>
                            </h4>
                        </div>
                        <form id="newsletter_form-1" class="newsletter-input-box" action="<?php echo base_url('api/add_newsletter'); ?>" method="post" onsubmit="return false;">
                            <div class="form-group">
                                <input type="text" name="subscriberEmail" placeholder="<?php echo $homeData->home_contact_emailPlaceholder; ?>" style="width: 100%" />
                                <button type="submit" class="btn subscribe-btn"><?php echo $homeData->home_contact_btnSubcribeText; ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ======== Newsletter  Area End ========== -->
    <!-- ======== Hindu Video Area Start ========== -->
    <div class="hindu-video-area">
        <div class="container-fluid container-fluid--cp-100">
            <div class="church-video-bg hindu-video-section-pb bg-overlay-black border-radius-5">
                <div class="row">
                    <div class="col-lg-6 ml-auto mr-auto">
                        <div class="video-content-wrap text-center">
                            <div class="icon">
                                <a href="<?php echo $homeData->home_video_link; ?>" class="video-link popup-youtube"><img src="<?php echo MEDIA_PATH . $homeData->home_video_play_icon; ?>" alt="Video Icon" /></a>
                            </div>
                            <div class="content section-space--mt_80">
                                <h3 class="text-white mb-10">
                                    <?php echo $homeData->home_video_title; ?>
                                </h3>
                                <p class="text-white">
                                    <?php echo $homeData->home_video_text; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ========  Hindu Video Area End ========== -->
</div>
<script src="<?php echo ASSETS_PATH; ?>js/custom/public/home.js"></script>