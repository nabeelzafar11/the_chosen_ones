

<section class="main-section" data-background="<?php echo MEDIA_PATH . $currentCategory->categoryImage; ?>">
    <div class="overlay" data-overlay="#6a419a"></div>
    <div class="container">
        <div class="section-content">
            <div class="title light mb-0 wow fadeInUp">
                <h2><?php echo $currentCategory->categoryName; ?></h2>
            </div>
        </div>
    </div>
</section>

<section id="section-1" class="page-section pb-0" data-bg-color="#f7f7f7">
    <div class="container">

        <div class="section-title">
            <h2><?php echo $currentCategory->categoryDescription; ?></h2>
        </div>

        <div class="row align-items-stretch">
            <?php foreach ($categories as $category){ ?>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <a href="<?php echo $category->categorySlug; ?>" class="feature-box style-3 wow fadeIn">
                    <div class="feature-icon">
                        <img src="<?php echo MEDIA_PATH . $category->categoryIcon; ?>"/>
                    </div>

                    <div class="feature-title">
                        <h4><?php echo $category->categoryName; ?></h4>
                    </div>

                    <div class="feature-content">
                        <p><?php echo $category->categoryDescription; ?></p>
                    </div>
                </a>
            </div>
            <?php } ?>
        </div>

    </div>
</section>

<section class="page-section">
    <div class="container">
        <div class="section-title">
            <h2>You Might Be Interested In</h2>
        </div>

        <div class="row">
            <?php if ($interestedCategoryOne) { ?>
                <div class="col-md-4 col-sm-12">
                    <div class="feature-box style-2 wow fadeInUp">
                        <div class="feature-title">
                            <h4><?php echo $interestedCategoryOne->categoryName; ?></h4>
                        </div>

                        <div class="feature-content">
                            <p><?php echo $interestedCategoryOne->categoryDescription; ?></p>
                        </div>

                        <div class="read-more"><a href="<?php echo $interestedCategoryOne->categorySlug; ?>">Read
                                More</a></div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($interestedCategoryTwo) { ?>
                <div class="col-md-4 col-sm-12">
                    <div class="feature-box style-2 wow fadeInUp">
                        <div class="feature-title">
                            <h4><?php echo $interestedCategoryTwo->categoryName; ?></h4>
                        </div>

                        <div class="feature-content">
                            <p><?php echo $interestedCategoryTwo->categoryDescription; ?></p>
                        </div>

                        <div class="read-more"><a href="<?php echo $interestedCategoryTwo->categorySlug; ?>">Read
                                More</a></div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($interestedCategoryThree) { ?>
                <div class="col-md-4 col-sm-12">
                    <div class="feature-box style-2 wow fadeInUp">
                        <div class="feature-title">
                            <h4><?php echo $interestedCategoryThree->categoryName; ?></h4>
                        </div>

                        <div class="feature-content">
                            <p><?php echo $interestedCategoryThree->categoryDescription; ?></p>
                        </div>

                        <div class="read-more"><a href="<?php echo $interestedCategoryThree->categorySlug; ?>">Read
                                More</a></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>