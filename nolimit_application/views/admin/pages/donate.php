<div class="dashboard-content">
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Donate</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2" role="alert"><?php echo $notificationData['message']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="donate_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/update_donate_content'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Donate Content</h4>
                    <div class="dashboard-list-box-static">
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="donateHeader">Page Title</label>
                                        <input placeholder="Header" class="form-control" type="text" id="donateHeader" value="<?php echo $homeData->donate_header; ?>" name="donateHeader" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeText">Home Text</label>
                                        <input placeholder="Home" class="form-control" type="text" id="homeText" value="<?php echo $homeData->donate_homeText; ?>" name="homeText" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="donateTitle">Title</label>
                                        <input placeholder="Title" class="form-control" type="text" id="donateTitle" value="<?php echo $homeData->donate_title; ?>" name="donateTitle" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="donateDescription">Description</label>
                                        <textarea placeholder="Description" class="form-control" type="text" id="donateDescription" name="donateDescription"><?php echo $homeData->donate_description; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="donateNowBtn">Donate Now Button</label>
                                        <input placeholder="Donate Now" class="form-control" type="text" id="donateNowBtn" value="<?php echo $homeData->donate_donateNow; ?>" name="donateNowBtn" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/donate.js"></script>