<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>About</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="about_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/update_about_content'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">About Content</h4>
                    <div class="dashboard-list-box-static">
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="pageTitle">Page Title</label>
                                        <input placeholder="About" class="form-control" type="text" id="pageTitle" value="<?php echo $homeData->aboutPageTitle; ?>" name="pageTitle" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeText">Home Text</label>
                                        <input placeholder="Home" class="form-control" type="text" id="homeText" value="<?php echo $homeData->aboutHomeText; ?>" name="homeText" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input placeholder="Title" class="form-control" type="text" id="title" value="<?php echo $homeData->aboutTitle; ?>" name="title" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="form-control" style="border-radius: 5px;" rows="4" id="description" name="description" cols="50"><?php echo $homeData->aboutDescription; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="detailofText">Detail of Text</label>
                                        <input placeholder="Detail of" class="form-control" type="text" id="detailofText" value="<?php echo $homeData->aboutDetailofText; ?>" name="detailofText" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="videoLink">Video Link </label>
                                        <input placeholder="Video Link" class="form-control" type="text" id="videoLink" value="<?php echo $homeData->aboutVideoLink; ?>" name="videoLink" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>
        </div>
        </form>
    </div>
</div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/about_content.js"></script>