<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>About</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2" role="alert"><?php echo $notificationData['message']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="small-list-wrapper">
            <!-- small list wrapper -->
            <ul id="sortable">
                <?php foreach ($counters as $counter) { ?>
                    <li data-id="<?php echo $counter->counterId; ?>">
                        <div class="small-listing-box white-bg">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <img src="<?php echo MEDIA_PATH . $counter->counterImage; ?>" class="img-responsive" alt="">
                                <h4><?php echo $counter->counterTitle; ?></h4>
                            </div>
                            <div class="small-list-action">
                                <label class="switch">
                                    <input type="checkbox" <?php if ($counter->counterIsActive) { ?> checked="checked" <?php } ?> onchange="trigger_is_active(this, <?php echo $counter->counterId; ?>, '<?php echo base_url('api/update_counter_status'); ?>')">
                                    <span class="slider round"></span>
                                </label>
                                <a href="<?php echo base_url('admin/edit_counter_item/' . $counter->counterId); ?>" class="light-gray-btn btn-square" data-placement="top" data-toggle="tooltip" title="Edit Item"><i class="ti-pencil"></i></a>
                                <a href="javascript:void(0);" onclick="delete_item(this, <?php echo $counter->counterId; ?>, '<?php echo base_url('api/delete_counter'); ?>');" class="theme-btn btn-square" data-toggle="tooltip" title="Delete Item"><i class="ti-trash"></i></a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div> <!-- ending of small list wrapper -->
        <div class="col-md-12">
            <?php if (count($counters) == 0) { ?>
                <h4>No record found.</h4>
            <?php } ?>
        </div>
    </div>
</div>

</div>
<!-- Content / End -->
<script>
    var sortUrl = '<?php echo base_url('api/sort_counters'); ?>';
</script>