<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Contact</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="home_fourth_section_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/update_contact_content'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Contact Content</h4>
                    <div class="dashboard-list-box-static">
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="pageTitle">Page Title</label>
                                        <input placeholder="Contact" class="form-control" type="text" id="pageTitle" value="<?php echo $homeData->contact_pageTitle; ?>" name="pageTitle" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeText">Home Text</label>
                                        <input placeholder="Home" class="form-control" type="text" id="homeText" value="<?php echo $homeData->contact_homeText; ?>" name="homeText" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <input placeholder="Address" class="form-control" type="text" id="address" value="<?php echo $homeData->contact_address; ?>" name="address" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="phone1">Phone No 1</label>
                                        <input placeholder="Phone" class="form-control" type="text" id="phone1" value="<?php echo $homeData->contact_phone1; ?>" name="phone1" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="phone2">Phone NO 2</label>
                                        <input placeholder="Phone" class="form-control" type="text" id="phone2" value="<?php echo $homeData->contact_phone2; ?>" name="phone2" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="mail1">Mail 1</label>
                                        <input placeholder="E-mail" class="form-control" type="text" id="mail1" value="<?php echo $homeData->contact_mail1; ?>" name="mail1" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="mail2">Mail 2</label>
                                        <input placeholder="E-mail" class="form-control" type="text" id="mail2" value="<?php echo $homeData->contact_mail2; ?>" name="mail2" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="mapLoc">Map Location (iframe src)</label>
                                        <input placeholder="iframe src" class="form-control" type="text" id="mapLoc" value="<?php echo $homeData->contact_mapLoc; ?>" name="mapLoc" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>
        </div>
        </form>
    </div>
</div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/home_fourth_section.js"></script>