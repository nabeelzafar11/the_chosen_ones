<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Contact</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="home_fourth_section_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/update_contact_form'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Contact Form</h4>
                    <div class="dashboard-list-box-static">
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="title">Form Title</label>
                                        <input placeholder="Title" class="form-control" type="text" id="title" value="<?php echo $homeData->contact_formTitle; ?>" name="title" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name_l">Name Label</label>
                                        <input placeholder="Name" class="form-control" type="text" id="name_l" value="<?php echo $homeData->contact_name_l; ?>" name="name_l" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name_p">Name Field Placeholder</label>
                                        <input placeholder="Enter you name" class="form-control" type="text" id="name_p" value="<?php echo $homeData->contact_name_p; ?>" name="name_p" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phone_l">Phone Label</label>
                                        <input placeholder="Phone" class="form-control" type="text" id="phone_l" value="<?php echo $homeData->contact_phone_l; ?>" name="phone_l" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phone_p">Phone Field Placeholder</label>
                                        <input placeholder="Enter you phone" class="form-control" type="text" id="phone_p" value="<?php echo $homeData->contact_phone_p; ?>" name="phone_p" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email_l">Email Label</label>
                                        <input placeholder="Email" class="form-control" type="text" id="email_l" value="<?php echo $homeData->contact_email_l; ?>" name="email_l" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email_p">Email Field Placeholder</label>
                                        <input placeholder="Enter you email" class="form-control" type="text" id="email_p" value="<?php echo $homeData->contact_email_p; ?>" name="email_p" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="message_l">Message Label</label>
                                        <input placeholder="Message" class="form-control" type="text" id="message_l" value="<?php echo $homeData->contact_message_l; ?>" name="message_l" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="message_p">Message Field Placeholder</label>
                                        <input placeholder="Enter you message" class="form-control" type="text" id="message_p" value="<?php echo $homeData->contact_message_p; ?>" name="message_p" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="submit_btn">Submit Button Text</label>
                                        <input placeholder="Submit" class="form-control" type="text" id="submit_btn" value="<?php echo $homeData->contact_submitBtn; ?>" name="submit_btn" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>
        </div>
        </form>
    </div>
</div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/home_fourth_section.js"></script>