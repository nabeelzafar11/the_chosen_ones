<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home Third Section / Add New Third Section Item</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="testimonial_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/add_new_thirdsection'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Add New Testimonial</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo ASSETS_PATH; ?>listeo_updated/images/placeholder.png"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i>
                                                                                                Upload Image
                                            </span>
                                            </div>
                                        </div>
                                        <input type="file" name="testimonialImage" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="testimonialName">Name</label>
                                        <input placeholder="Name" class="form-control" type="text"
                                               id="testimonialName"
                                               name="testimonialName"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="testimonialOccupation">Href</label>
                                        <input placeholder="Occupation" class="form-control" type="text"
                                               id="testimonialOccupation"
                                               name="testimonialOccupation"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="testimonialComment">
                                            Comment
                                        </label>
                                        <textarea name="testimonialComment" id="testimonialComment" rows="5"
                                                  placeholder="Comment"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="button margin-top-15">Add</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/add_new_testimonial.js"></script>




