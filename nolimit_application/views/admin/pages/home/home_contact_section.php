<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home Contact Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="home_fourth_section_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/update_home_contact_section'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Home Contact Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_contact_main_text">Main Text</label>
                                        <input placeholder="Title" class="form-control" type="text" id="home_contact_main_text" value="<?php echo $homeData->home_contact_main_text; ?>" name="home_contact_main_text" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_contact_small_text">Small Text</label>
                                        <input placeholder="Text" class="form-control" type="text" id="home_contact_small_text" value="<?php echo $homeData->home_contact_small_text; ?>" name="home_contact_small_text" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_contact_btnText">Contact Button Text</label>
                                        <input placeholder="Contact" class="form-control" type="text" id="home_contact_btnText" value="<?php echo $homeData->home_contact_btnText; ?>" name="home_contact_btnText" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_contact_btnLink">Contact Button Href</label>
                                        <select data-placeholder="Contact Button Href" id="home_contact_btnLink" name="home_contact_btnLink" class="form-control chosen-select" tabindex="2">
                                            <option value="">Button Href</option>
                                            <?php foreach ($categories as $category) { ?>
                                                <option <?php if ($homeData->home_contact_btnLink == $category->categorySlug) { ?> selected="selected" <?php } ?> value="<?php echo $category->categorySlug; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_contact_subscribeNewsletterText">Subscribe Newsletter Text</label>
                                        <input placeholder="Subscribe Newsletter Text" class="form-control" type="text" id="home_contact_subscribeNewsletterText" value="<?php echo $homeData->home_contact_subscribeNewsletterText; ?>" name="home_contact_subscribeNewsletterText" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_contact_emailPlaceholder">Email</label>
                                        <input placeholder="Email" class="form-control" type="text" id="home_contact_emailPlaceholder" value="<?php echo $homeData->home_contact_emailPlaceholder; ?>" name="home_contact_emailPlaceholder" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_contact_btnSubcribeText">Subcribe Button Text</label>
                                        <input placeholder="Subcribe" class="form-control" type="text" id="home_contact_btnSubcribeText" value="<?php echo $homeData->home_contact_btnSubcribeText; ?>" name="home_contact_btnSubcribeText" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/home_fourth_section.js"></script>