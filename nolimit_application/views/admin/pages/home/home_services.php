<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home Services Listings </h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2" role="alert"><?php echo $notificationData['message']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>


    </div>

    <!-- content -->
    <div class="row">
        <div class="small-list-wrapper">
            <!-- small list wrapper -->
            <ul id="sortable">
                <?php foreach ($services as $service) { ?>
                    <li data-id="<?php echo $service->homeServiceId; ?>">
                        <div class="small-listing-box white-bg">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <img src="<?php echo MEDIA_PATH . $service->homeServiceImage; ?>" class="img-responsive" alt="">
                                <h4><?php echo $service->homeServiceTitle; ?></h4>
                            </div>
                            <div class="small-list-action">
                                <label class="switch">
                                    <input type="checkbox" <?php if ($service->homeServiceIsActive) { ?> checked="checked" <?php } ?> onchange="trigger_is_active(this, <?php echo $service->homeServiceId; ?>, '<?php echo base_url('api/update_service_status'); ?>')">
                                    <span class="slider round"></span>
                                </label>
                                <a href="<?php echo base_url('admin/home_servies_listings/edit_home_service/' . $service->homeServiceId); ?>" class="light-gray-btn btn-square" data-placement="top" data-toggle="tooltip" title="Edit Item"><i class="ti-pencil"></i></a>
                                <a href="javascript:void(0);" onclick="delete_item(this, <?php echo $service->homeServiceId; ?>, '<?php echo base_url('api/delete_service'); ?>');" class="theme-btn btn-square" data-toggle="tooltip" title="Delete Item"><i class="ti-trash"></i></a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div> <!-- ending of small list wrapper -->
        <div class="col-md-12">
            <?php if (count($services) == 0) { ?>
                <h4>No record found.</h4>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    var sortUrl = '<?php echo base_url('api/sort_service'); ?>';
</script>