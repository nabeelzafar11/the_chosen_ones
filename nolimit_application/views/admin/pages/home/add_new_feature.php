<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>What We Do / Add New Service</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="testimonial_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/add_new_feature_service'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Add New Service</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <!-- <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo ASSETS_PATH; ?>listeo_updated/images/placeholder.png" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload Image
                                                </span>
                                            </div>
                                        </div>
                                        <input type="file" name="featureImage" value="" onchange="readURL(this);" style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;" />
                                    </div>
                                </div> -->
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="featureTitle">Service Title</label>
                                        <input placeholder="Service Title" class="form-control" type="text" id="featureTitle" name="featureTitle" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="featureDescription">Service Description</label>
                                        <input placeholder="Service Description" class="form-control" type="text" id="featureDescription" name="featureDescription" />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="button margin-top-15">Add</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/add_new_testimonial.js"></script>