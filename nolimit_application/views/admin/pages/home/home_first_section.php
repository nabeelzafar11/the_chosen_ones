<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>First Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="footer_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/update_first_section'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">First Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <!-- <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $homeData->firstSectionImage; ?>" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload Section Image
                                                </span>
                                            </div>
                                        </div>
                                        <input type="file" name="sectionImage" value="" onchange="readURL(this);" style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;" />
                                        <input type="hidden" name="sectionImage" value="<?php echo $homeData->firstSectionImage; ?>" />
                                    </div>
                                </div> -->
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="phoneText">Section Text</label>
                                        <input placeholder="Section Text" class="form-control" type="text" id="sectionText" value="<?php echo $homeData->firstSectionText; ?>" name="sectionText" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="phoneText">Button Text</label>
                                        <input placeholder="Button Text" class="form-control" type="text" id="buttonText" value="<?php echo $homeData->firstButtonText; ?>" name="buttonText" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="phoneText">Button Href </label>
                                        <select data-placeholder="Button Href" id="buttonLink" name="buttonLink" class="form-control chosen-select" tabindex="2">
                                            <option value="">Button Href</option>
                                            <?php foreach ($categories as $category) { ?>
                                                <option <?php if ($homeData->firstButtonLink == $category->categorySlug) { ?> selected="selected" <?php } ?> value="<?php echo $category->categorySlug; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/footer.js"></script>