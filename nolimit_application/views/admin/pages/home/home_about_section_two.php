<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home About Section Two</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="home_about_section_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_home_about_section_two'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Home About Section Two</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo banner-img gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $homeData->homeDataAboutTwoImage; ?>"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i>
                                                                                                Upload Icon
                                            </span>
                                            </div>
                                        </div>
                                        <input type="file" name="homeDataAboutTwoImage" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                        <input type="hidden" name="homeDataAboutTwoImage"
                                               value="<?php echo $homeData->homeDataAboutTwoImage; ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeDataAboutTwoTitleOne">
                                            Title One
                                        </label>
                                        <textarea name="homeDataAboutTwoTitleOne" id="homeDataAboutTwoTitleOne" rows="5"
                                                  placeholder="Title One"><?php echo $homeData->homeDataAboutTwoTitleOne; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeDataAboutTwoTitleTwo">
                                            Title Two
                                        </label>
                                        <textarea name="homeDataAboutTwoTitleTwo" id="homeDataAboutTwoTitleTwo" rows="5"
                                                  placeholder="Title Two"><?php echo $homeData->homeDataAboutTwoTitleTwo; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="homeDataAboutTwoButtonText">Button Text</label>
                                        <input placeholder="Button Text" class="form-control" type="text"
                                               id="homeDataAboutTwoButtonText"
                                               value="<?php echo $homeData->homeDataAboutTwoButtonText; ?>"
                                               name="homeDataAboutTwoButtonText"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="homeDataAboutTwoButtonCategoryId">Button Href</label>
                                        <select data-placeholder="Select Text Position" id="homeDataAboutTwoButtonCategoryId"
                                                name="homeDataAboutTwoButtonCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                            <?php foreach ($categories as $category) { ?>
                                                <option
                                                    <?php if ($homeData->homeDataAboutTwoButtonCategoryId == $category->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/home_about_section.js"></script>




