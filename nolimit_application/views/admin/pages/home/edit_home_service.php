<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home Services Listings / Edit Service</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="testimonial_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/edit_home_service'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Edit Service</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <input type="hidden" name="homeServiceId" value="<?php echo $homeService->homeServiceId; ?>" />
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $homeService->homeServiceImage; ?>" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload Image
                                                </span>
                                            </div>
                                        </div>
                                        <input type="file" name="serviceImage" value="" onchange="readURL(this);" style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;" />
                                        <input type="hidden" name="serviceImage" value="<?php echo $homeService->homeServiceImage; ?>" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="serviceTitle">Service Title</label>
                                        <input placeholder="Service Title" class="form-control" type="text" id="serviceTitle" value="<?php echo $homeService->homeServiceTitle; ?>" name="serviceTitle" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="serviceDescription">Service Description</label>
                                        <input placeholder="Service Description" class="form-control" type="text" id="serviceDescription" value="<?php echo $homeService->homeServiceText; ?>" name="serviceDescription" />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="button margin-top-15">Save</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/add_new_testimonial.js"></script>