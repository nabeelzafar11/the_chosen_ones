<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home Blog Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="home_blog_section_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_home_blog_section'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Home Blog Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="homeDataBlogBlogOne">Blog One</label>
                                        <select data-placeholder="Select Text Position" id="homeDataBlogBlogOne"
                                                name="homeDataBlogBlogOne"
                                                class="form-control chosen-select" tabindex="2">
                                            <?php foreach ($blogs as $blog) { ?>
                                                <option
                                                    <?php if ($homeData->homeDataBlogBlogOne == $blog->blogId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $blog->blogId; ?>"><?php echo $blog->blogTitle; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="homeDataBlogBlogTwo">Blog Two</label>
                                        <select data-placeholder="Select Text Position" id="homeDataBlogBlogTwo"
                                                name="homeDataBlogBlogTwo"
                                                class="form-control chosen-select" tabindex="2">
                                            <?php foreach ($blogs as $blog) { ?>
                                                <option
                                                    <?php if ($homeData->homeDataBlogBlogTwo == $blog->blogId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $blog->blogId; ?>"><?php echo $blog->blogTitle; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="homeDataBlogBlogThree">Blog Three</label>
                                        <select data-placeholder="Select Text Position" id="homeDataBlogBlogThree"
                                                name="homeDataBlogBlogThree"
                                                class="form-control chosen-select" tabindex="2">
                                            <?php foreach ($blogs as $blog) { ?>
                                                <option
                                                    <?php if ($homeData->homeDataBlogBlogThree == $blog->blogId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $blog->blogId; ?>"><?php echo $blog->blogTitle; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/home_blog_section.js"></script>




