<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home Video & Facebook Feed Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="home_facebook_section_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_home_facebook_section'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Home Video & Facebook Feed Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo banner-img gray-bg imageDiv">
                                        <video width="400" controls>
                                            <source id="video" src="<?php echo MEDIA_PATH . $homeData->homepage_video; ?>" type="video/mp4">
                                        </video>
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload Video
                                                </span>
                                            </div>
                                        </div>
                                        <input type="file" name="homepage_video" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                        <input type="hidden" name="homepage_video"
                                               value="<?php echo $homeData->homepage_video; ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="facebook_script">
                                            Facebook Feed Script
                                        </label>
                                        <textarea name="facebook_script" id="homeDataTestimonialSubTitle" rows="5"
                                                  placeholder="Please add your facebook script code here"><?php echo $homeData->facebook_script; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/home_facebook_section.js"></script>




