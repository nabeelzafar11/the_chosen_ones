<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home Testmonial Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="home_testimonial_section_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_home_testimonial_section'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Home Testmonial Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12 text-center" style='display:none;'>
                                    <div class="edit-profile-photo banner-img gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $homeData->homeDataTestimonialImage; ?>"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i>
                                            Upload Icon
                                            </span>
                                            </div>
                                        </div>
                                        <input type="file" name="homeDataTestimonialImage" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                        <input type="hidden" name="homeDataTestimonialImage"
                                               value="<?php echo $homeData->homeDataTestimonialImage; ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeDataTestimonialTitle">Title</label>
                                        <input placeholder="Title" class="form-control" type="text"
                                               id="homeDataTestimonialTitle"
                                               value="<?php echo $homeData->homeDataTestimonialTitle; ?>"
                                               name="homeDataTestimonialTitle"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeDataTestimonialSubTitle">
                                            Sub Title
                                        </label>
                                        <textarea name="homeDataTestimonialSubTitle" id="homeDataTestimonialSubTitle" rows="5"
                                                  placeholder="Sub Title"><?php echo $homeData->homeDataTestimonialSubTitle; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/home_testimonial_section.js"></script>




