<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home Interested Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="home_interested_section_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/update_home_interested_section'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Home Interested Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeDataInterestedTitle">Title</label>
                                        <input placeholder="Title" class="form-control" type="text"
                                               id="homeDataInterestedTitle"
                                               value="<?php echo $homeData->homeDataInterestedTitle; ?>"
                                               name="homeDataInterestedTitle"/>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="homeDataInterestedCategoryOne">Interested One Category</label>
                                        <select data-placeholder="Select Text Position" id="homeDataInterestedCategoryOne"
                                                name="homeDataInterestedCategoryOne"
                                                class="form-control chosen-select" tabindex="2">
                                            <?php foreach ($categories as $category) { ?>
                                                <option
                                                    <?php if ($homeData->homeDataInterestedCategoryOne == $category->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="homeDataInterestedCategoryTwo">Interested Two Category</label>
                                        <select data-placeholder="Select Text Position" id="homeDataInterestedCategoryTwo"
                                                name="homeDataInterestedCategoryTwo"
                                                class="form-control chosen-select" tabindex="2">
                                            <?php foreach ($categories as $category) { ?>
                                                <option
                                                    <?php if ($homeData->homeDataInterestedCategoryTwo == $category->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="homeDataInterestedCategoryThree">Interested Three Category</label>
                                        <select data-placeholder="Select Text Position" id="homeDataInterestedCategoryThree"
                                                name="homeDataInterestedCategoryThree"
                                                class="form-control chosen-select" tabindex="2">
                                            <?php foreach ($categories as $category) { ?>
                                                <option
                                                    <?php if ($homeData->homeDataInterestedCategoryThree == $category->categoryId) { ?>
                                                        selected="selected"
                                                    <?php } ?>
                                                        value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/home_interested_section.js"></script>




