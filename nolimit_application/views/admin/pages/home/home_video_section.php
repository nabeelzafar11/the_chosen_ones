<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Video Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="footer_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/update_video_section'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Video Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $homeData->home_video_play_icon; ?>" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload Play Icon
                                                </span>
                                            </div>
                                        </div>
                                        <input type="file" name="home_video_play_icon" value="" onchange="readURL(this);" style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;" />
                                        <input type="hidden" name="home_video_play_icon" value="<?php echo $homeData->home_video_play_icon; ?>" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_video_link">Video Link </label>
                                        <input placeholder="Video Link" class="form-control" type="text" id="home_video_link" value="<?php echo $homeData->home_video_link; ?>" name="home_video_link" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_video_title">Section Title</label>
                                        <input placeholder="Section Text" class="form-control" type="text" id="home_video_title" value="<?php echo $homeData->home_video_title; ?>" name="home_video_title" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_video_text">Section Text</label>
                                        <input placeholder="Section Text" class="form-control" type="text" id="home_video_text" value="<?php echo $homeData->home_video_text; ?>" name="home_video_text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/footer.js"></script>