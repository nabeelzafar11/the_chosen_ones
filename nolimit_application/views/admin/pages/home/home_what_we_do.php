<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>What We Do</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2" role="alert"><?php echo $notificationData['message']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="testimonial_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/update_what_we_do'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="whatWeDO">What We DO Text</label>
                                        <input placeholder="What We DO" class="form-control" type="text" id="whatWeDO" value="<?php echo $homeData->whatWeDO; ?>" name="whatWeDO" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="button margin-top-15">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="titlebar" class="margin-top-30">
        <div class="row">
            <div class="col-md-12">
                <h2>Services</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="small-list-wrapper col-md-12">
            <!-- small list wrapper -->
            <ul id="sortable">
                <?php foreach ($features as $feature) { ?>
                    <li data-id="<?php echo $feature->featureId; ?>">
                        <div class="small-listing-box white-bg">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail">
                                <!-- <img src="<?php echo MEDIA_PATH . $feature->featureImage; ?>" class="img-responsive" alt=""> -->
                                <h4><?php echo $feature->featureTitle; ?></h4>
                            </div>
                            <div class="small-list-action">
                                <label class="switch">
                                    <input type="checkbox" <?php if ($feature->featureIsActive) { ?> checked="checked" <?php } ?> onchange="trigger_is_active(this, <?php echo $feature->featureId; ?>, '<?php echo base_url('api/update_feature_status'); ?>')">
                                    <span class="slider round"></span>
                                </label>
                                <a href="<?php echo base_url('admin/home_what_we_do/edit_feature/' . $feature->featureId); ?>" class="light-gray-btn btn-square" data-placement="top" data-toggle="tooltip" title="Edit Item"><i class="ti-pencil"></i></a>
                                <a href="javascript:void(0);" onclick="delete_item(this, <?php echo $feature->featureId; ?>, '<?php echo base_url('api/delete_feature'); ?>');" class="theme-btn btn-square" data-toggle="tooltip" title="Delete Item"><i class="ti-trash"></i></a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div> <!-- ending of small list wrapper -->
        <div class="col-md-12">
            <?php if (count($features) == 0) { ?>
                <h4>No record found.</h4>
            <?php } ?>
        </div>
    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/add_new_testimonial.js"></script>