<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Home Third Section</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="home_fourth_section_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/update_home_third_section'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Home Third Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-4 text-center">
                                    <div class="edit-profile-photo banner-img gray-bg imageDiv">
                                        <img id="image1" src="<?php echo MEDIA_PATH . $homeData->home_fourth_section_image_main; ?>" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload Main Image
                                                </span>
                                            </div>
                                        </div>
                                        <input class="image1" type="file" name="home_fourth_section_image_main" value="" onchange="readURL(this);" style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;" />
                                        <input class="image1" type="hidden" name="home_fourth_section_image_main" value="<?php echo $homeData->home_fourth_section_image_main; ?>" />
                                    </div>
                                </div>
                                <div class="col-sm-4 text-center">
                                    <div class="edit-profile-photo banner-img gray-bg imageDiv">
                                        <img id="image2" src="<?php echo MEDIA_PATH . $homeData->home_fourth_section_image; ?>" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload Icon
                                                </span>
                                            </div>
                                        </div>
                                        <input class="image2" type="file" name="home_fourth_section_image" value="" onchange="readURL(this);" style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;" />
                                        <input class="image2" type="hidden" name="home_fourth_section_image" value="<?php echo $homeData->home_fourth_section_image; ?>" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_fourth_section_title">Title</label>
                                        <input placeholder="Title" class="form-control" type="text" id="home_fourth_section_title" value="<?php echo $homeData->home_fourth_section_title; ?>" name="home_fourth_section_title" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_fourth_section_text">Description</label>
                                        <input placeholder="Description" class="form-control" type="text" id="home_fourth_section_text" value="<?php echo $homeData->home_fourth_section_text; ?>" name="home_fourth_section_text" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_fourth_section_btnText">Button Text</label>
                                        <input placeholder="Button Text" class="form-control" type="text" id="home_fourth_section_btnText" value="<?php echo $homeData->home_fourth_section_btnText; ?>" name="home_fourth_section_btnText" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="home_fourth_section_href">Button Link</label>
                                        <select data-placeholder="Button Href" id="home_fourth_section_href" name="home_fourth_section_href" class="form-control chosen-select" tabindex="2">
                                            <option value="">Button Href</option>
                                            <?php foreach ($categories as $category) { ?>
                                                <option <?php if ($homeData->home_fourth_section_href == $category->categorySlug) { ?> selected="selected" <?php } ?> value="<?php echo $category->categorySlug; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/home_fourth_section.js"></script>