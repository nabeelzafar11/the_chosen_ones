<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Footer Menu Listings
                </h2>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2"
                         role="alert"><?php echo $notificationData['message']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>


    </div>
    <div class="row">
        <div class="col-sm-12">
            <form id="footer_menu" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/add_footer_menu'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Footer Menu</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="menuButtonText">Menu Text</label>
                                        <input placeholder="Menu Text" class="form-control" type="text"
                                               id="menuButtonText" value=""
                                               name="menuName"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="menuButtonText">Menu Href Link (Linking to some-other application)</label>
                                        <input placeholder="Leave empty if want to link to internal site" class="form-control" type="text"
                                               id="footerMenuHref" value=""
                                               name="footerMenuHref"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="menuButtonCategoryId">Menu Href (Internal Linking)</label>
                                        <select data-placeholder="Menu Href" id="menuButtonCategoryId"
                                                name="menuCategoryId"
                                                class="form-control chosen-select" tabindex="2">
                                                <option></option>
                                            <?php foreach ($categories as $category1) { ?>
                                                <option
                                                        value="<?php echo $category1->categoryId; ?>"><?php echo $category1->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="button margin-top-15">Save Changes</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>

    </div>

    <!-- Titlebar -->
   
    
</div>
<script>
document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $("#footer_menu").validate({
            rules: {
                'menuButtonText': {
                    required: true
                },
                'menuButtonCategoryId': {
                    required: true
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (response) {
                        ajax_success_function(form, response);
                    },
                    beforeSubmit: function () {
                        ajax_start_function(form);
                    },
                    error: function (response) {
                        show_request_failed_alert(form);
                        ajax_end_function();
                    }
                });
            }
        });
    }
}
</script>


