<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Category Listings / Add New Category Page Section</h2>
                <b><?php echo $category->categoryName; ?></b>
                <a href="<?php echo base_url('admin/category_listings/category_page/'.$category->categoryId); ?>"
                   class="button border with-icon" style="float: right;">
                    Back
                    <i class="sl sl-icon-arrow-left"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="category_page_section_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/add_new_category_page_section'); ?>"
                  onsubmit="return false;">
                <input type="hidden" name="pageSectionCategoryId" value="<?php echo $category->categoryId; ?>"/>
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Add New Category Page Section</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="pageSectionTitle">Page Section Title</label>
                                        <input placeholder="Page Section Title" class="form-control" type="text"
                                               id="pageSectionTitle"
                                               name="pageSectionTitle"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Add</button>
                </div>


            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/add_new_category_page_section.js"></script>




