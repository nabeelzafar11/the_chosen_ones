<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Cateories Listing / Edit Category Page</h2>
                <b><?php echo $category->categoryName; ?></b>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="category_page_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/edit_category_page_content'); ?>"
                  onsubmit="return false;">
                <input type="hidden" name="categoryId" value="<?php echo $category->categoryId; ?>"/>
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Edit Category Page</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categoryDescription">
                                            Category Page Content
                                        </label>
                                        <textarea name="categoryPageContent" id="categoryPageContent" rows="20"
                                                  placeholder="Category Page Content"><?php echo $category->categoryPageContent; ?>
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/edit_category_page_content.js"></script>




