<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Category Listings / Add New Category</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="category_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/add_new_category'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Add New Category</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <!-- <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo banner-img gray-bg imageDiv">
                                        <img src="<?php echo ASSETS_PATH; ?>listeo_updated/images/placeholder.png"
                                             alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i>
                                                                                                Upload Banner Image
                                            </span>
                                            </div>
                                        </div>
                                        <input type="file" name="categoryImage" value="" onchange="readURL(this);"
                                               style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;"/>
                                    </div>
                                </div> -->
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categoryName">Page Name</label>
                                        <input placeholder="Page Name" class="form-control" type="text" id="categoryName" name="categoryName" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="homeText">Home Text</label>
                                        <input placeholder="Home" class="form-control" type="text" id="homeText" name="homeText" />
                                    </div>
                                </div>
                                <div class="col-sm-6" style="display:none;">
                                    <div class="form-group">
                                        <label for="categoryParentId">Parent Category</label>
                                        <select data-placeholder="Select Text Position" id="categoryParentId" name="categoryParentId" class="form-control chosen-select" tabindex="2">
                                            <option value="0">No Parent Category</option>
                                            <?php foreach ($categories as $category) { ?>
                                                <option value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6" style="display:none;">
                                    <div class="form-group">
                                        <label for="categoryPageType">Category Page Type</label>
                                        <select data-placeholder="Select Text Position" id="categoryPageType" name="categoryPageType" class="form-control chosen-select" tabindex="2">
                                            <option value="0">No Page</option>
                                            <option value="1">Blogs</option>
                                            <!--                                            <option value="2">Board of Directors</option>-->
                                            <option value="9">Contact Us</option>
                                            <option value="3" selected="selected">Content</option>
                                            <!--                                            <option value="4">Media Release</option>-->
                                            <!--                                            <option value="5">News</option>-->
                                            <option value="7">Search</option>
                                            <option value="6">Sections</option>
                                            <option value="8">Site Map</option>
                                            <option value="10">Career</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categoryShortDescription">
                                            Short Description
                                        </label>
                                        <textarea name="categoryShortDescription" id="categoryShortDescription" rows="5" placeholder="Short Description"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categoryDescription">
                                            Description
                                        </label>
                                        <textarea name="categoryDescription" id="categoryDescription" rows="5" placeholder="Description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dashboard-list-box margin-top-0" style="display:none;">
                    <h4 class="gray">Interested In</h4>
                    <div class="dashboard-list-box-static">
                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="categoryInterestedOneCategoryId">Interested One Category</label>
                                        <select data-placeholder="Select Text Position" id="categoryInterestedOneCategoryId" name="categoryInterestedOneCategoryId" class="form-control chosen-select" tabindex="2">
                                            <option value="0">No Interested Category</option>
                                            <?php foreach ($categories as $category) { ?>
                                                <option value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="categoryInterestedTwoCategoryId">Interested Two Category</label>
                                        <select data-placeholder="Select Text Position" id="categoryInterestedTwoCategoryId" name="categoryInterestedTwoCategoryId" class="form-control chosen-select" tabindex="2">
                                            <option value="0">No Interested Category</option>
                                            <?php foreach ($categories as $category) { ?>
                                                <option value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="categoryInterestedThreeCategoryId">Interested Three Category</label>
                                        <select data-placeholder="Select Text Position" id="categoryInterestedThreeCategoryId" name="categoryInterestedThreeCategoryId" class="form-control chosen-select" tabindex="2">
                                            <option value="0">No Interested Category</option>
                                            <?php foreach ($categories as $category) { ?>
                                                <option value="<?php echo $category->categoryId; ?>"><?php echo $category->categoryName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Seo Settings</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categoryPageTitle">Page Title</label>
                                        <input placeholder="Page Title" class="form-control" type="text" id="categoryPageTitle" name="categoryPageTitle" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categorySeoTitle">Seo Title</label>
                                        <input placeholder="Seo Title" class="form-control" type="text" id="categorySeoTitle" name="categorySeoTitle" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categorySeoKeywords">Seo Keywords</label>
                                        <input placeholder="Seo Keywords" class="form-control" type="text" id="categorySeoKeywords" name="categorySeoKeywords" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="categorySeoDescription">
                                            Seo Description
                                        </label>
                                        <textarea name="categorySeoDescription" id="categorySeoDescription" rows="5" placeholder="Seo Description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Add</button>
                </div>


            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/add_new_category.js"></script>