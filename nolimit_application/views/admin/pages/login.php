<!DOCTYPE html>


<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title><?php echo SITE_TITLE; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <style type="text/css">
        /* league gothic */
        /*Regular*/
        @font-face {
            font-family: leaguegothic;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/LeagueGothic-Regular.otf);
        }

        /* Condensed */
        @font-face {
            font-family: leaguegothicbold;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/LeagueGothic-CondensedRegular.otf);
        }


        /* Cooper hewitt */

        @font-face {
            font-family: cooperhewittbold;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/CooperHewitt-Bold.otf);
        }

        @font-face {
            font-family: cooperhewittbook;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/CooperHewitt-Book.otf);
        }

        @font-face {
            font-family: cooperhewittheavy;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/CooperHewitt-Heavy.otf);
        }

        @font-face {
            font-family: cooperhewittsemi;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/CooperHewitt-Semibold.otf);
        }

        @font-face {
            font-family: cooperhewittlight;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/CooperHewitt-Light.otf);
        }


        @font-face {
            font-family: cooperhewittthin;
            src: url(<?php echo ASSETS_PATH; ?>listeo_updated/fonts/CooperHewitt-Thin.otf);
        }


    </style>

    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>listinghub/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>listeo_updated/css/style.css">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>listeo_updated/css/colors/main.css" id="colors">
    <!-- Custom style -->
    <link href="<?php echo ASSETS_PATH; ?>listinghub/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo ASSETS_PATH; ?>listinghub/assets/css/responsiveness.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" id="jssDefault"
          href="<?php echo ASSETS_PATH; ?>listinghub/assets/css/colors/main.css">

    <link href="<?php echo ASSETS_PATH; ?>listinghub/assets/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>css/custom.css">


</head>

<body>
<div id="wrapper">
    <div id="dashboard">
        <div class="dashboard-content" style="margin-left: 0 !important;">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <div class="dashboard-list-box margin-top-0">
                        <h4 class="gray text-center" style="padding-top: 10px; padding-bottom: 10px;">
                            <a href="<?php echo base_url(); ?>"><img style="max-width: 200px;"
                                                                     src="<?php echo MEDIA_PATH . $header->headerLogo; ?>"
                                                                     alt=""></a>
                        </h4>
                        <div class="dashboard-list-box-static">

                            <form id="login_form" class="login" method="post"
                                  action="<?php echo base_url('api/login'); ?>"
                                  onsubmit="return false;">
                                <div class="my-profile">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password">
                                    </div>
                                    <div class="form-group text-center">
                                        <button class="button margin-top-15">LOG IN</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="copyrights">© <?php echo date('Y'); ?> <?php echo SITE_TITLE; ?>. All Rights Reserved.</div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/mmenu.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/chosen.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/slick.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/rangeslider.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/waypoints.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/counterup.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/tooltips.min.js"></script>
<script src="<?php echo ASSETS_PATH; ?>js/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="<?php echo ASSETS_PATH; ?>js/jquery.form.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_PATH; ?>listeo_updated/scripts/custom.js"></script>
<script src="<?php echo ASSETS_PATH; ?>js/custom/public/general.js"></script>
<script src="<?php echo ASSETS_PATH; ?>js/custom/public/login.js"></script>


</body>

</html>