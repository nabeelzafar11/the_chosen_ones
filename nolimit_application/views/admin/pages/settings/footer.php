<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Footer</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="footer_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/update_footer'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Footer</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $footer->footerLogo; ?>" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload footer Logo
                                                </span>
                                            </div>
                                        </div>
                                        <input type="file" name="footerLogo" value="" onchange="readURL(this);" style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;" />
                                        <input type="hidden" name="footerLogo" value="<?php echo $footer->footerLogo; ?>" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phoneText">Address Text</label>
                                        <input placeholder="Address" class="form-control" type="text" id="addressText" value="<?php echo $footer->addressText; ?>" name="addressText" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phoneText">Address</label>
                                        <input placeholder="Enter Address" class="form-control" type="text" id="address" value="<?php echo $footer->address; ?>" name="address" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phoneText">Related Links Left</label>
                                        <input placeholder="Related Links" class="form-control" type="text" id="relatedLinksLeft" value="<?php echo $footer->relatedLinksLeft; ?>" name="relatedLinksLeft" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phoneText">Related Links Right</label>
                                        <input placeholder="Related Links" class="form-control" type="text" id="relatedLinksRight" value="<?php echo $footer->relatedLinksRight; ?>" name="relatedLinksRight" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phone">Subscribe to our Newsletter Text</label>
                                        <input placeholder="Subscribe to our Newsletter" class="form-control" type="text" id="subscribeNewsletterText" value="<?php echo $footer->subscribeNewsletterText; ?>" name="subscribeNewsletterText" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phoneTwoText">Email Placeholder</label>
                                        <input placeholder="Email" class="form-control" type="text" id="emailPlaceholder" value="<?php echo $footer->emailPlaceholder; ?>" name="emailPlaceholder" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="copyright">Copy Right Text (use "%YEAR%" for year variable)</label>
                                        <input placeholder="Copy Right Text (use " %YEAR%" for year variable)" class="form-control" type="text" id="copyright" value="<?php echo $footer->copyright; ?>" name="copyright" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="facebook">Facebook Link</label>
                                        <input placeholder="Facebook Link" class="form-control" type="text" id="facebook" value="<?php echo $footer->facebook; ?>" name="facebook" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="twitter">Twitter Link</label>
                                        <input placeholder="Twitter Link" class="form-control" type="text" id="twitter" value="<?php echo $footer->twitter; ?>" name="twitter" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="google">Google Link</label>
                                        <input placeholder="Google Link" class="form-control" type="text" id="google" value="<?php echo $footer->google; ?>" name="google" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="linkedin">LinkedIn Link</label>
                                        <input placeholder="LinkedIn Link" class="form-control" type="text" id="linkedin" value="<?php echo $footer->linkedin; ?>" name="linkedin" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="youtube">Youtube Link</label>
                                        <input placeholder="Youtube Link" class="form-control" type="text" id="youtube" value="<?php echo $footer->youtube; ?>" name="youtube" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="skype">Skype Link</label>
                                        <input placeholder="Skype Link" class="form-control" type="text" id="skype" value="<?php echo $footer->skype; ?>" name="skype" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="vimeo">Vimeo Link</label>
                                        <input placeholder="Vimeo Link" class="form-control" type="text" id="vimeo" value="<?php echo $footer->vimeo; ?>" name="vimeo" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="instagram">Instagram Link</label>
                                        <input placeholder="Instagram Link" class="form-control" type="text" id="instagram" value="<?php echo $footer->instagram; ?>" name="instagram" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="pinterest">Pinterest Link</label>
                                        <input placeholder="Pinterest Link" class="form-control" type="text" id="pinterest" value="<?php echo $footer->pinterest; ?>" name="pinterest" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="rss">RSS Link</label>
                                        <input placeholder="RSS Link" class="form-control" type="text" id="rss" value="<?php echo $footer->rss; ?>" name="rss" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="behance">Behance Link</label>
                                        <input placeholder="Behance Link" class="form-control" type="text" id="behance" value="<?php echo $footer->behance; ?>" name="behance" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="github">Github Link</label>
                                        <input placeholder="Github Link" class="form-control" type="text" id="github" value="<?php echo $footer->github; ?>" name="github" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/footer.js"></script>