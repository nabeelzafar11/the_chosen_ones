<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Header</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="header_form" class="sign-in-form login" method="post" action="<?php echo base_url('api/update_header'); ?>" onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Header</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-4 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $header->headerLogo; ?>" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload Header Light Logo
                                                </span>
                                            </div>
                                        </div>
                                        <input type="file" name="headerLogo" value="" onchange="readURL(this);" style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;" />
                                        <input type="hidden" name="headerLogo" value="<?php echo $header->headerLogo; ?>" />
                                    </div>
                                </div>
                                <div class="col-sm-4 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $header->headerDarkLogo; ?>" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload Header Dark Logo
                                                </span>
                                            </div>
                                        </div>
                                        <input type="file" name="headerDarkLogo" value="" onchange="readURL(this);" style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;" />
                                        <input type="hidden" name="headerDarkLogo" value="<?php echo $header->headerDarkLogo; ?>" />
                                    </div>
                                </div>
                                <div class="col-sm-4 text-center">
                                    <div class="edit-profile-photo gray-bg imageDiv">
                                        <img src="<?php echo MEDIA_PATH . $header->favicon; ?>" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload">
                                                <span><i class="fa fa-upload"></i>
                                                    Upload Favicon
                                                </span>
                                            </div>
                                        </div>
                                        <input type="file" name="favicon" value="" onchange="readURL(this);" style="opacity: 0; z-index: 1000; position: absolute; bottom: 0;" />
                                        <input type="hidden" name="favicon" value="<?php echo $header->favicon; ?>" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="donateBntText">Donate Button Text</label>
                                        <input placeholder="Donate" class="form-control" type="text" id="donateBntText" value="<?php echo $header->menuButtonText; ?>" name="donateBntText" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="siteTitle">Site Title</label>
                                        <input placeholder="Site Title" class="form-control" type="text" id="siteTitle" value="<?php echo $header->siteTitle; ?>" name="siteTitle" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="seoTitle">Seo Title</label>
                                        <input placeholder="Seo Title" class="form-control" type="text" id="seoTitle" value="<?php echo $header->seoTitle; ?>" name="seoTitle" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="seoKeywords">Seo Keywords</label>
                                        <input placeholder="Seo Keywords" class="form-control" type="text" id="seoKeywords" value="<?php echo $header->seoKeywords; ?>" name="seoKeywords" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="seoDescription">
                                            Seo Description
                                        </label>
                                        <textarea name="seoDescription" id="seoDescription" rows="5" placeholder="Seo Description"><?php echo $header->seoDescription; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="button margin-top-15">Save Changes</button>
                </div>

            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/header.js"></script>