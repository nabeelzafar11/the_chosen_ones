<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Document Media Listings </h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" id="alert_div">

            <?php if ($this->session->flashdata('notification')) { ?>
                <?php $notificationData = $this->session->flashdata('notification'); ?>
                <?php if (isset($notificationData['status']) && isset($notificationData['message'])) { ?>
                    <div class="alert alert-<?php echo $notificationData['status']; ?>  alert-dismissible m-2" role="alert"><?php echo $notificationData['message']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>


    </div>

    <!-- content -->
    <div class="row">
        <div class="small-list-wrapper">
            <!-- small list wrapper -->
            <ul id="sortable1">
                <?php foreach ($medias as $media) { ?>
                    <li data-id="<?php echo $media->mediaId; ?>" style="width: 30%; display: inline-block; padding: 2px;">
                        <div class="small-listing-box white-bg">
                            <div class="small-list-img">
                            </div>
                            <div class="small-list-detail text-center" style="max-width: 100%;width: 100%;">
                                <img src="<?php echo MEDIA_PATH . "file.png"; ?>" style="float: none; min-height: 120px; width:120px; max-width: 100%;" />
                                <h4><?php echo $media->mediaName; ?></h4>
                                <h6><?php echo MEDIA_PATH . $media->mediaHref; ?></h6>
                            </div>
                            <div class="small-list-action text-center" style="float: none;">
                                <a href="javascript:void(0);" onclick="delete_item(this, <?php echo $media->mediaId; ?>, '<?php echo base_url('api/delete_documnet_media'); ?>');" class="theme-btn btn-square" data-toggle="tooltip" title="Delete Item"><i class="ti-trash"></i></a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div> <!-- ending of small list wrapper -->
        <div class="col-md-12">
            <?php if (count($medias) == 0) { ?>
                <h4>No record found.</h4>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    //var sortUrl = '<?php //echo base_url('api/sort_i'); 
                        ?>//';
</script>