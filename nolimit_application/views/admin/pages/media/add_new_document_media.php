<!-- Content
================================================== -->
<div class="dashboard-content">

    <!-- Titlebar -->
    <div id="titlebar">
        <div class="row">
            <div class="col-md-12">
                <h2>Document Media Listings / Add New Document Media</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form id="image_media_form" class="sign-in-form login" method="post"
                  action="<?php echo base_url('api/add_new_document_media'); ?>"
                  onsubmit="return false;">
                <div class="dashboard-list-box margin-top-0">
                    <h4 class="gray">Add New Document Media</h4>
                    <div class="dashboard-list-box-static">

                        <!-- Banner Content -->
                        <div class="my-profile cnt-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="mediaName">Upload Document</label>
                                        <input placeholder="Upload Document" class="form-control" type="file"
                                               id="mediaHref"
                                               name="mediaHref"/>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="mediaName">Document Name</label>
                                        <input placeholder="Document Name" class="form-control" type="text"
                                               id="mediaName"
                                               name="mediaName"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="button margin-top-15">Add</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>

    </div>

</div>
<!-- Content / End -->
<script src="<?php echo ASSETS_PATH; ?>js/custom/admin/add_new_image_media.js"></script>




