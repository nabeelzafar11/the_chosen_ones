<body>
    <div id="myLoader" style="display: none;">
        <i class="ion ion-ios-refresh-outline fa fa-spin"></i>
    </div>

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header Container
        ================================================== -->
        <header id="header-container" class="fixed fullwidth dashboard">

            <!-- Header -->
            <div id="header" class="not-sticky">
                <div class="container">

                    <!-- Left Side Content -->
                    <div class="left-side">

                        <!-- Logo -->
                        <div id="logo" class="text-center">
                            <a href="<?php echo base_url(); ?>">
                                <img src="<?php echo MEDIA_PATH . $header->headerDarkLogo; ?>" alt="">
                            </a>
                        </div>

                        <!-- Mobile Navigation -->
                        <div class="mmenu-trigger" style="opacity: 0;margin: 0;">
                            <button class="hamburger hamburger--collapse" type="button">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>


                    </div>
                    <!-- Left Side Content / End -->

                    <!-- Right Side Content / End -->
                    <div class="right-side dashboard-right-side">
                        <!-- Header Widget -->
                        <div class="header-widget">

                            <!-- User Menu -->
                            <div class="user-menu hide">
                                <div class="user-name">
                                    <span>
                                        <?php if ($admin->image) { ?>
                                            <img src="<?php echo MEDIA_PATH . $admin->image; ?>" alt="">
                                        <?php } else { ?>
                                            <img src="<?php echo ASSETS_PATH; ?>listeo_updated/images/user-placeholder.png" alt="">
                                        <?php } ?>
                                    </span>
                                    My Account
                                </div>
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('profile'); ?>"><i class="sl sl-icon-user"></i>
                                            My Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('logout'); ?>">
                                            <i class="sl sl-icon-power"></i>
                                            Logout
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <?php if (isset($addSelfLoveCareItem) && $addSelfLoveCareItem == true) { ?>
                                <a href="<?php echo base_url('admin/selflovecare/add_new_item/' . $monthId); ?>" class="button border with-icon">
                                    Add New Self Love/Self Care List Item
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>

                            <?php if (isset($backsCalendarDailyAffirmationButton) && $backsCalendarDailyAffirmationButton == true) { ?>
                                <a href="<?php echo base_url('admin/app-calendar-dailyaffirmation-listing'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>

                            <?php if (isset($backSelflovecareButton) && $backSelflovecareButton == true) { ?>
                                <a href="<?php echo base_url('admin/selflovecare-list/' . $monthId); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>

                            <?php if (isset($addPersonalAffirmationItem) && $addPersonalAffirmationItem == true) { ?>
                                <a href="<?php echo base_url('admin/personalaffirmation/add_new_item'); ?>" class="button border with-icon">
                                    Add New Love List Item
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backPersonalAffirmationButton) && $backPersonalAffirmationButton == true) { ?>
                                <a href="<?php echo base_url('admin/personalaffirmation-list'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>

                            <?php if (isset($addLoveItem) && $addLoveItem == true) { ?>
                                <a href="<?php echo base_url('admin/love/add_new_item'); ?>" class="button border with-icon">
                                    Add New Love List Item
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backLoveButton) && $backLoveButton == true) { ?>
                                <a href="<?php echo base_url('admin/love-list'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>

                            <?php if (isset($addNewBucketListItem) && $addNewBucketListItem == true) { ?>
                                <a href="<?php echo base_url('admin/bucketlist/add_new_bucketlist_item'); ?>" class="button border with-icon">
                                    Add New Bucket List Item
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backBucketListButton) && $backBucketListButton == true) { ?>
                                <a href="<?php echo base_url('admin/bucket-list'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>

                            <?php if (isset($addNewAbundanceItem) && $addNewAbundanceItem == true) { ?>
                                <a href="<?php echo base_url('admin/abundance/add_new_abundance_item'); ?>" class="button border with-icon">
                                    Add New Abundance List Item
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backAbundanceButton) && $backAbundanceButton == true) { ?>
                                <a href="<?php echo base_url('admin/abundance-list'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addHealthandBeautyItem) && $addHealthandBeautyItem == true) { ?>
                                <a href="<?php echo base_url('admin/healthandbeauty/add_new_item'); ?>" class="button border with-icon">
                                    Add New Health & Beauty List Item
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backHealthandBeautyButton) && $backHealthandBeautyButton == true) { ?>
                                <a href="<?php echo base_url('admin/healthandbeauty-list'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addCategoryButton) && $addCategoryButton == true) { ?>
                                <a href="<?php echo base_url('admin/category_listings/add_new_category'); ?>" class="button border with-icon">
                                    Add New Category
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backCategoryButton) && $backCategoryButton == true) { ?>
                                <a href="<?php echo base_url('admin/category_listings'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addMenuButton) && $addMenuButton == true) { ?>
                                <a href="<?php echo base_url('admin/menu_listings/add_new_menu'); ?>" class="button border with-icon">
                                    Add New Menu
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backMenuButton) && $backMenuButton == true) { ?>
                                <a href="<?php echo base_url('admin/menu_listings'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backselflovecaredashboardButton) && $backselflovecaredashboardButton == true) { ?>
                                <a href="<?php echo base_url('admin/app-selflovecaredashboard-listing'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backPositiveAffirmationDashboardButton) && $backPositiveAffirmationDashboardButton == true) { ?>
                                <a href="<?php echo base_url('admin/app-positiveaffirmationdashboard-listing'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backHomeServiceButton) && $backHomeServiceButton == true) { ?>
                                <a href="<?php echo base_url('admin/home_servies_listings'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addHomeServiceButton) && $addHomeServiceButton == true) { ?>
                                <a href="<?php echo base_url('admin/home_servies_listings/add_new_home_service'); ?>" class="button border with-icon">
                                    Add New Home Service
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backFeaturesButton) && $backFeaturesButton == true) { ?>
                                <a href="<?php echo base_url('admin/home_what_we_do'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addFeaturesButton) && $addFeaturesButton == true) { ?>
                                <a href="<?php echo base_url('admin/home_what_we_do/add_new_feature'); ?>" class="button border with-icon">
                                    Add New Service
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addCounterItemButton) && $addCounterItemButton == true) { ?>
                                <a href="<?php echo base_url('admin/add_new_counter_item'); ?>" class="button border with-icon">
                                    Add New Counter Item
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backCounterItemButton) && $backCounterItemButton == true) { ?>
                                <a href="<?php echo base_url('admin/about_counters'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addHomeSlideButton) && $addHomeSlideButton == true) { ?>
                                <a href="<?php echo base_url('admin/home_slider_listings/add_new_home_slider'); ?>" class="button border with-icon">
                                    Add New Home Slider
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backHomeSliderButton) && $backHomeSliderButton == true) { ?>
                                <a href="<?php echo base_url('admin/home_slider_listings'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>

                            <?php if (isset($addHomethirdSectionlButton) && $addHomethirdSectionlButton == true) { ?>
                                <a href="<?php echo base_url('admin/home_third_section_listings/add_new_testimonial'); ?>" class="button border with-icon">
                                    Add New Third Section Item
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backHomethirdsectionButton) && $backHomethirdsectionButton == true) { ?>
                                <a href="<?php echo base_url('admin/home_third_section_listings'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>

                            <?php if (isset($addHomeTestmonialButton) && $addHomeTestmonialButton == true) { ?>
                                <a href="<?php echo base_url('admin/home_testimonial_listings/add_new_testimonial'); ?>" class="button border with-icon">
                                    Add New Testimonial
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backHomeTestmonialButton) && $backHomeTestmonialButton == true) { ?>
                                <a href="<?php echo base_url('admin/home_testimonial_listings'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addBlogCategoryButton) && $addBlogCategoryButton == true) { ?>
                                <a href="<?php echo base_url('admin/blog_category_listings/add_new_blog_category'); ?>" class="button border with-icon">
                                    Add New Blog Category
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backBlogCategoryButton) && $backBlogCategoryButton == true) { ?>
                                <a href="<?php echo base_url('admin/blog_category_listings'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addBlogButton) && $addBlogButton == true) { ?>
                                <a href="<?php echo base_url('admin/blog_listings/add_new_blog'); ?>" class="button border with-icon">
                                    Add New Blog
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backBlogButton) && $backBlogButton == true) { ?>
                                <a href="<?php echo base_url('admin/blog_listings'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addImageMediaCategoryButton) && $addImageMediaCategoryButton == true) { ?>
                                <a href="<?php echo base_url('admin/image_media_listings/add_new_image_media'); ?>" class="button border with-icon">
                                    Add New Image Media
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backImageMediaCategoryButton) && $backImageMediaCategoryButton == true) { ?>
                                <a href="<?php echo base_url('admin/image_media_listings'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addDocumentMediaCategoryButton) && $addDocumentMediaCategoryButton == true) { ?>
                                <a href="<?php echo base_url('admin/document_media_listings/add_new_document_media'); ?>" class="button border with-icon">
                                    Add New Document Media
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backDocumentMediaCategoryButton) && $backDocumentMediaCategoryButton == true) { ?>
                                <a href="<?php echo base_url('admin/document_media_listings'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($headerMenuListing) && $headerMenuListing == true) { ?>
                                <a href="<?php echo base_url('admin/header_menus'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($footerMenuListing) && $footerMenuListing == true) { ?>
                                <a href="<?php echo base_url('admin/footer_menus'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>

                            <!-- <?php if (isset($subscribers) && count($subscribers)) { ?>
                                <a href="<?php echo base_url('admin/newsletter_export'); ?>" target="_blank" class="button border with-icon">
                                    Download
                                    <i class="sl sl-icon-cloud-download"></i>
                                </a>
                            <?php } ?> -->

                            <?php if (isset($addNewHeaderMenu) && $addNewHeaderMenu == true) { ?>
                                <a href="<?php echo base_url('admin/header_menus/add'); ?>" class="button border with-icon">
                                    Add New Header Menu
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addNewFooterMenu) && $addNewFooterMenu == true) { ?>
                                <a href="<?php echo base_url('admin/footer_menus/add'); ?>" class="button border with-icon">
                                    Add New Footer Menu
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addDashboardButton) && $addDashboardButton == true) { ?>
                                <a href="<?php echo base_url('admin/dashboard_listings/add_new_item'); ?>" class="button border with-icon">
                                    Add New Application Dashboard Item
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($addPositiveAffirmationDashboardButton) && $addPositiveAffirmationDashboardButton == true) { ?>
                                <a href="<?php echo base_url('admin/positiveaffirmationdashboard_listings/add_new_item'); ?>" class="button border with-icon">
                                    Add New Application Positive Affirmation Item
                                    <i class="sl sl-icon-plus"></i>
                                </a>
                            <?php } ?>
                            <?php if (isset($backDashboardButton) && $backDashboardButton == true) { ?>
                                <a href="<?php echo base_url('admin/app-dashboard-listing'); ?>" class="button border with-icon">
                                    Back
                                    <i class="sl sl-icon-arrow-left"></i>
                                </a>
                            <?php } ?>
                        </div>
                        <!-- Header Widget / End -->
                    </div>
                    <!-- Right Side Content / End -->

                </div>
            </div>
            <!-- Header / End -->

        </header>
        <div class="clearfix"></div>
        <!-- Header Container / End -->

        <!-- Dashboard -->
        <div id="dashboard">

            <!-- Navigation
            ================================================== -->

            <!-- Responsive Navigation Trigger -->
            <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i>
                Dashboard Navigation
            </a>

            <div class="dashboard-nav">
                <div class="dashboard-nav-inner">

                    <ul data-submenu-title="Main">
                        <li <?php if ($page == DASHBOARD) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/dashboard'); ?>"><i class="sl sl-icon-settings"></i>
                                Dashboard
                            </a>
                        </li>
                    </ul>
                    <!-- <ul data-submenu-title="Calendar">
                        <li <?php if ($page == DAILYAFFIRMATION_LISTINGS) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/app-calendar-dailyaffirmation-listing'); ?>"><i class="sl sl-icon-layers"></i>
                                Calendar Daily Affirmations
                            </a>
                        </li>
                    </ul>
                    <ul data-submenu-title="Application Dashboard">
                        <li <?php if ($page == DASHBOARD_LISTINGS) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/app-dashboard-listing'); ?>"><i class="sl sl-icon-layers"></i>
                                Application Dashboard Listings
                            </a>
                        </li>
                    </ul>
                    <ul data-submenu-title="Self Love/Self Care List">
                        <li <?php if ($page == SELLOVECARE_DASHBOARD_LISTINGS) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/app-selflovecaredashboard-listing'); ?>"><i class="sl sl-icon-layers"></i>
                                Self Love/Self Care Dashboard
                            </a>
                        </li>
                        <li <?php if ($page == SELFLOVECARE_LIST) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/selflovecare-monthly-list'); ?>"><i class="sl sl-icon-layers"></i>
                                Self Love/Self Care List
                            </a>
                        </li>
                    </ul>
                    <ul data-submenu-title="Positive Affirmations">
                        <li <?php if ($page == POSITIVE_AFFIRMATION_DASHBOARD_LISTINGS) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/app-positiveaffirmationdashboard-listing'); ?>"><i class="sl sl-icon-layers"></i>
                                Dashboard List
                            </a>
                        </li>
                        <li <?php if ($page == ABUNDANCE_LIST) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/abundance-list'); ?>"><i class="sl sl-icon-layers"></i>
                                Abundance
                            </a>
                        </li>
                        <li <?php if ($page == HEALTHANDBEAUTY_LIST) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/healthandbeauty-list'); ?>"><i class="sl sl-icon-layers"></i>
                                Health & Beauty
                            </a>
                        </li>
                        <li <?php if ($page == LOVE_LIST) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/love-list'); ?>"><i class="sl sl-icon-layers"></i>
                                Love
                            </a>
                        </li>
                        <li <?php if ($page == PERSONAL_AFFIRMATION_LIST) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/personalaffirmation-list'); ?>"><i class="sl sl-icon-layers"></i>
                                My Personal Affirmation
                            </a>
                        </li>
                    </ul>
                    <ul data-submenu-title="Application Bucket List">
                        <li <?php if ($page == BUCKET_LIST) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/bucket-list'); ?>"><i class="sl sl-icon-layers"></i>
                                Bucket List
                            </a>
                        </li>
                    </ul> -->
                    <ul data-submenu-title="Admin Settings">

                        <li <?php if ($page == CATEGORY_LISTINGS) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/category_listings'); ?>"><i class="sl sl-icon-layers"></i>
                                Category Listings
                            </a>
                        </li>
                        <!-- <li <?php if ($page == IMAGE_MEDIA_LISTINGS) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/image_media_listings'); ?>"><i class="sl sl-icon-layers"></i>
                                Image Media Listings
                            </a>
                        </li> -->
                        <li <?php if ($page == DOCUMENT_MEDIA_LISTINGS) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/document_media_listings'); ?>"><i class="sl sl-icon-layers"></i>
                                Document Media Listings
                            </a>
                        </li>
                        <li <?php if ($page == NEWSLETTER_LISTINGS) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/newsletter_listings'); ?>"><i class="sl sl-icon-layers"></i>
                                Newsletter Listings
                            </a>
                        </li>
                        <li <?php if ($page == HEADER_MENUS) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/header_menus'); ?>"><i class="sl sl-icon-layers"></i>
                                Header Menu Listings
                            </a>
                        </li>
                        <!-- <li <?php if ($page == FOOTER_MENUS) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/footer_menus'); ?>"><i class="sl sl-icon-layers"></i>
                                Footer Menu Listings
                            </a>
                        </li> -->
                    </ul>
                    <ul data-submenu-title="Home Page">
                        <li <?php if ($page == HOME_FIRST_SECTION) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/show_first_section'); ?>"><i class="sl sl-icon-layers"></i>
                                First Section
                            </a>
                        </li>
                        <li <?php if ($page == HOME_SERVICES) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/home_servies_listings'); ?>"><i class="sl sl-icon-layers"></i>
                                Home Servies
                            </a>
                        </li>
                        <li <?php if ($page == HOME_FOURTH_SECTION) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/show_home_third_section'); ?>"><i class="sl sl-icon-layers"></i>
                                Home Third Section
                            </a>
                        </li>
                        <li <?php if ($page == HOME_WHAT_WE_DO) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/home_what_we_do'); ?>"><i class="sl sl-icon-layers"></i>
                                What We DO
                            </a>
                        </li>
                        <li <?php if ($page == CONTACT_SECTION) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/home_contact_section'); ?>"><i class="sl sl-icon-layers"></i>
                                Home Contact Section
                            </a>
                        </li>
                        <li <?php if ($page == VIDEO_SECTION) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/home_video_section'); ?>"><i class="sl sl-icon-layers"></i>
                                Home Video Section
                            </a>
                        </li>
                        <!-- <li <?php if ($page == HOME_SLIDER) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/home_slider_listings'); ?>"><i class="sl sl-icon-layers"></i>
                                Home Slider
                            </a>
                        </li>
                        <li <?php if ($page == HOME_ABOUT_SECTION) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/home_about_section_one'); ?>"><i class="sl sl-icon-layers"></i>
                                Home About Section One
                            </a>
                        </li>
                        <!--                        <li <?php if ($page == HOME_ABOUT_SECTION_TWO) { ?> class="active" <?php } ?>><a
                                href="<?php echo base_url('admin/home_about_section_two'); ?>"><i class="sl sl-icon-layers"></i>
                                Home About Section Two
                            </a>
                        </li>-->
                        <!-- <li <?php if ($page == HOME_TESTIMONIAL_THIRD_SECTION) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/home_third_section_listings'); ?>"><i class="sl sl-icon-layers"></i>
                                Home About Section Three
                            </a>
                        </li> -->

                        <!--                    <li --><?php //if ($page == HOME_MEDIA_SECTION) {                
                                                        ?>
                        <!-- class="active" --><?php //}                
                                                ?>
                        <!--><a-->
                        <!--                                href="--><?php //echo base_url('admin/home_media_section');                
                                                                        ?>
                        <!--"><i class="sl sl-icon-layers"></i>-->
                        <!--                            Home Media Section-->
                        <!--                        </a>-->
                        <!--                    </li>-->

                        <!-- <li <?php if ($page == HOME_TESTIMONIAL_SECTION) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/home_testimonial_section'); ?>"><i class="sl sl-icon-layers"></i>
                                Home Testimonial Section
                            </a>
                        </li>
                        <li <?php if ($page == HOME_TESTIMONIAL) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/home_testimonial_listings'); ?>"><i class="sl sl-icon-layers"></i>
                                Home Testimonial
                            </a>
                        </li> -->
                    </ul>
                    <ul data-submenu-title="About Page">
                        <li <?php if ($page == ABOUT_CONTENT) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/about_content'); ?>"><i class="sl sl-icon-layers"></i>
                                About Page Content
                            </a>
                        </li>
                        <li <?php if ($page == ABOUT_COUNTERS) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/about_counters'); ?>"><i class="sl sl-icon-layers"></i>
                                Count Items
                            </a>
                        </li>
                    </ul>
                    <ul data-submenu-title="Contact Page">
                        <li <?php if ($page == CONTACT_CONTENT) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/contact_content'); ?>"><i class="sl sl-icon-layers"></i>
                                Contact Page Content
                            </a>
                        </li>
                        <li <?php if ($page == CONTACT_FORM) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/contact_form'); ?>"><i class="sl sl-icon-layers"></i>
                                Contact Form
                            </a>
                        </li>
                    </ul>
                    <ul data-submenu-title="Donate Page">
                        <li <?php if ($page == DONATE_CONTENT) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/donate_content'); ?>"><i class="sl sl-icon-layers"></i>
                                Donate Page Content
                            </a>
                        </li>
                    </ul>
                    <ul data-submenu-title="General Settings">
                        <li <?php if ($page == HEADER) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/header'); ?>"><i class="sl sl-icon-layers"></i>
                                Header
                            </a>
                        </li>
                        <li <?php if ($page == FOOTER) { ?> class="active" <?php } ?>><a href="<?php echo base_url('admin/footer'); ?>"><i class="sl sl-icon-layers"></i>
                                Footer
                            </a>
                        </li>
                    </ul>
                    <ul data-submenu-title="Account">
                        <!--                    <li --><?php //if ($page == PROFILE) {                
                                                        ?>
                        <!-- class="active" --><?php //}                
                                                ?>
                        <!--><a-->
                        <!--                                href="--><?php //echo base_url('profile');                
                                                                        ?>
                        <!--"><i class="sl sl-icon-user"></i>-->
                        <!--                            My Profile-->
                        <!--                        </a>-->
                        <!--                    </li>-->
                        <li><a href="<?php echo base_url('logout'); ?>"><i class="sl sl-icon-power"></i>
                                Logout
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
            <!-- Navigation / End -->