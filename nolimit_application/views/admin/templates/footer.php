<!-- Copyrights -->
<div class="copyrights">
    <?php echo str_replace("%YEAR%", date('Y'), $footer->copyright); ?>
</div>
</div>
<!-- Dashboard / End -->
</div>
<!-- Wrapper / End -->

<div class="hide">
    <form id="delete_form" class="hide" method="post">
        <input type="hidden" name="id"/>
    </form>
</div>

<div class="hide">
    <form id="trigger_is_active_form" class="hide" method="post">
        <input type="hidden" name="id"/>
        <input type="hidden" name="isActive"/>
    </form>
</div>