<?php

defined('BASEPATH') or exit('No direct script access allowed');

require 'php-mailer/class.phpmailer.php';

class MY_Controller extends CI_Controller {

    public $user;
    public $pageTitle;
    public $seoTitle;
    public $seoDescription;
    public $seoKeywords;
    public $header;
    public $userTypes = array("1" => "DEVELOPER", "2" => "ADMIN", "3" => 'USER');
    public $countries = array(
        "AD" => "ANDORRA",
        "AD" => "ANDORRA",
        "AM" => "ARMENIA",
        "AR" => "ARGENTINA",
        "AS" => "AMERICAN SAMOA",
        "AT" => "AUSTRIA",
        "AU" => "AUSTRALIA",
        "AZ" => "AZERBAIJAN",
        "BA" => "BOSNIA AND HERZEGOVINA",
        "BD" => "BANGLADESH",
        "BE" => "BELGIUM",
        "BG" => "BULGARIA",
        "BN" => "BRUNEI",
        "BR" => "BRAZIL",
        "BY" => "BELARUS",
        "CA" => "CANADA",
        "CH" => "SWITZERLAND",
        "CN" => "CHINA, PEOPLES REPUBLIC",
        "CO" => "COLOMBIA",
        "CU" => "CUBA",
        "CY" => "CYPRUS",
        "CZ" => "CZECH REPUBLIC, THE",
        "DE" => "GERMANY",
        "DK" => "DENMARK",
        "DZ" => "ALGERIA",
        "EC" => "ECUADOR",
        "EE" => "ESTONIA",
        "ES" => "SPAIN",
        "FI" => "FINLAND",
        "FM" => "MICRONESIA, FEDERATED STATES OF",
        "FO" => "FAROE ISLANDS",
        "FR" => "FRANCE",
        "GB" => "UNITED KINGDOM",
        "GE" => "GEORGIA",
        "GF" => "FRENCH GUYANA",
        "GG" => "GUERNSEY",
        "GL" => "GREENLAND",
        "GP" => "GUADELOUPE",
        "GR" => "GREECE",
        "GU" => "GUAM",
        "HR" => "CROATIA",
        "HU" => "HUNGARY",
        "IC" => "CANARY ISLANDS, THE",
        "ID" => "INDONESIA",
        "IL" => "ISRAEL",
        "IN" => "INDIA",
        "IS" => "ICELAND",
        "IT" => "ITALY",
        "JE" => "JERSEY",
        "JP" => "JAPAN",
        "KG" => "KYRGYZSTAN",
        "KH" => "CAMBODIA",
        "KR" => "KOREA, REPUBLIC OF (SOUTH K.)",
        "KV" => "KOSOVO",
        "KZ" => "KAZAKHSTAN",
        "LI" => "LIECHTENSTEIN",
        "LT" => "LITHUANIA",
        "LU" => "LUXEMBOURG",
        "LV" => "LATVIA",
        "MA" => "MOROCCO",
        "MC" => "MONACO",
        "MD" => "MOLDOVA, REPUBLIC OF",
        "ME" => "MONTENEGRO, REPUBLIC OF",
        "MG" => "MADAGASCAR",
        "MH" => "MARSHALL ISLANDS",
        "MK" => "MACEDONIA, REPUBLIC OF",
        "MN" => "MONGOLIA",
        "MP" => "COMMONWEALTH NO. MARIANA ISLANDS",
        "MQ" => "MARTINIQUE",
        "MV" => "MALDIVES",
        "MX" => "MEXICO",
        "MY" => "MALAYSIA",
        "NC" => "NEW CALEDONIA",
        "NL" => "NETHERLANDS, THE",
        "NO" => "NORWAY",
        "NZ" => "NEW ZEALAND",
        "PG" => "PAPUA NEW GUINEA",
        "PH" => "PHILIPPINES, THE",
        "PK" => "PAKISTAN",
        "PL" => "POLAND",
        "PR" => "PUERTO RICO",
        "PT" => "PORTUGAL",
        "PW" => "PALAU",
        "RE" => "REUNION, ISLAND OF",
        "RO" => "ROMANIA",
        "RS" => "SERBIA, REPUBLIC OF",
        "RU" => "RUSSIAN FEDERATION, THE",
        "SE" => "SWEDEN",
        "SG" => "SINGAPORE",
        "SH" => "SAINT HELENA",
        "SI" => "SLOVENIA",
        "SK" => "SLOVAKIA",
        "SM" => "SAN MARINO",
        "SZ" => "SWAZILAND",
        "TH" => "THAILAND",
        "TN" => "TUNISIA",
        "TR" => "TURKEY",
        "TW" => "TAIWAN",
        "UA" => "UKRAINE",
        "US" => "UNITED STATES OF AMERICA",
        "UZ" => "UZBEKISTAN",
        "VI" => "VIRGIN ISLANDS (US)",
        "XY" => "ST. BARTHELEMY",
        "YT" => "MAYOTTE",
        "ZA" => "SOUTH AFRICA",
    );

    public function __construct() {
        parent::__construct();
        //        if (!(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443)) {
        //            if ($_SERVER['HTTP_HOST'] == "localhost") {
        //                redirect("https://localhost/nolimit/public/", "refresh");
        //            } else {
        //                redirect("https://" . $_SERVER['HTTP_HOST'], "refresh");
        //            }
        //        }
        $this->get_user();
        $this->pageTitle = '';
        $this->seoTitle = '';
        $this->seoDescription = '';
        $this->seoKeywords = '';
        $this->header = $this->header_model->get_record();
        $this->footer = $this->footer_model->get_record();

        if ($this->session->redirect_data && isset($this->session->redirect_data['redirectUrl'])) {
            $redirect = $this->session->redirect_data['redirectUrl'];
            $this->session->unset_userdata('redirect_data');
            redirect($redirect, 'refresh');
        }
        $this->pageTitle = '';
        $this->seoTitle = '';
        $this->seoDescription = '';
        $this->seoKeywords = '';
    }

    public function get_user() {
        if ($this->mobile_api_check()) {
            if ($this->input->post('userToken')) {
                $whereConditionArray = array('loginId' => decode($this->input->post('userToken')), 'loginIsLogout' => 0);
                $result = $this->login_model->get_record($whereConditionArray);
                if ($result) {
                    $whereConditionArray = array('userId' => $result->loginUserId);
                    $this->user = $this->user_model->get_record($whereConditionArray);
                    $this->user = ($this->user && $this->user->isActive && $this->user->isDeleted == 0) ? $this->user : '';
                } else {
                    $this->user = '';
                }
            } else {
                $this->user = '';
            }
        } elseif ($this->session->loginId) {
            $whereConditionArray = array('loginId' => decode($this->session->loginId), 'loginIsLogout' => 0);
            $result = $this->login_model->get_record($whereConditionArray);
            if ($result) {
                $whereConditionArray = array('userId' => $result->loginUserId);
                $this->user = $this->user_model->get_record($whereConditionArray);
                $this->user = ($this->user && $this->user->isActive && $this->user->isDeleted == 0) ? $this->user : '';
            } else {
                $this->session->unset_userdata('loginId');
                $this->user = '';
            }
        } else {
            $this->user = '';
        }
    }

    //    public function mobile_api_check() {
    //        $this->load->library('form_validation');
    //        $this->form_validation->set_rules('id', 'Id', 'trim|required');
    //        if ($this->form_validation->run() == FALSE) {
    //            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
    //        } else {
    //            if ($this->input->post('id') != "mobileApp") {
    //                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
    //            }
    //        }
    //    }
    public function mobile_api_check() {
        if ($this->input->post('appToken') && decode($this->input->post('appToken')) == "mobileApp") {
            return true;
        } else {
            return false;
        }
    }

    public function admin_login_check() {
        $this->login_check();
        if ($this->user->typeId > ADMIN) {
            if ($this->input->is_ajax_request()) {
                $this->send_api_respone('login', '', 'refresh', "S'il vous plait Connectez-vous d'abord.");
            } else {
                redirect('dashboard', 'refresh');
            }
        }
    }

    public function user_login_check() {
        $this->login_check();
        if ($this->user->typeId == USER) {
            if ($this->input->is_ajax_request()) {
                $this->send_api_respone('login', '', 'refresh', "S'il vous plait Connectez-vous d'abord.");
            } else {
                redirect('admin', 'refresh');
            }
        }
    }

    public function get_category_link($homeDataAboutTwoButtonCategoryId) {
        $resultLink = '';
        while ($homeDataAboutTwoButtonCategoryId > 0) {
            $whereCondition = array('categoryId' => $homeDataAboutTwoButtonCategoryId);
            $homeDataAboutTwoCategory = $this->category_model->get_record($whereCondition);
            if ($homeDataAboutTwoCategory) {
                if ($resultLink) {
                    $resultLink = "/" . $resultLink;
                }
                $resultLink = $homeDataAboutTwoCategory->categorySlug . $resultLink;
            } else {
                $resultLink = base_url();
                break;
            }
            $homeDataAboutTwoButtonCategoryId = $homeDataAboutTwoCategory->categoryParentId;
        }
        if ($resultLink) {
            $resultLink = base_url() . $resultLink;
        }
        return $resultLink;
    }

    public function login_check() {

        if (!$this->user) {

            $notificationData = array(
                'notification' => array(
                    'status' => 'warning',
                    'message' => "Please login into the application!"
                )
            );
            $this->session->set_flashdata($notificationData);
            if ($this->input->is_ajax_request() || $this->mobile_api_check()) {
                $this->send_api_respone('login', '', 'refresh', "Please login first.", '2');
            } else {
                redirect('admin', 'refresh');
            }
        }
    }

    public function not_login_check() {

        if ($this->user) {
            if ($this->input->is_ajax_request() || $this->mobile_api_check()) {
                $this->send_api_respone('', '', '', 'Already logged in.');
            } else {
                if ($this->user->typeId == 3) {
                    redirect('/', 'refresh');
                } else {
                    redirect('/admin/dashboard', 'refresh');
                }
            }
        }
    }

    public function send_api_respone($page, $route, $status, $message = null, $mobileResponse = null, $mobileMessage = null) {
        if ($this->input->is_ajax_request() || $this->mobile_api_check()) {
            if ($this->mobile_api_check()) {
                if ($status == "success" || $status == 'refresh') {
                    $status = "1";
                } else {
                    $status = "0";
                }
                if ($mobileResponse) {
                    $status = $mobileResponse;
                }
                if (is_string($message)) {
                    $message = strip_tags($message);
                }
                if ($mobileMessage) {
                    $message = $mobileMessage;
                }
            }
            $res = array(
                'status' => $status,
                'message' => $message,
                'id' => $mobileResponse
            );
            //            header('Content-Type: application/json');
            echo json_encode($res);
            die();
        } else {
            if ($status != 'refresh') {
                $newdata = array(
                    $page . '_data1' => array(
                        'status' => $status,
                        'message' => $message
                    )
                );
                $this->session->set_userdata($newdata);
            }
            redirect($route, 'refresh');
        }
    }

    public function view($page = 'home', $data = array()) {
        
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            show_404();
        }

        $this->set_client_id();

        $data['admin'] = $this->user;

        $this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);
        $this->load->view('templates/footer_script', $data);
    }

    public function login_view($page = 'login', $data = array()) {
        if (!file_exists(APPPATH . 'views/admin/pages/' . $page . '.php')) {
            show_404();
        }
        $data['header'] = $this->header;

        $this->load->view('admin/pages/' . $page, $data);
    }

    public function admin_view($page = 'dashboard', $data = array()) {
        if (!file_exists(APPPATH . 'views/admin/pages/' . $page . '.php')) {
            show_404();
        }

        $data['admin'] = $this->user;
        $data['header'] = $this->header;
        $data['footer'] = $this->footer;

        $this->load->view('admin/templates/head', $data);
        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/' . $page, $data);
        $this->load->view('admin/templates/footer', $data);
        $this->load->view('admin/templates/foot', $data);
    }

    public function listing_view($page = 'dashboard', $data = array()) {
        if (!file_exists(APPPATH . 'views/public/pages/' . $page . '.php')) {
            show_404();
        }

        $data['admin'] = $this->user;
        $this->load->view('public/pages/' . $page, $data);
    }

    public function invoice_view($page = 'dashboard', $data = array()) {
        if (!file_exists(APPPATH . 'views/public/pages/' . $page . '.php')) {
            show_404();
        }

        $data['admin'] = $this->user;
        $data['header'] = $this->header;
        $data['footer'] = $this->footer;

        $this->load->view('public/pages/' . $page, $data);
    }

    public function public_view($page = 'home', $data = array()) {
        if (!file_exists(APPPATH . 'views/public/pages/' . $page . '.php')) {
            show_404();
        }
        $data['admin'] = $this->user;
        $data['header'] = $this->header;
        $data['footer'] = $this->footer;
        $whereCondition = array('footerMenuIsActive' => 1);
        $headerMenus = $this->headermenu_model->get_records($whereCondition);

        foreach ($headerMenus as $key => $fm) {
            $headerMenus[$key]->footerMenuHrefOutsource = trim($fm->footerMenuHref);
            if ($fm->footerMenuCategoryId > 0) {
                $headerMenus[$key]->footerMenuHref = $this->get_category_link($fm->footerMenuCategoryId);
            }
        }

        $data['headerMenus'] = $headerMenus;
        if ($this->pageTitle) {
            $data['header']->siteTitle = $this->pageTitle;
        }
        if ($this->seoTitle) {
            $data['header']->seoTitle = $this->seoTitle;
        }
        if ($this->seoDescription) {
            $data['header']->seoDescription = $this->seoDescription;
        }
        if ($this->seoKeywords) {
            $data['header']->seoKeywords = $this->seoKeywords;
        }

        $data['header']->menuButtonCategorySlug = $this->get_category_link($data['header']->menuButtonCategoryId);
        
        $whereCondition = array('footerMenuIsActive' => 1);
        $footerMenus = $this->footermenu_model->get_records($whereCondition);

        foreach ($footerMenus as $key => $fm) {
            $footerMenus[$key]->footerMenuHrefOutsource = trim($fm->footerMenuHref);
            $footerMenus[$key]->footerMenuHref = $this->get_category_link($fm->footerMenuCategoryId);
        }
        $data['footerMenus'] = $footerMenus;

        $this->load->view('public/templates/head', $data);
        $this->load->view('public/templates/header', $data);
        $this->load->view('public/pages/' . $page, $data);
        $this->load->view('public/templates/footer', $data);
        $this->load->view('public/templates/foot', $data);
    }

    public function send_mail($email, $message, $subject) {

        $mail = new PHPMailer();                              // Passing `true` enables exceptions
        try {
            //Server settings
            //            $mail->SMTPDebug = 1;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'zeikhtech@gmail.com';                 // SMTP username
            $mail->Password = 'ZeikhTechnologies';
            $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 587; // or 587                                 // TCP port to connect to
            //Recipients
            $mail->setFrom(SITE_EMAIL, SITE_TITLE);
            $mail->addAddress($email);

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $message;

            $mail->send();
            return 1;
        } catch (Exception $e) {
            print_r($e);
            die();
            return 0;
        }
    }

    public function send_email2($page = 'home', $data = array()) {
        $data['admin'] = $this->user;
        $data['header'] = $this->header;

        $mail = new PHPMailer(true);                     // Passing `true` enables exceptions
        try {
            $mail->isSMTP();                                      // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'zeikhtech@gmail.com';                 // SMTP username
            $mail->Password = 'ZeikhTechnologies';
            $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 587; // or 587  

            $mail->CharSet = 'UTF-8';
            $mail->setFrom(SITE_EMAIL, SITE_TITLE);
            $mail->addAddress($data['sendTo']);
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $data['subject'];
            $mail->Body = $this->load->view('email_templates/' . $page, $data, true);
            $mail->send();
            return 1;
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e);
            die();
            return 0;
        }
    }

    public function send_cont_email($data = array()) {
        $data['admin'] = $this->user;
        $data['header'] = $this->header;
        $mail = new PHPMailer\PHPMailer\PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            if ($this->header->email && $this->header->password && $this->header->port && $this->header->host && $this->header->encryption) {
                $mail->isSMTP();
                //$mail->SMTPDebug = 2;
                $mail->Host = $this->header->host;
                $mail->Port = $this->header->port;
                $mail->SMTPAuth = true;
                $mail->Username = $this->header->email;
                $mail->Password = $this->header->password;
                $mail->SMTPSecure = 'ssl';
                $mail->setFrom($this->header->email, SITE_TITLE);
            } else {
                $mail->setFrom(FROM_EMAIL, SITE_TITLE);
            }
            $mail->CharSet = 'UTF-8';
            $mail->addAddress($this->header->email); //$this->header->email);
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $data['subject'];
            $mail->Body = $this->load->view('email_templates/contact_us_email', $data, true);
            $mail->send();
            return 1;
        } catch (PHPMailer\PHPMailer\Exception $e) {
            return 0;
        }

        //        $mail->IsSMTP();
        //        $mail->Host = 'ssl://smtp.gmail.com';
        //        $mail->SMTPAuth = true;
        //        $mail->Username = $this->header->email;
        //        $mail->Password = $this->header->password;
        //        $mail->Port =  $this->header->port;
        //        $mail->SMTPDebug = 2;
        //        $mail->SMTPSecure = 'ssl';
        //        $mail->SetFrom($this->header->email, 'Name');
        //        $mail->AddAddress('nabeelzafar.hsra@gmail.com', 'HisName');
        //        $mail->Subject = 'Subject';
        //        $mail->Subject = "Here is the subject";
        //        $mail->Body = "This is the HTML message body <b>in bold!</b>";
        //        $mail->AltBody = "This is the body in plain text for non-HTML mail    clients";
        //        if (!$mail->Send()) {
        //            echo 'Error : ' . $mail->ErrorInfo;
        //        } else {
        //            echo 'Ok!!';
        //        }
        //        die();
    }

    public function send_email3($message, $data = array()) {
        $data['admin'] = $this->user;
        $data['header'] = $this->header;
        $mail = new PHPMailer\PHPMailer\PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            $mail->CharSet = 'UTF-8';
            $mail->setFrom(FROM_EMAIL, SITE_TITLE);
            $mail->addAddress($data['email']);
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $data['subject'];
            $mail->Body = $message;
            $mail->send();
            return 1;
        } catch (PHPMailer\PHPMailer\Exception $e) {
            return 0;
        }
    }

    public function action_controll($actionData) {
        $this->load->model('site_action_model');
        $this->site_action_model->insert_record($actionData);
    }

    public function get_latitude_and_longitude($address) {

        $prepAddr = str_replace(' ', '+', $address);
        //        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAePzeIOmqXfFi5IrEuRk2rt-pb9PsYWjY&v=3&amp;libraries=places,geometry"></script>
        $url = 'https://maps.google.com/maps/api/geocode/json?key=AIzaSyAePzeIOmqXfFi5IrEuRk2rt-pb9PsYWjY&address=' . $prepAddr . '&sensor=false';


        $c = curl_init();
        // echo "<pre>";print_r($c);exit();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $url);
        $output = curl_exec($c);
        curl_close($c);
        $output = json_decode($output, true);
        if ($output['status'] != "REQUEST_DENIED" && $output['status'] != "ZERO_RESULTS" && $output) {
            $latitude = $output['results'][0]['geometry']['location']['lat'];
            $longitude = $output['results'][0]['geometry']['location']['lng'];
            $address_comp = $output['results'][0]['address_components'];
            $city = $state = $country = $zipCode = '';
            if (is_array($address_comp)) {
                foreach ($address_comp as $key => $value) {
                    switch ($value['types'][0]) {
                        case 'locality':
                            $city = $value['long_name'];
                            break;

                        case 'administrative_area_level_1':
                            $state = $value['long_name'];
                            break;

                        case 'country':
                            $country = $value['long_name'];
                            break;

                        case 'postal_code':
                            $zipCode = $value['long_name'];
                            break;
                    }
                }
            }

            return array('lat' => $latitude, 'long' => $longitude, 'city' => $city, 'state' => $state, 'country' => $country, 'zipCode' => $zipCode);
        } else {
            return array();
        }
    }

}
