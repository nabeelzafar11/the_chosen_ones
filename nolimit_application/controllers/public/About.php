<?php

defined('BASEPATH') or exit('No direct script access allowed');

class About extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['homeData'] = $this->home_data_model->get_record();

        $servicesWhereCondition = array('counterIsActive' => 1);
        $data['counters'] = $this->about_counter_model->get_records($servicesWhereCondition);

        $this->public_view('about', $data);
    }
}
