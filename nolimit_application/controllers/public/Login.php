<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->not_login_check();
    }

    public function show_login() {
        $this->public_view('signin');
    }

    public function show_signup() {
        $this->public_view('signup');
    }

    public function forgot_password() {
        $this->public_view('forgot_password');
    }

    public function reset_password($resetLink) {
        $whereCondition = array("resetToken" => "$resetLink");
        $data = array();
        $data["resetLink"] = $resetLink;
        $data["results"] = $this->user_model->get_record($whereCondition);
        $this->public_view('reset_password', $data);
        
    }

}
