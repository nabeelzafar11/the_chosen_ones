<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Privateroutes extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_check();
    }

    public function self_love() {
        $user_id = $this->user->userId;
        $whereCondition = array("selflovecare.isDeleted" => 0, "user_selflovecare_list.user_id" => $user_id);
        $data["user_list"] = $this->userselflovecare_model->get_records_join_bucketlist($whereCondition);
        $curr_month = date("m");
        $data["selflovecare_dashboard"] = $this->selflovecaredashboard_model->get_record(array("month" => $curr_month));
        $this->public_view('self_love', $data);
    }

    public function self_love_self_care_list_add_item() {
        $user_id = $this->user->userId;
        $whereCondition = " WHERE (user_id = 0 OR user_id = '$user_id') AND isDeleted = 0 AND id NOT IN (SELECT selflovecare_id FROM user_selflovecare_list WHERE user_id = $user_id) ORDER BY id ASC, sortOrder ASC";
        $data["list"] = $this->selflovecare_model->fetch_add_new_abundance_items($whereCondition);
        $this->public_view('self_love_add_item', $data);
    }

    public function calendar() {
        $user_id = $this->user->userId;
        $month = date('m');
        $year = date('Y');
        $date = date("Y-m-d");
        $datecompelte = date("F d Y");
        $query = "SELECT DISTINCT(DATE(dateandtime)) as dates from events WHERE MONTH(dateandtime) = '$month' AND YEAR(dateandtime) = '$year' AND user_id = $user_id";
        $records = $this->userselflovecare_model->generic_query($query);
        $dates = array();
        foreach ($records as $r) {
            array_push($dates, $r->dates);
        }
        $data["dates"] = $dates;
        $data["date"] = $date;
        $data["datecompelte"] = $datecompelte;
        $data["calendardailyaffirmation"] = $this->calendardailyaffirmation_model->get_record(array("month" => $month));
        $this->public_view('calendar', $data);
    }

    public function gratitude_journal() {
        $data["header"] = $this->header_model->custom_query(" gratitude_mobile ");
        $user_id = $this->user->userId;
        $whereCondition = array("user_id" => $user_id);
        $records = $this->gratitudejournal_model->get_record($whereCondition);
        if (!$records) {
            $current_date = date("Y-m-d");
            $records = (object) array("h1_title" => "Things I am grateful for today", "h1_message" => "", "h1_date" => "$current_date", "h2_title" => "Things I am grateful for about my body", "h2_message" => "", "h2_date" => "$current_date", "h3_title" => "Things I am grateful for about my family", "h3_message" => "", "h3_date" => "$current_date");
        }
        $data["records"] = $records;
        $this->public_view('gratitude_journal', $data);
    }

    public function highest_goods_of_all() {
        $data["header"] = $this->header_model->custom_query(" highestgoodofall_mobile ");
        $user_id = $this->user->userId;
        $whereCondition = array("isDeleted" => 0, "user_id" => $user_id);
        $data["records"] = $this->highestgoodofall_model->get_records($whereCondition);
        $this->public_view('highest_goods_of_all', $data);
    }
    public function bucket_list() {
        $user_id = $this->user->userId;
        $whereCondition = " WHERE (user_id = 0 OR user_id = '$user_id') AND isDeleted = 0 AND id NOT IN (SELECT bucketlist_id FROM user_bukcet_list WHERE user_id = $user_id) ORDER BY user_id ASC, sortOrder ASC";
        $data["add_new_items_list"] = $this->bucketlist_model->fetch_add_new_bucketlist_items($whereCondition);
        $whereCondition = array("bucket_list.isDeleted" => 0, "user_bukcet_list.user_id" => $user_id);
        $data["users_list"] = $this->userbucketlist_model->get_records_join_bucketlist($whereCondition);
        $this->public_view('bucket_list', $data);
        
    }

    public function positive_affirmation() {
        $data["header"] = $this->header_model->custom_query(" positiveaffirmation_mobile ");
        $data["dashboard"] = $this->positiveaffirmationdashboard_model->get_records();
        $this->public_view('positive_affirmation', $data);
    }

    public function abundance() {
        $user_id = $this->user->userId;
        $whereCondition = " WHERE (user_id = 0 OR user_id = '$user_id') AND isDeleted = 0 AND id NOT IN (SELECT abundance_id FROM user_abundance_list WHERE user_id = $user_id) ORDER BY user_id ASC, sortOrder ASC";
        $data["add_new_items_list"] = $this->abundance_model->fetch_add_new_abundance_items($whereCondition);
        $whereCondition = array("abundance_list.isDeleted" => 0, "user_abundance_list.user_id" => $user_id);
        $data["users_list"] = $this->userabundance_model->get_records_join_bucketlist($whereCondition);
        $this->public_view('abundance', $data);
    }

    public function health_and_beauty() {
        $user_id = $this->user->userId;
        $whereCondition = " WHERE (user_id = 0 OR user_id = '$user_id') AND isDeleted = 0 AND id NOT IN (SELECT healthandbeauty_id FROM user_healthandbeauty_list WHERE user_id = $user_id) ORDER BY user_id ASC, sortOrder ASC";
        $data["add_new_items_list"] = $this->healthandbeauty_model->fetch_add_new_abundance_items($whereCondition);
        $whereCondition = array("healthandbeauty.isDeleted" => 0, "user_healthandbeauty_list.user_id" => $user_id);
        $data["users_list"] = $this->userhealthandbeauty_model->get_records_join_bucketlist($whereCondition);
        $this->public_view('health_and_beauty', $data);
    }

    public function love() {
        $user_id = $this->user->userId;
        $whereCondition = " WHERE (user_id = 0 OR user_id = '$user_id') AND isDeleted = 0 AND id NOT IN (SELECT love_id FROM user_love_list WHERE user_id = $user_id) ORDER BY user_id ASC, sortOrder ASC";
        $data["add_new_items_list"] = $this->love_model->fetch_add_new_abundance_items($whereCondition);
        $whereCondition = array("love.isDeleted" => 0, "user_love_list.user_id" => $user_id);
        $data["users_list"] = $this->userlove_model->get_records_join_bucketlist($whereCondition);
        $this->public_view('love', $data);
    }

    public function my_personal_affirmation() {
        $user_id = $this->user->userId;
        $whereCondition = " WHERE (user_id = 0 OR user_id = '$user_id') AND isDeleted = 0 AND id NOT IN (SELECT personalaffirmation_id FROM user_personalaffirmation_list WHERE user_id = $user_id) ORDER BY user_id ASC, sortOrder ASC";
        $data["add_new_items_list"] = $this->personalaffirmation_model->fetch_add_new_abundance_items($whereCondition);
        $whereCondition = array("personalaffirmation.isDeleted" => 0, "user_personalaffirmation_list.user_id" => $user_id);
        $data["users_list"] = $this->userpersonalaffirmation_model->get_records_join_bucketlist($whereCondition);
        $this->public_view('my_personal_affirmation', $data);
    }

}
