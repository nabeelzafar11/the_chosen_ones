<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Contact extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['homeData'] = $this->home_data_model->get_record();

        $this->public_view('contact', $data);
    }
}
