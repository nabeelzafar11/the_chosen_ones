<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();
    }

    public function update_first_section()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('sectionText', 'Section Text', 'trim|required');
        $this->form_validation->set_rules('buttonText', 'Button Text', 'trim|required');
        $this->form_validation->set_rules('buttonLink', 'Button Link', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            // $image = html_escape($this->input->post('sectionImage'));
            // if (isset($_FILES['sectionImage']['name'])) {
            //     $config['upload_path'] = './assets/uploaded_media';
            //     $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
            //     $config['max_size'] = 5120;
            //     $image_name = time();
            //     $config['file_name'] = $image_name;
            //     $this->load->library('upload', $config);
            //     if (!$this->upload->do_upload('sectionImage')) {
            //         $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
            //     } else {
            //         $data = $this->upload->data();
            //         $image = $image_name . $data['file_ext'];
            //     }
            // }
            // if (!$image) {
            //     $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            // }
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'firstSectionText' => html_escape($this->input->post('sectionText')),
                'firstButtonText' => html_escape($this->input->post('buttonText')),
                'firstButtonLink' => html_escape($this->input->post('buttonLink')),
                //'firstSectionImage' => $image,
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_what_we_do()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('whatWeDO', 'What We Do', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'whatWeDO' => html_escape($this->input->post('whatWeDO')),
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function add_new_feature_service()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('featureTitle', 'Feature Title', 'trim|required');
        $this->form_validation->set_rules('featureDescription', 'Feature Description', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            // $image = "";
            // if (isset($_FILES['featureImage']['name'])) {
            //     $config['upload_path'] = './assets/uploaded_media';
            //     $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
            //     $config['max_size'] = 5120;
            //     $image_name = time();
            //     $config['file_name'] = $image_name;
            //     $this->load->library('upload', $config);
            //     if (!$this->upload->do_upload('featureImage')) {
            //         $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
            //     } else {
            //         $data = $this->upload->data();
            //         $image = $image_name . $data['file_ext'];
            //     }
            // } else {
            //     $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            // }

            $recordData = array(
                'featureTitle' => html_escape($this->input->post('featureTitle')),
                'featureText' => html_escape($this->input->post('featureDescription')),
                //  'featureImage' => $image,
            );
            if ($this->feature_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/home_what_we_do'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function add_new_home_service()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('serviceTitle', 'Service Title', 'trim|required');
        $this->form_validation->set_rules('serviceDescription', 'Service Description', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = "";
            if (isset($_FILES['serviceImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('serviceImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $recordData = array(
                'homeServiceTitle' => html_escape($this->input->post('serviceTitle')),
                'homeServiceText' => html_escape($this->input->post('serviceDescription')),
                'homeServiceImage' => $image,
            );
            if ($this->home_services_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/home_servies_listings'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function add_new_home_slider()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeSliderTitleOne', 'Title One', 'trim');
        $this->form_validation->set_rules('homeSliderTitleTwo', 'Title Two', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = "";
            if (isset($_FILES['homeSliderImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('homeSliderImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $recordData = array(
                'homeSliderTitleOne' => html_escape($this->input->post('homeSliderTitleOne')),
                'homeSliderTitleTwo' => html_escape($this->input->post('homeSliderTitleTwo')),
                'homeSliderImage' => $image,
            );
            if ($this->home_slider_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/home_slider_listings'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_home_feature()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('featureId', 'Feature Id', 'trim|required');
        $this->form_validation->set_rules('featureTitle', 'Feature Title', 'trim|required');
        $this->form_validation->set_rules('featureDescription', 'Feature Description', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            // $image = html_escape($this->input->post('featureImage'));
            // if (isset($_FILES['featureImage']['name'])) {
            //     $config['upload_path'] = './assets/uploaded_media';
            //     $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
            //     $config['max_size'] = 5120;
            //     $image_name = time();
            //     $config['file_name'] = $image_name;
            //     $this->load->library('upload', $config);
            //     if (!$this->upload->do_upload('featureImage')) {
            //         $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
            //     } else {
            //         $data = $this->upload->data();
            //         $image = $image_name . $data['file_ext'];
            //     }
            // }
            // if (!$image) {
            //     $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            // }
            $featureId = html_escape($this->input->post('featureId'));
            $whereCondition = array('featureId' => $featureId);
            $updateData = array(
                'featureTitle' => html_escape($this->input->post('featureTitle')),
                'featureText' => html_escape($this->input->post('featureDescription')),
                //'featureImage' => $image,
            );
            if ($this->feature_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function edit_home_service()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeServiceId', 'Home Slider Id', 'trim|required');
        $this->form_validation->set_rules('serviceTitle', 'Service Title', 'trim|required');
        $this->form_validation->set_rules('serviceDescription', 'Service Description', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('serviceImage'));
            if (isset($_FILES['serviceImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('serviceImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $homeServiceId = html_escape($this->input->post('homeServiceId'));
            $whereCondition = array('homeServiceId' => $homeServiceId);
            $updateData = array(
                'homeServiceTitle' => html_escape($this->input->post('serviceTitle')),
                'homeServiceText' => html_escape($this->input->post('serviceDescription')),
                'homeServiceImage' => $image,
            );
            if ($this->home_services_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_video_section()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('home_video_link', 'Video Link', 'trim|required');
        $this->form_validation->set_rules('home_video_title', 'Video Title', 'trim|required');
        $this->form_validation->set_rules('home_video_text', 'Video Text', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('home_video_play_icon'));
            if (isset($_FILES['home_video_play_icon']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('home_video_play_icon')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'home_video_link' => html_escape($this->input->post('home_video_link')),
                'home_video_title' => html_escape($this->input->post('home_video_title')),
                'home_video_text' => html_escape($this->input->post('home_video_text')),
                'home_video_play_icon' => $image,
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function edit_home_slider()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeSliderId', 'Home Slider Id', 'trim|required');
        $this->form_validation->set_rules('homeSliderTitleOne', 'Title One', 'trim');
        $this->form_validation->set_rules('homeSliderTitleTwo', 'Title Two', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('homeSliderImage'));
            if (isset($_FILES['homeSliderImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('homeSliderImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $homeSliderId = html_escape($this->input->post('homeSliderId'));
            $whereCondition = array('homeSliderId' => $homeSliderId);
            $updateData = array(
                'homeSliderTitleOne' => html_escape($this->input->post('homeSliderTitleOne')),
                'homeSliderTitleTwo' => html_escape($this->input->post('homeSliderTitleTwo')),
                'homeSliderImage' => $image,
            );
            if ($this->home_slider_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }


    public function update_home_slider_status()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Home Slider Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Home Slider Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeSliderId' => html_escape($this->input->post('id')));
            $updateData = array(
                'homeSliderIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->home_slider_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }
    }

    public function delete_home_slider()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Home Slider Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeSliderId' => html_escape($this->input->post('id')));
            $updateData = array(
                'homeSliderIsDeleted' => 1,
            );
            if ($this->home_slider_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function sort_home_slider()
    {
        $ids = $this->input->post('id');
        $updateData = array(
            'homeSliderSortOrder' => 0
        );
        $whereCondition = array('homeSliderId >' => 0);
        $this->home_slider_model->update_record($whereCondition, $updateData);
        foreach ($ids as $key => $id) {
            $updateData = array(
                'homeSliderSortOrder' => $key + 1
            );
            $whereCondition = array('homeSliderId' => $id);
            $this->home_slider_model->update_record($whereCondition, $updateData);
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

    public function update_home_blog_section()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeDataBlogBlogOne', 'Blog One', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataBlogBlogTwo', 'Blog Two', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataBlogBlogThree', 'Blog Three', 'trim|required|greater_than_equal_to[0]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'homeDataBlogBlogOne' => html_escape($this->input->post('homeDataBlogBlogOne')),
                'homeDataBlogBlogTwo' => html_escape($this->input->post('homeDataBlogBlogTwo')),
                'homeDataBlogBlogThree' => html_escape($this->input->post('homeDataBlogBlogThree')),
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_about_section()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeDataAboutTitle', 'Title', 'trim');
        $this->form_validation->set_rules('homeDataAboutSubTitle', 'Sub Title', 'trim');
        $this->form_validation->set_rules('homeDataAboutDescription', 'Description', 'trim');
        $this->form_validation->set_rules('homeDataAboutCategoryOne', 'Category One', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataAboutCategoryTwo', 'Category Two', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataAboutCategoryThree', 'Category Three', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataAboutCategoryFour', 'Category Four', 'trim|required|greater_than_equal_to[0]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'homeDataAboutTitle' => html_escape($this->input->post('homeDataAboutTitle')),
                'homeDataAboutSubTitle' => html_escape($this->input->post('homeDataAboutSubTitle')),
                'homeDataAboutDescription' => html_escape($this->input->post('homeDataAboutDescription')),
                'homeDataAboutCategoryOne' => html_escape($this->input->post('homeDataAboutCategoryOne')),
                'homeDataAboutCategoryTwo' => html_escape($this->input->post('homeDataAboutCategoryTwo')),
                'homeDataAboutCategoryThree' => html_escape($this->input->post('homeDataAboutCategoryThree')),
                'homeDataAboutCategoryFour' => html_escape($this->input->post('homeDataAboutCategoryFour')),
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_about_section_two()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeDataAboutTwoTitleOne', 'Title One', 'trim');
        $this->form_validation->set_rules('homeDataAboutTwoTitleTwo', 'Title Two', 'trim');
        $this->form_validation->set_rules('homeDataAboutTwoButtonText', 'Button Text', 'trim');
        $this->form_validation->set_rules('homeDataAboutTwoButtonCategoryId', 'Category', 'trim|required|greater_than_equal_to[0]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('homeDataAboutTwoImage'));
            if (isset($_FILES['homeDataAboutTwoImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('homeDataAboutTwoImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'homeDataAboutTwoTitleOne' => html_escape($this->input->post('homeDataAboutTwoTitleOne')),
                'homeDataAboutTwoTitleTwo' => html_escape($this->input->post('homeDataAboutTwoTitleTwo')),
                'homeDataAboutTwoButtonText' => html_escape($this->input->post('homeDataAboutTwoButtonText')),
                'homeDataAboutTwoButtonCategoryId' => html_escape($this->input->post('homeDataAboutTwoButtonCategoryId')),
                'homeDataAboutTwoImage' => $image,
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_media_section()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeDataMediaTitle', 'Title', 'trim');
        $this->form_validation->set_rules('homeDataMediaSubTitle', 'Sub Title', 'trim');
        $this->form_validation->set_rules('homeDataMediaDescription', 'Description', 'trim');
        $this->form_validation->set_rules('homeDataMediaButtonText', 'Button Text', 'trim');
        $this->form_validation->set_rules('homeDataMediaButtonCategoryId', 'Category', 'trim|required|greater_than_equal_to[0]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'homeDataMediaTitle' => html_escape($this->input->post('homeDataMediaTitle')),
                'homeDataMediaSubTitle' => html_escape($this->input->post('homeDataMediaSubTitle')),
                'homeDataMediaDescription' => html_escape($this->input->post('homeDataMediaDescription')),
                'homeDataMediaButtonText' => html_escape($this->input->post('homeDataMediaButtonText')),
                'homeDataMediaButtonCategoryId' => html_escape($this->input->post('homeDataMediaButtonCategoryId'))
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_interested_section()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeDataInterestedTitle', 'Title', 'trim');
        $this->form_validation->set_rules('homeDataInterestedCategoryOne', 'Category One', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataInterestedCategoryTwo', 'Category Two', 'trim|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('homeDataInterestedCategoryThree', 'Category Three', 'trim|required|greater_than_equal_to[0]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'homeDataInterestedTitle' => html_escape($this->input->post('homeDataInterestedTitle')),
                'homeDataInterestedCategoryOne' => html_escape($this->input->post('homeDataInterestedCategoryOne')),
                'homeDataInterestedCategoryTwo' => html_escape($this->input->post('homeDataInterestedCategoryTwo')),
                'homeDataInterestedCategoryThree' => html_escape($this->input->post('homeDataInterestedCategoryThree'))
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_testimonial_section()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('homeDataTestimonialTitle', 'Title', 'trim');
        $this->form_validation->set_rules('homeDataTestimonialSubTitle', 'Title', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('homeDataTestimonialImage'));
            if (isset($_FILES['homeDataTestimonialImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('homeDataTestimonialImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'homeDataTestimonialTitle' => html_escape($this->input->post('homeDataTestimonialTitle')),
                'homeDataTestimonialSubTitle' => html_escape($this->input->post('homeDataTestimonialSubTitle')),
                'homeDataTestimonialImage' => $image,
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_third_section()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('home_fourth_section_title', 'Title', 'trim|required');
        $this->form_validation->set_rules('home_fourth_section_text', 'Description', 'trim|required');
        $this->form_validation->set_rules('home_fourth_section_btnText', 'Button Text', 'trim|required');
        $this->form_validation->set_rules('home_fourth_section_href', 'Href', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('home_fourth_section_image_main'));
            if (isset($_FILES['home_fourth_section_image_main']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('home_fourth_section_image_main')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $this->upload = null;
            $image1 = html_escape($this->input->post('home_fourth_section_image'));
            if (isset($_FILES['home_fourth_section_image']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('home_fourth_section_image')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image1 = $image_name . $data['file_ext'];
                }
            }
            if (!$image1) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'home_fourth_section_title' => html_escape($this->input->post('home_fourth_section_title')),
                'home_fourth_section_text' => html_escape($this->input->post('home_fourth_section_text')),
                'home_fourth_section_btnText' => html_escape($this->input->post('home_fourth_section_btnText')),
                'home_fourth_section_href' => html_escape($this->input->post('home_fourth_section_href')),
                'home_fourth_section_image_main' => $image,
                'home_fourth_section_image' => $image1,
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_contact_section()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('home_contact_main_text', 'Title', 'trim|required');
        $this->form_validation->set_rules('home_contact_small_text', 'Description', 'trim|required');
        $this->form_validation->set_rules('home_contact_btnText', 'Button Text', 'trim|required');
        $this->form_validation->set_rules('home_contact_btnLink', 'Href', 'trim|required');
        $this->form_validation->set_rules('home_contact_subscribeNewsletterText', 'Title', 'trim|required');
        $this->form_validation->set_rules('home_contact_emailPlaceholder', 'Description', 'trim|required');
        $this->form_validation->set_rules('home_contact_btnSubcribeText', 'Button Text', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'home_contact_main_text' => html_escape($this->input->post('home_contact_main_text')),
                'home_contact_small_text' => html_escape($this->input->post('home_contact_small_text')),
                'home_contact_btnText' => html_escape($this->input->post('home_contact_btnText')),
                'home_contact_btnLink' => html_escape($this->input->post('home_contact_btnLink')),
                'home_contact_subscribeNewsletterText' => html_escape($this->input->post('home_contact_subscribeNewsletterText')),
                'home_contact_emailPlaceholder' => html_escape($this->input->post('home_contact_emailPlaceholder')),
                'home_contact_btnSubcribeText' => html_escape($this->input->post('home_contact_btnSubcribeText')),
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_home_facebook_section()
    {

        $image = html_escape($this->input->post('homepage_video'));
        if (isset($_FILES['homepage_video']['name'])) {
            $config['upload_path'] = './assets/uploaded_media';
            $config['allowed_types'] = 'mp4';
            $config['max_size'] = 5120;
            $image_name = time();
            $config['file_name'] = $image_name;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('homepage_video')) {
                $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
            } else {
                $data = $this->upload->data();
                $image = $image_name . $data['file_ext'];
            }
        }
        $whereCondition = array('homeDataId >' => 0);
        $updateData = array(
            'facebook_script' => $this->input->post('facebook_script'),
            'homepage_video' => $image,
        );
        if ($this->home_data_model->update_record($whereCondition, $updateData)) {
            $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_UPDATED);
        } else {
            $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
        }
    }

    public function add_new_testimonial()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('testimonialName', 'Name', 'trim|required');
        $this->form_validation->set_rules('testimonialOccupation', 'Occupation', 'trim|required');
        $this->form_validation->set_rules('testimonialComment', 'Comment', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = "";
            if (isset($_FILES['testimonialImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('testimonialImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $recordData = array(
                'testimonialName' => html_escape($this->input->post('testimonialName')),
                'testimonialOccupation' => html_escape($this->input->post('testimonialOccupation')),
                'testimonialComment' => html_escape($this->input->post('testimonialComment')),
                'testimonialImage' => $image,
            );
            if ($this->testimonial_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/home_testimonial_listings'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_testimonial()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('testimonialId', 'Testimonial Id', 'trim|required');
        $this->form_validation->set_rules('testimonialName', 'Name', 'trim|required');
        $this->form_validation->set_rules('testimonialOccupation', 'Occupation', 'trim|required');
        $this->form_validation->set_rules('testimonialComment', 'Comment', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('testimonialImage'));
            if (isset($_FILES['testimonialImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('testimonialImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $testimonialId = html_escape($this->input->post('testimonialId'));
            $whereCondition = array('testimonialId' => $testimonialId);
            $updateData = array(
                'testimonialName' => html_escape($this->input->post('testimonialName')),
                'testimonialOccupation' => html_escape($this->input->post('testimonialOccupation')),
                'testimonialComment' => html_escape($this->input->post('testimonialComment')),
                'testimonialImage' => $image,
            );
            if ($this->testimonial_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function delete_service()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Service Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeServiceId' => html_escape($this->input->post('id')));
            $updateData = array(
                'homeServiceIsDeleted' => 1,
            );
            if ($this->home_services_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function delete_feature()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'feature Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('featureId' => html_escape($this->input->post('id')));
            $updateData = array(
                'featureIsDeleted' => 1,
            );
            if ($this->feature_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function delete_testimonial()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Testimonial Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('testimonialId' => html_escape($this->input->post('id')));
            $updateData = array(
                'testimonialIsDeleted' => 1,
            );
            if ($this->testimonial_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function update_service_status()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Service Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Service Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeServiceId' => html_escape($this->input->post('id')));
            $updateData = array(
                'homeServiceIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->home_services_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }
    }

    public function update_feature_status()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Feature Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Feature Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('featureId' => html_escape($this->input->post('id')));
            $updateData = array(
                'featureIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->feature_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }
    }

    public function update_testimonial_status()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Testimonial Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Testimonial Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('testimonialId' => html_escape($this->input->post('id')));
            $updateData = array(
                'testimonialIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->testimonial_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }
    }

    public function sort_service()
    {
        $ids = $this->input->post('id');
        $updateData = array(
            'homeServiceSortOrder' => 0
        );
        $whereCondition = array('homeServiceId >' => 0);
        $this->home_services_model->update_record($whereCondition, $updateData);
        foreach ($ids as $key => $id) {
            $updateData = array(
                'homeServiceSortOrder' => $key + 1
            );
            $whereCondition = array('homeServiceId' => $id);
            $this->home_services_model->update_record($whereCondition, $updateData);
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

    public function sort_testimonials()
    {
        $ids = $this->input->post('id');
        $updateData = array(
            'testimonialSortOrder' => 0
        );
        $whereCondition = array('testimonialId >' => 0);
        $this->testimonial_model->update_record($whereCondition, $updateData);
        foreach ($ids as $key => $id) {
            $updateData = array(
                'testimonialSortOrder' => $key + 1
            );
            $whereCondition = array('testimonialId' => $id);
            $this->testimonial_model->update_record($whereCondition, $updateData);
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

    public function add_new_thirdsection()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('testimonialName', 'Name', 'trim|required');
        $this->form_validation->set_rules('testimonialComment', 'Comment', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = "";
            if (isset($_FILES['testimonialImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('testimonialImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $recordData = array(
                'testimonialName' => html_escape($this->input->post('testimonialName')),
                'testimonialComment' => html_escape($this->input->post('testimonialComment')),
                'testimonialOccupation' => html_escape($this->input->post('testimonialOccupation')),
                'testimonialImage' => $image,
            );
            if ($this->thirdsection_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/home_third_section_listings'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_thirdsection()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('testimonialId', 'Testimonial Id', 'trim|required');
        $this->form_validation->set_rules('testimonialName', 'Name', 'trim|required');
        $this->form_validation->set_rules('testimonialComment', 'Comment', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('testimonialImage'));
            if (isset($_FILES['testimonialImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('testimonialImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $testimonialId = html_escape($this->input->post('testimonialId'));
            $whereCondition = array('testimonialId' => $testimonialId);
            $updateData = array(
                'testimonialName' => html_escape($this->input->post('testimonialName')),
                'testimonialComment' => html_escape($this->input->post('testimonialComment')),
                'testimonialOccupation' => html_escape($this->input->post('testimonialOccupation')),
                'testimonialImage' => $image,
            );
            if ($this->thirdsection_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function delete_thirdsection()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Testimonial Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('testimonialId' => html_escape($this->input->post('id')));
            $updateData = array(
                'testimonialIsDeleted' => 1,
            );
            if ($this->thirdsection_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function update_thirdsection_status()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Testimonial Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Testimonial Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('testimonialId' => html_escape($this->input->post('id')));
            $updateData = array(
                'testimonialIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->thirdsection_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }
    }

    public function sort_thirdsection()
    {
        $ids = $this->input->post('id');
        $updateData = array(
            'testimonialSortOrder' => 0
        );
        $whereCondition = array('testimonialId >' => 0);
        $this->testimonial_model->update_record($whereCondition, $updateData);
        foreach ($ids as $key => $id) {
            $updateData = array(
                'testimonialSortOrder' => $key + 1
            );
            $whereCondition = array('testimonialId' => $id);
            $this->thirdsection_model->update_record($whereCondition, $updateData);
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }
}
