<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Footermenus extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function add_footer_menu() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('menuName', 'Menu Name', 'trim|required');
        $this->form_validation->set_rules('menuCategoryId', 'Menu Category Id', 'trim');
        $this->form_validation->set_rules('menuFooterHref', 'Menu Footer Href', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {


            $recordData = array(
                'footerMenuName' => html_escape($this->input->post('menuName')),
                'footerMenuHref' => !empty(trim($this->input->post('footerMenuHref'))) ? html_escape($this->input->post('footerMenuHref')) : "",
                'footerMenuCategoryId' => html_escape($this->input->post('menuCategoryId'))
            );
            if ($this->footermenu_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);

                $redirectionData = array(
                    'redirection' => 'admin/footer_menus'
                );

                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_footer_menu() {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('menuId', 'Menu Id', 'trim|required');
        $this->form_validation->set_rules('menuName', 'Menu Name', 'trim|required');
        $this->form_validation->set_rules('menuCategoryId', 'Menu Category Id', 'trim');
        $this->form_validation->set_rules('menuFooterHref', 'Menu Footer Href', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {

            $where = array('footerMenuId' => html_escape($this->input->post('menuId')));

            $recordData = array(
                'footerMenuName' => html_escape($this->input->post('menuName')),
                'footerMenuHref' => !empty(trim($this->input->post('footerMenuHref'))) ? html_escape($this->input->post('footerMenuHref')) : "",
                'footerMenuCategoryId' => html_escape($this->input->post('menuCategoryId'))
            );

            if ($this->footermenu_model->update_record($where, $recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_UPDATED
                    )
                );
                $this->session->set_flashdata($notificationData);

                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function change_menu_state() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Menu Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Menu Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('footerMenuId' => html_escape($this->input->post('id')));
            $updateData = array(
                'footerMenuIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->footermenu_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }
    }

    public function delete_footer_menu() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Menu Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('footerMenuId' => html_escape($this->input->post('id')));
            $updateData = array(
                'footerMenuIsDeleted' => 1,
            );
            if ($this->footermenu_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function sort_footer_menu() {
        $ids = $this->input->post('id');
        if (count($ids)) {
            $whereCondition = array('footerMenuId' => $ids[0]);
            $category = $this->footermenu_model->get_record($whereCondition);
            $updateData = array(
                'footerMenuSortOrder' => 0
            );
            $this->footermenu_model->update_record($whereCondition, $updateData);
            foreach ($ids as $key => $id) {
                $updateData = array(
                    'footerMenuSortOrder' => $key + 1
                );
                $whereCondition = array('	footerMenuId' => $id);
                $this->footermenu_model->update_record($whereCondition, $updateData);
            }
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

}
