<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Settings extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();
    }

    public function update_header()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('donateBntText', 'Donate Button Text', 'trim');
        $this->form_validation->set_rules('siteTitle', 'Site Title', 'trim');
        $this->form_validation->set_rules('seoTitle', 'Seo Title', 'trim');
        $this->form_validation->set_rules('seoKeywords', 'Seo Keywords', 'trim');
        $this->form_validation->set_rules('seoDescription', 'Seo Description', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('headerLogo'));
            if (isset($_FILES['headerLogo']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('headerLogo')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            $image2 = html_escape($this->input->post('favicon'));
            if (isset($_FILES['favicon']['name'])) {
                $config1['upload_path'] = './assets/uploaded_media';
                $config1['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config1['max_size'] = 5120;
                $image_name = time();
                $config1['file_name'] = $image_name;
                $this->load->library('upload', $config1);
                if (!$this->upload->do_upload('favicon')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image2 = $image_name . $data['file_ext'];
                }
            }
            $image3 = html_escape($this->input->post('headerDarkLogo'));
            if (isset($_FILES['headerDarkLogo']['name'])) {
                $config2['upload_path'] = './assets/uploaded_media';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config2['max_size'] = 5120;
                $image_name = time();
                $config2['file_name'] = $image_name;
                $this->load->library('upload', $config2);
                if (!$this->upload->do_upload('headerDarkLogo')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image3 = $image_name . $data['file_ext'];
                }
            }
            $updateData = array(
                'menuButtonText' => html_escape($this->input->post('donateBntText')),
                'siteTitle' => html_escape($this->input->post('siteTitle')),
                'seoTitle' => html_escape($this->input->post('seoTitle')),
                'seoKeywords' => html_escape($this->input->post('seoKeywords')),
                'seoDescription' => html_escape($this->input->post('seoDescription')),
                'headerLogo' => $image,
                'favicon' => $image2,
                'headerDarkLogo' => $image3,
            );
            if ($this->header_model->update_record($updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_header_button()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('menuButtonText', 'Menu Button Text', 'trim');
        $this->form_validation->set_rules('menuButtonCategoryId', 'Menu Button Href', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $updateData = array(
                'menuButtonText' => html_escape($this->input->post('menuButtonText')),
                'menuButtonCategoryId' => html_escape($this->input->post('menuButtonCategoryId')),
            );
            if ($this->header_model->update_record($updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_footer()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('addressText', 'Address Text', 'trim');
        $this->form_validation->set_rules('address', 'Address', 'trim');
        $this->form_validation->set_rules('relatedLinksLeft', 'Related Links Left', 'trim');
        $this->form_validation->set_rules('relatedLinksRight', 'Related Links Right', 'trim');
        $this->form_validation->set_rules('subscribeNewsletterText', 'Subscribe Newsletter Text', 'trim');
        $this->form_validation->set_rules('emailPlaceholder', 'Email Placeholder', 'trim');
        $this->form_validation->set_rules('copyright', 'Copyright Text', 'trim');
        $this->form_validation->set_rules('facebook', 'Facebook Link', 'trim');
        $this->form_validation->set_rules('twitter', 'Twitter Link', 'trim');
        $this->form_validation->set_rules('google', 'Google Link', 'trim');
        $this->form_validation->set_rules('linkedin', 'LinkedIn Link', 'trim');
        $this->form_validation->set_rules('youtube', 'Youtube Link', 'trim');
        $this->form_validation->set_rules('skype', 'Skype Link', 'trim');
        $this->form_validation->set_rules('vimeo', 'Vimeo Link', 'trim');
        $this->form_validation->set_rules('instagram', 'Instagram Link', 'trim');
        $this->form_validation->set_rules('pinterest', 'Pinterest Link', 'trim');
        $this->form_validation->set_rules('rss', 'RSS Link', 'trim');
        $this->form_validation->set_rules('behance', 'Behance Link', 'trim');
        $this->form_validation->set_rules('github', 'Github Link', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('footerLogo'));
            if (isset($_FILES['footerLogo']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('footerLogo')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            $updateData = array(
                'addressText' => html_escape($this->input->post('addressText')),
                'address' => html_escape($this->input->post('address')),
                'relatedLinksLeft' => html_escape($this->input->post('relatedLinksLeft')),
                'relatedLinksRight' => html_escape($this->input->post('relatedLinksRight')),
                'subscribeNewsletterText' => html_escape($this->input->post('subscribeNewsletterText')),
                'emailPlaceholder' => html_escape($this->input->post('emailPlaceholder')),
                'copyright' => html_escape($this->input->post('copyright')),
                'facebook' => html_escape($this->input->post('facebook')),
                'twitter' => html_escape($this->input->post('twitter')),
                'google' => html_escape($this->input->post('google')),
                'linkedin' => html_escape($this->input->post('linkedin')),
                'youtube' => html_escape($this->input->post('youtube')),
                'skype' => html_escape($this->input->post('skype')),
                'instagram' => html_escape($this->input->post('instagram')),
                'pinterest' => html_escape($this->input->post('pinterest')),
                'rss' => html_escape($this->input->post('rss')),
                'behance' => html_escape($this->input->post('behance')),
                'github' => html_escape($this->input->post('github')),
                'footerLogo' => $image,
            );
            if ($this->footer_model->update_record($updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }
}
