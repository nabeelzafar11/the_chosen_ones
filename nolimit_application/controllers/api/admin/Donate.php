<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Donate extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();
    }

    public function update_donate_content()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('donateHeader', 'Donate Header', 'trim|required');
        $this->form_validation->set_rules('homeText', 'Home Text', 'trim|required');
        $this->form_validation->set_rules('donateTitle', 'Donate Title', 'trim|required');
        $this->form_validation->set_rules('donateDescription', 'Donate Description', 'trim|required');
        $this->form_validation->set_rules('donateNowBtn', 'Donate Now Button', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'donate_header' => html_escape($this->input->post('donateHeader')),
                'donate_homeText' => html_escape($this->input->post('homeText')),
                'donate_title' => html_escape($this->input->post('donateTitle')),
                'donate_description' => html_escape($this->input->post('donateDescription')),
                'donate_donateNow' => html_escape($this->input->post('donateNowBtn')),
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }
}
