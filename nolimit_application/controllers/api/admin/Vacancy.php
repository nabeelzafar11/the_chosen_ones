<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vacancy extends MY_Controller
{
    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function add_new_vacancy(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('vacancyDesignation', 'Vacancy Designation', 'trim|required');
        $this->form_validation->set_rules('vacancyExp', 'Vacancy Experience', 'trim|required');
        $this->form_validation->set_rules('vacancyQual', 'Vacancy Qualification', 'trim|required');
        $this->form_validation->set_rules('vacancyEmail', 'Vacancy Apply Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('vacancyArea', 'Vacancy Area', 'trim|required');
        $this->form_validation->set_rules('skill1', 'skill 1', 'trim');
        $this->form_validation->set_rules('skill2', 'skill 2', 'trim');
        $this->form_validation->set_rules('skill3', 'skill 3', 'trim');
        $this->form_validation->set_rules('skill4', 'skill 4', 'trim');
        $this->form_validation->set_rules('skill5', 'skill 5', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', validation_errors());
        } else {
            
            $recordData = array(
                'vacancyName' => html_escape($this->input->post('vacancyDesignation')),
                'vacancyExp' => html_escape($this->input->post('vacancyExp')),
                'vacancyQual' => html_escape($this->input->post('vacancyQual')),
                'vacancyApplyEmail' => html_escape($this->input->post('vacancyEmail')),
                'vacancyArea' => html_escape($this->input->post('vacancyArea')),
                'vacancySkill1' => html_escape($this->input->post('skill1')),
                'vacancySkill2' => html_escape($this->input->post('skill2')),
                'vacancySkill3' => html_escape($this->input->post('skill3')),
                'vacancySkill4' => html_escape($this->input->post('skill4')),
                'vacancySkill5' => html_escape($this->input->post('skill5')),
            );
           
            if ($this->vacancy_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => ''
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function update_vacancy_status() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Vacancy Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Vacancy Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('vacancyId' => html_escape($this->input->post('id')));
            $updateData = array(
                'vacancyIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->vacancy_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }
    }

    public function edit_vacancy() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('vacancyId', 'Vacancy Id', 'trim|required');
        $this->form_validation->set_rules('vacancyDesignation', 'Vacancy Designation', 'trim|required');
        $this->form_validation->set_rules('vacancyExp', 'Vacancy Experience', 'trim|required');
        $this->form_validation->set_rules('vacancyQual', 'Vacancy Qualification', 'trim|required');
        $this->form_validation->set_rules('vacancyEmail', 'Vacancy Apply Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('vacancyArea', 'Vacancy Area', 'trim|required');
        $this->form_validation->set_rules('skill1', 'skill 1', 'trim');
        $this->form_validation->set_rules('skill2', 'skill 2', 'trim');
        $this->form_validation->set_rules('skill3', 'skill 3', 'trim');
        $this->form_validation->set_rules('skill4', 'skill 4', 'trim');
        $this->form_validation->set_rules('skill5', 'skill 5', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            
            $recordData = array(
                'vacancyName' => html_escape($this->input->post('vacancyDesignation')),
                'vacancyExp' => html_escape($this->input->post('vacancyExp')),
                'vacancyQual' => html_escape($this->input->post('vacancyQual')),
                'vacancyApplyEmail' => html_escape($this->input->post('vacancyEmail')),
                'vacancyArea' => html_escape($this->input->post('vacancyArea')),
                'vacancySkill1' => html_escape($this->input->post('skill1')),
                'vacancySkill2' => html_escape($this->input->post('skill2')),
                'vacancySkill3' => html_escape($this->input->post('skill3')),
                'vacancySkill4' => html_escape($this->input->post('skill4')),
                'vacancySkill5' => html_escape($this->input->post('skill5')),
            );

            $whereCondition = array(
                'vacancyId' => html_escape($this->input->post('vacancyId'))
            );
           
            if ($this->vacancy_model->update_record($whereCondition ,$recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => ''
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function delete_vacancy() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Vacancy Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('vacancyId' => html_escape($this->input->post('id')));
            $updateData = array(
                'vacancyIsDeleted' => 1,
            );
            if ($this->vacancy_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }

    public function sort_vacancy()
    {
        $ids = $this->input->post('id');
        if(count($ids)){
            $whereCondition = array('vacancyId' => $ids[0]);
            $category = $this->vacancy_model->get_record($whereCondition);
            $updateData = array(
                'vacancyOrderId' => 0
            );
            //$whereCondition = array('categoryParentId' => $category->categoryParentId);
            $this->vacancy_model->update_record($whereCondition, $updateData);
            foreach ($ids as $key => $id) {
                $updateData = array(
                    'vacancyOrderId' => $key + 1
                );
                $whereCondition = array('vacancyId' => $id);
                $this->vacancy_model->update_record($whereCondition, $updateData);
            }
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

}