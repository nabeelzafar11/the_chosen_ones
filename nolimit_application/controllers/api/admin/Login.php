<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->not_login_check();
    }

    public function do_login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('login', '', 'danger', INVALID_REQUEST);
        } else {
            $email = html_escape($this->input->post('email'));
            $whereConditionArray = array('email' => $email, 'typeId' => 1);
            $resultUser = $this->user_model->get_record($whereConditionArray);
            if (!$resultUser) {
                $this->send_api_respone('login', '', 'danger', INVALID_USERNAME_OR_PASSWORD);
            } else {
                if (decode($resultUser->password) != $this->input->post('password'))
                    $this->send_api_respone('login', '', 'danger', INVALID_USERNAME_OR_PASSWORD);
                if ($resultUser->isActive == 0) {
                    $this->send_api_respone('login', '', 'danger', INACTIVE_ACCOUNT);
                }
                if ($resultUser->isDeleted == 1) {
                    $this->send_api_respone('login', '', 'danger', DELETED_ACCOUNT);
                }
                $this->load->library('user_agent');
                $recordData = array(
                    'loginUserId' => $resultUser->userId,
                    'loginSource' => LOGIN_SOURCE_FORM,
                    'loginDevice' => 'web',

                );
                $loginId = $this->login_model->insert_record($recordData);
                $newdata = array(
                    'loginId' => encode($loginId)
                );
                $this->session->set_userdata($newdata);
                $this->send_api_respone('login', '', 'refresh', 'Successfully login.');
            }
        }
    }

    public function forgot_password()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('login', '', 'danger', INVALID_REQUEST);
        } else {
            $email = html_escape($this->input->post('email'));
            $whereConditionArray = array('email' => $email);
            $resultUser = $this->user_model->get_record($whereConditionArray);
            if ($resultUser) {
                $passwordResetToken = encode(time());
                $whereConditionArray = array('email' => $email);
                $updateData = array(
                    'passwordResetToken' => $passwordResetToken
                );
                if ($this->user_model->update_record($whereConditionArray, $updateData) && $this->send_password_reset_email($email, $passwordResetToken)) {
                    $actionData = array(
                        'siteActionUserId' => $resultUser->userId,
                        'siteActionStudioId' => 0,
                        'actionId' => ACTION_FORGOT_PASSWORD
                    );
                    $this->action_controll($actionData);
                    $this->send_api_respone('login', '', 'success', SENT_RESET_PASSWORD_LINK);
                } else {
                    $this->send_api_respone('login', '', 'danger', RESET_PASSWORD_FAILED);
                }
            } else {
                $this->send_api_respone('login', '', 'danger', EMAIL_NOT_EXIST);
            }
        }
    }

    public function reset_password()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('passwordResetToken', 'Password Reset Token', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[100]');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('login', '', 'danger', INVALID_REQUEST);
        } else {
            $email = html_escape($this->input->post('email'));
            $passwordResetToken = html_escape($this->input->post('passwordResetToken'));
            $whereConditionArray = array('email' => $email, 'passwordResetToken' => $passwordResetToken);
            $resultUser = $this->user_model->get_record($whereConditionArray);
            if($resultUser) {
                $updateData = array(
                    'password' => encode($this->input->post('password')),
                    'passwordResetToken' => ''
                );
                if ($this->user_model->update_record($whereConditionArray, $updateData)) {
                    $newdata = array(
                        'login_data' => array(
                            'status' => 'success',
                            'message' => SUCCESS_RECOVER_PASSWORD
                        )
                    );
                    $this->session->set_userdata($newdata);
                    $actionData = array(
                        'siteActionUserId' => $resultUser->userId,
                        'siteActionStudioId' => 0,
                        'actionId' => ACTION_RESET_PASSWORD
                    );
                    $this->action_controll($actionData);
                    $this->send_api_respone('login', '', 'refresh', SUCCESS_RECOVER_PASSWORD);
                } else {
                    $this->send_api_respone('login', '', 'danger', FAILED_RECOVER_PASSWORD);
                }
            }else{
                $this->send_api_respone('login', '', 'refresh', SUCCESS_RECOVER_PASSWORD);
            }
        }
    }

    private function send_password_reset_email($email, $passwordResetToken)
    {
        $data = array(
            'email' => $email,
            'subject' => RESET_PASSWORD_LINK_EMAIL_SUBJECT,
            'passwordResetToken' => $passwordResetToken
        );
        return $this->send_email("reset_password", $data);
    }
}
