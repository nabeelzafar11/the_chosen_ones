<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Contact extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();
    }

    public function update_contact_content()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('pageTitle', 'Page Title', 'trim|required');
        $this->form_validation->set_rules('homeText', 'Home Text', 'trim|required');
        $this->form_validation->set_rules('address', 'address', 'trim|required');
        $this->form_validation->set_rules('phone1', 'phone1', 'trim|required');
        $this->form_validation->set_rules('phone2', 'phone2', 'trim|required');
        $this->form_validation->set_rules('mail1', 'mail1', 'trim|required');
        $this->form_validation->set_rules('mail2', 'mail2', 'trim|required');
        $this->form_validation->set_rules('mapLoc', 'Map', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'contact_pageTitle' => html_escape($this->input->post('pageTitle')),
                'contact_homeText' => html_escape($this->input->post('homeText')),
                'contact_address' => html_escape($this->input->post('address')),
                'contact_phone1' => html_escape($this->input->post('phone1')),
                'contact_phone2' => html_escape($this->input->post('phone2')),
                'contact_mail1' => html_escape($this->input->post('mail1')),
                'contact_mail2' => html_escape($this->input->post('mail2')),
                'contact_mapLoc' => html_escape($this->input->post('mapLoc')),
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_contact_form()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'address', 'trim|required');
        $this->form_validation->set_rules('name_l', 'phone1', 'trim|required');
        $this->form_validation->set_rules('name_p', 'phone2', 'trim|required');
        $this->form_validation->set_rules('phone_l', 'mail1', 'trim|required');
        $this->form_validation->set_rules('phone_p', 'mail2', 'trim|required');
        $this->form_validation->set_rules('email_l', 'address', 'trim|required');
        $this->form_validation->set_rules('email_p', 'phone1', 'trim|required');
        $this->form_validation->set_rules('message_l', 'phone2', 'trim|required');
        $this->form_validation->set_rules('message_p', 'mail1', 'trim|required');
        $this->form_validation->set_rules('submit_btn', 'mail2', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'contact_formTitle' => html_escape($this->input->post('title')),
                'contact_name_l' => html_escape($this->input->post('name_l')),
                'contact_name_p' => html_escape($this->input->post('name_p')),
                'contact_phone_l' => html_escape($this->input->post('phone_l')),
                'contact_phone_p' => html_escape($this->input->post('phone_p')),
                'contact_email_l' => html_escape($this->input->post('email_l')),
                'contact_email_p' => html_escape($this->input->post('email_p')),
                'contact_message_l' => html_escape($this->input->post('message_l')),
                'contact_message_p' => html_escape($this->input->post('message_p')),
                'contact_submitBtn' => html_escape($this->input->post('submit_btn')),
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }
}
