<?php

defined('BASEPATH') or exit('No direct script access allowed');

class About extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();
    }

    public function update_about_content()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('pageTitle', 'Page Title', 'trim|required');
        $this->form_validation->set_rules('homeText', 'Home Text', 'trim|required');
        $this->form_validation->set_rules('title', 'Section Text', 'trim|required');
        $this->form_validation->set_rules('description', 'Button Text', 'trim|required');
        $this->form_validation->set_rules('detailofText', 'Button Link', 'trim|required');
        $this->form_validation->set_rules('videoLink', 'Button Link', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('homeDataId >' => 0);
            $updateData = array(
                'aboutPageTitle' => html_escape($this->input->post('pageTitle')),
                'aboutHomeText' => html_escape($this->input->post('homeText')),
                'aboutTitle' => html_escape($this->input->post('title')),
                'aboutDescription' => html_escape($this->input->post('description')),
                'aboutDetailofText' => html_escape($this->input->post('detailofText')),
                'aboutVideoLink' => html_escape($this->input->post('videoLink')),
            );
            if ($this->home_data_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function add_new_counter_item()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('count', 'Count', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = "";
            if (isset($_FILES['itemImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('itemImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            } else {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }

            $recordData = array(
                'counterTitle' => html_escape($this->input->post('title')),
                'counterCount' => html_escape($this->input->post('count')),
                'counterOperator' => html_escape($this->input->post('operator')),
                'counterImage' => $image,
            );
            if ($this->about_counter_model->insert_record($recordData)) {
                $notificationData = array(
                    'notification' => array(
                        'status' => 'success',
                        'message' => SUCCESSFULLY_ADDED
                    )
                );
                $this->session->set_flashdata($notificationData);
                $redirectionData = array(
                    'redirection' => 'admin/about_counters'
                );
                $this->session->set_flashdata($redirectionData);
                $this->send_api_respone('', '', 'refresh', SUCCESSFULLY_ADDED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_ADDED);
            }
        }
    }

    public function edit_counter_item()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('counterId', 'Counter Id', 'trim|required');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('count', 'Count', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $image = html_escape($this->input->post('counterImage'));
            if (isset($_FILES['counterImage']['name'])) {
                $config['upload_path'] = './assets/uploaded_media';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
                $config['max_size'] = 5120;
                $image_name = time();
                $config['file_name'] = $image_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('counterImage')) {
                    $this->send_api_respone('', '', 'danger', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $image = $image_name . $data['file_ext'];
                }
            }
            if (!$image) {
                $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
            }
            $counterId = html_escape($this->input->post('counterId'));
            $whereCondition = array('counterId' => $counterId);
            $updateData = array(
                'counterTitle' => html_escape($this->input->post('title')),
                'counterCount' => html_escape($this->input->post('count')),
                'counterOperator' => html_escape($this->input->post('operator')),
                'counterImage' => $image,
            );
            if ($this->about_counter_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_UPDATED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_UPDATED);
            }
        }
    }

    public function update_counter_status()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Counter Id', 'trim|required');
        $this->form_validation->set_rules('isActive', 'Counter Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('counterId' => html_escape($this->input->post('id')));
            $updateData = array(
                'counterIsActive' => html_escape($this->input->post('isActive')),
            );
            if ($this->about_counter_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_CHANGED_STATUS);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_CHANGED_STATUS);
            }
        }
    }

    public function sort_counters()
    {
        $ids = $this->input->post('id');
        $updateData = array(
            'counterSortOrder' => 0
        );
        $whereCondition = array('counterId >' => 0);
        $this->about_counter_model->update_record($whereCondition, $updateData);
        foreach ($ids as $key => $id) {
            $updateData = array(
                'counterSortOrder' => $key + 1
            );
            $whereCondition = array('counterId' => $id);
            $this->about_counter_model->update_record($whereCondition, $updateData);
        }
        $this->send_api_respone('', '', 'success', "Successfully updated.");
    }

    public function delete_counter()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'Counter Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $whereCondition = array('counterId' => html_escape($this->input->post('id')));
            $updateData = array(
                'counterIsDeleted' => 1,
            );
            if ($this->about_counter_model->update_record($whereCondition, $updateData)) {
                $this->send_api_respone('', '', 'success', SUCCESSFULLY_DELETED);
            } else {
                $this->send_api_respone('', '', 'danger', FAILED_DELETED);
            }
        }
    }
}
