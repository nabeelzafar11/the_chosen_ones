<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Twilio\Rest\Client;

class Login extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->not_login_check();
    }

    public function do_login() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', "'email'", 'trim|required');
        $this->form_validation->set_rules('password', "'Password'", 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', validation_errors());
        } else {
            $email = html_escape($this->input->post('email'));
            $whereConditionArray = array('email' => $email);
            $resultUser = $this->user_model->get_record($whereConditionArray);
            if (!$resultUser) {
                $this->send_api_respone('', '', 'danger', "Invalid email or password");
            } else {

                if (decode($resultUser->password) != $this->input->post('password'))
                    $this->send_api_respone('', '', 'danger', "Invalid email or password");
                if ($resultUser->isActive == 0) {
                    $this->send_api_respone('', '', 'warning', "Please verify your email address through the verification code sent you at your email address. <a href='#!' onclick='resend_verification_email(\"" . encode($email) . "\");'> Click here</a> to resend verification email.", "", "Please verify your email address through the verification code sent you at your email address.");
                }
                if ($resultUser->isDeleted == 1) {
                    $this->send_api_respone('', '', 'danger', DELETED_ACCOUNT);
                }
                if ($this->mobile_api_check()) {
                    $recordData = array(
                        'loginUserId' => $resultUser->userId,
                        'loginDevice' => 'app',
                    );
                    $loginId = $this->login_model->insert_record($recordData);
                    $newdata = array(
                        'userToken' => encode($loginId),
                        'user' => $resultUser,
                    );
                    $this->send_api_respone('', '', 'refresh', $newdata);
                } else {
                    $recordData = array(
                        'loginUserId' => $resultUser->userId,
                        'loginDevice' => 'web',
                    );
                    $loginId = $this->login_model->insert_record($recordData);
                    $newdata = array(
                        'loginId' => encode($loginId)
                    );
                    $this->session->set_userdata($newdata);
                    $this->send_api_respone('', '', 'refresh', 'Successfully login.');
                }
            }
        }
    }

    public function verify_email($email, $activationToken) {
        $email = decode($email);
        $whereConditionArray = array('email' => $email);
        $resultUser = $this->user_model->get_record($whereConditionArray);
        if (!$resultUser) {
            redirect('', 'refresh');
        } else {
            if ($activationToken == $resultUser->activationToken && $resultUser->isActive == 0) {
                $updateData = array('isActive' => 1);
                $this->user_model->update_record($whereConditionArray, $updateData);
            }
            $recordData = array(
                'loginUserId' => $resultUser->userId,
                'loginDevice' => 'web',
            );
            $loginId = $this->login_model->insert_record($recordData);
            $newdata = array(
                'loginId' => encode($loginId)
            );
            $this->session->set_userdata($newdata);

            $notificationData = array(
                'notification' => array(
                    'status' => 'success',
                    'message' => "Your account has been activated!"
                )
            );
            $this->session->set_flashdata($notificationData);

            redirect('', 'refresh');
        }
    }

    public function resend_verification_email() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('token', 'Token', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', validation_errors());
        } else {
            $email = decode(html_escape($this->input->post('token')));
            $whereConditionArray = array('email' => $email);
            $resultUser = $this->user_model->get_record($whereConditionArray);
            if (!$resultUser) {
                $this->send_api_respone('', '', 'danger', "Invalid request.");
            } else {
                $message = base_url('verify-email/' . encode($email) . '/' . $resultUser->activationToken);
                $this->send_email2("active_email_account", array("email" => $email, "subject" => "Account Verification", "message" => $message));
                $this->send_api_respone('', '', 'success', 'Please verify your email address through the verification link sent to you at your email address.');
            }
        }
    }

    public function forgot_password() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('login', '', 'danger', INVALID_REQUEST);
        } else {
            $email = html_escape($this->input->post('email'));
            $whereConditionArray = array('email' => $email);
            $resultUser = $this->user_model->get_record($whereConditionArray);
            if ($resultUser) {
                $passwordResetToken = encode(time());
                $whereConditionArray = array('email' => $email);
                $updateData = array(
                    'resetToken' => $passwordResetToken
                );
                if ($this->user_model->update_record($whereConditionArray, $updateData) && $this->send_email2("reset_password", array("email" => $email, "resetToken" => $passwordResetToken, "subject" => "Reset Your Password"))) {
                    $this->send_api_respone('login', '', 'success', SENT_RESET_PASSWORD_LINK);
                } else {
                    $this->send_api_respone('login', '', 'danger', RESET_PASSWORD_FAILED);
                }
                $this->send_api_respone('login', '', 'success', SENT_RESET_PASSWORD_LINK);
            } else {
                $this->send_api_respone('login', '', 'danger', EMAIL_NOT_EXIST);
            }
        }
    }

}
