<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->not_login_check();
    }

    public function do_register() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('firstName', "First Name'", 'trim|required');
        $this->form_validation->set_rules('lastName', "Last Name", 'trim|required');
        $this->form_validation->set_rules('email', "Email Address", 'valid_email|trim|required');
        $this->form_validation->set_rules('password', "Password", 'trim|required');
        $this->form_validation->set_rules('confirmPassword', "Confirm Password", 'trim|required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', validation_errors());
        } else {
            $email = html_escape($this->input->post('email'));
            $whereConditionArray = array('email' => $email);
            $resultUser = $this->user_model->get_record($whereConditionArray);
            if ($resultUser) {
                $this->send_api_respone('', '', 'danger', "User with this email address already exists in our system.");
            }
            $firstName = html_escape($this->input->post('firstName'));
            $lastName = html_escape($this->input->post('lastName'));
            $password = html_escape($this->input->post('password'));
            $activationToken = encode(time());
            $recordData = array(
                'firstName' => $firstName,
                'lastName' => $lastName,
                'email' => $email,
                'activationToken' => $activationToken,
                'password' => encode($password)
            );
            if ($this->user_model->insert_record($recordData)) {
                $message = base_url('verify-email/' . encode($email) . '/' . $activationToken);
                $this->send_email2("active_email_account", array("email" => $email, "subject" => "Account Verification", "message" => $message));
                $this->send_api_respone('', '', 'success', 'Please verify your email address through the verification link sent to you at your email address.');
            } else {
                $this->send_api_respone('', '', 'danger', 'Failed to register.');
            }
        }
    }

    public function choose_new_password() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('resetLink', "Reset Link", 'trim|required');
        $this->form_validation->set_rules('email', "Email Address", 'valid_email|trim|required');
        $this->form_validation->set_rules('password', "Password", 'trim|required');
        $this->form_validation->set_rules('confirmPassword', "Confirm Password", 'trim|required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', validation_errors());
        } else {
             $recordData = array(
                'password' => encode(html_escape($this->input->post('password'))),
                'resetToken' => ""
            );
             $whereCondition = array(
                 'email' => html_escape($this->input->post('email')),
                 'resetToken' => html_escape($this->input->post('resetLink'))
             );
             if ($this->user_model->update_record($whereCondition, $recordData)) {
                  $this->send_api_respone('', '', 'success', 'Your password has been successfully updated! Please login using your new password.');
             }else{
                  $this->send_api_respone('', '', 'danger', 'Failed to update your password! Please try again.');
             }
        }
    }

}
