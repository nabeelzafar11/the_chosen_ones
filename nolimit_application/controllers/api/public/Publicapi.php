<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Publicapi extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function get_public_info() {
        $data["header"] = $this->header_model->custom_query(" gratitude_mobile,positiveaffirmation_mobile,highestgoodofall_mobile,login_mobile ");
        $curr_month = date("m");
        $data["calender_daily_affirmation"] = $this->calendardailyaffirmation_model->get_record(array("month" => $curr_month));
        $data["selflovecare_dashboard"] = $this->selflovecaredashboard_model->get_record(array("month" => $curr_month));
        $data["base_url"] = base_url();
        $data["media_path"] = MEDIA_PATH;
        $this->send_api_respone('', '', 'success', $data);
    }

    public function push_notification() {
        define('API_ACCESS_KEY', 'AIzaSyC8krlfaV8b4FAqadyi-KB-kw5MKBMKKmA');


        $registrationIds = array("dMnMuDyLK60:APA91bE7TH12qfn5qqvK8Sco-Fqr9vkrvpmbZ1UD3XgxGOZwsNmn_lyr_qVZNZs4UB1rNLjYy09ihi_IKzf3BttpjhtRb4IOhNKR8J9RjEnlNWaEDFtaX4MA1Eyt_OZjVOcvwVOLEAq7dMnMuDyLK60:APA91bE7TH12qfn5qqvK8Sco-Fqr9vkrvpmbZ1UD3XgxGOZwsNmn_lyr_qVZNZs4UB1rNLjYy09ihi_IKzf3BttpjhtRb4IOhNKR8J9RjEnlNWaEDFtaX4MA1Eyt_OZjVOcvwVOLEAq7");
        $msg = array
            (
            'body' => 'NASIR IS GOOD BOY!!',
            'title' => "ZEIKH"
        );

        $fields = array
            (
            'to' => "dMnMuDyLK60:APA91bE7TH12qfn5qqvK8Sco-Fqr9vkrvpmbZ1UD3XgxGOZwsNmn_lyr_qVZNZs4UB1rNLjYy09ihi_IKzf3BttpjhtRb4IOhNKR8J9RjEnlNWaEDFtaX4MA1Eyt_OZjVOcvwVOLEAq7",
            'notification' => $msg
        );

        $headers = array
            (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        echo $result;
    }

}
