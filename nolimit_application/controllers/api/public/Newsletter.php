<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Newsletter extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add_new_newsletter()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('subscriberEmail', 'Email', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $subscriberEmail = html_escape($this->input->post('subscriberEmail'));
            $whereCondition = array('subscriberEmail' => $subscriberEmail);
            $subscriptionResult = $this->subscription_model->get_record($whereCondition);
            if (!$subscriptionResult) {
                $recordData = array(
                    'subscriberEmail' => $subscriberEmail,
                );
                $this->subscription_model->insert_record($recordData);
            }
            $this->send_api_respone('', '', 'success', "Thank you for subscribing to our newsletter.");
        }
    }

    public function contactus_form()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('con_name', 'Email', 'trim|required');
        $this->form_validation->set_rules('con_phone', 'Email', 'trim|required');
        $this->form_validation->set_rules('con_email', 'Email', 'trim|required');
        $this->form_validation->set_rules('con_message', 'Email', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->send_api_respone('', '', 'danger', INVALID_REQUEST);
        } else {
            $data["sendTo"] = ADMIN_EMAIL_ADDRESS;
            $data["name"] = html_escape($this->input->post('con_name'));
            $data["phone"] = html_escape($this->input->post('con_phone'));
            $data["email"] = html_escape($this->input->post('con_email'));
            $data["message"] = html_escape($this->input->post('con_message'));
            $data["subject"] = "Query through Contact Us Form";
            if ($this->send_email2('contact_us', $data)) {
                $this->send_api_respone('', '', 'success', "Thank you for contacting us.");
            } else {
                $this->send_api_respone('', '', 'danger', "Failed to send email.");
            }
        }
    }
}
