<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Category extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function show_category($categorySlug1 = '', $categorySlug2 = '', $categorySlug3 = '', $categorySlug4 = '', $categorySlug5 = '', $categorySlug6 = '')
    {
        $data['currentCategory'] = '';
        $data['categoryArray'] = array();
        if ($categorySlug1) {
            $whereCondition = array('categorySlug' => $categorySlug1, 'categoryIsActive' => 1);
            $data['currentCategory'] = $this->category_model->get_record($whereCondition);
            if ($data['currentCategory']) {
                array_push($data['categoryArray'], $data['currentCategory']->categoryId);
            }
        }
        if ($data['currentCategory'] && $categorySlug2) {
            $whereCondition = array('categorySlug' => $categorySlug2, 'categoryIsActive' => 1, 'categoryParentId' => $data['currentCategory']->categoryId);
            $data['currentCategory'] = $this->category_model->get_record($whereCondition);
            if ($data['currentCategory']) {
                array_push($data['categoryArray'], $data['currentCategory']->categoryId);
            }
        }
        if ($data['currentCategory'] && $categorySlug3) {
            $whereCondition = array('categorySlug' => $categorySlug3, 'categoryIsActive' => 1, 'categoryParentId' => $data['currentCategory']->categoryId);
            $data['currentCategory'] = $this->category_model->get_record($whereCondition);
            if ($data['currentCategory']) {
                array_push($data['categoryArray'], $data['currentCategory']->categoryId);
            }
        }
        if ($data['currentCategory'] && $categorySlug4) {
            $whereCondition = array('categorySlug' => $categorySlug4, 'categoryIsActive' => 1, 'categoryParentId' => $data['currentCategory']->categoryId);
            $data['currentCategory'] = $this->category_model->get_record($whereCondition);
            if ($data['currentCategory']) {
                array_push($data['categoryArray'], $data['currentCategory']->categoryId);
            }
        }
        if ($data['currentCategory'] && $categorySlug5) {
            $whereCondition = array('categorySlug' => $categorySlug5, 'categoryIsActive' => 1, 'categoryParentId' => $data['currentCategory']->categoryId);
            $data['currentCategory'] = $this->category_model->get_record($whereCondition);
            if ($data['currentCategory']) {
                array_push($data['categoryArray'], $data['currentCategory']->categoryId);
            }
        }
        if ($data['currentCategory'] && $categorySlug6) {
            $whereCondition = array('categorySlug' => $categorySlug6, 'categoryIsActive' => 1, 'categoryParentId' => $data['currentCategory']->categoryId);
            $data['currentCategory'] = $this->category_model->get_record($whereCondition);
            if ($data['currentCategory']) {
                array_push($data['categoryArray'], $data['currentCategory']->categoryId);
            }
        }

        if ($data['currentCategory']) {
            $data['interestedCategoryOne'] = $data['currentCategory']->categoryInterestedOneCategoryId;
            if ($data['interestedCategoryOne']) {
                $whereCondition = array('categoryId' => $data['interestedCategoryOne'], 'categoryIsActive' => 1);
                $data['interestedCategoryOne'] = $this->category_model->get_record($whereCondition);
                if ($data['interestedCategoryOne']) {
                    $data['interestedCategoryOne']->categorySlug = $this->get_category_link($data['interestedCategoryOne']->categoryId);
                }
            }
            $data['interestedCategoryTwo'] = $data['currentCategory']->categoryInterestedTwoCategoryId;
            if ($data['interestedCategoryTwo']) {
                $whereCondition = array('categoryId' => $data['interestedCategoryTwo'], 'categoryIsActive' => 1);
                $data['interestedCategoryTwo'] = $this->category_model->get_record($whereCondition);
                if ($data['interestedCategoryTwo']) {
                    $data['interestedCategoryTwo']->categorySlug = $this->get_category_link($data['interestedCategoryTwo']->categoryId);
                }
            }
            $data['interestedCategoryThree'] = $data['currentCategory']->categoryInterestedThreeCategoryId;
            if ($data['interestedCategoryThree']) {
                $whereCondition = array('categoryId' => $data['interestedCategoryThree'], 'categoryIsActive' => 1);
                $data['interestedCategoryThree'] = $this->category_model->get_record($whereCondition);
                if ($data['interestedCategoryThree']) {
                    $data['interestedCategoryThree']->categorySlug = $this->get_category_link($data['interestedCategoryThree']->categoryId);
                }
            }

            if ($data['currentCategory']->categoryPageType == 3) {
                $topCategory = array_shift($data['categoryArray']);
                $whereCondition = array('categoryParentId' => $topCategory, 'categoryIsActive' => 1);
                $data['sideCategories'] = $this->category_model->get_records($whereCondition);
                foreach ($data['sideCategories'] as $key => $value) {
                    $whereCondition = array('categoryParentId' => $value->categoryId, 'categoryIsActive' => 1);
                    $data['sideCategories'][$key]->sideCategories = $this->category_model->get_records($whereCondition);
                    $data['sideCategories'][$key]->categorySlug = $this->get_category_link($value->categoryId);
                    foreach ($value->sideCategories as $key1 => $value1) {
                        $whereCondition = array('categoryParentId' => $value1->categoryId, 'categoryIsActive' => 1);
                        $data['sideCategories'][$key]->sideCategories[$key1]->sideCategories = $this->category_model->get_records($whereCondition);
                        $data['sideCategories'][$key]->sideCategories[$key1]->categorySlug = $this->get_category_link($value1->categoryId);
                        foreach ($value1->sideCategories as $key2 => $value2) {
                            $whereCondition = array('categoryParentId' => $value2->categoryId, 'categoryIsActive' => 1);
                            $data['sideCategories'][$key]->sideCategories[$key1]->sideCategories[$key2]->sideCategories = $this->category_model->get_records($whereCondition);
                            $data['sideCategories'][$key]->sideCategories[$key1]->sideCategories[$key2]->categorySlug = $this->get_category_link($value2->categoryId);
                            foreach ($value2->sideCategories as $key3 => $value3) {
                                $data['sideCategories'][$key]->sideCategories[$key1]->sideCategories[$key2]->sideCategories[$key3]->categorySlug = $this->get_category_link($value3->categoryId);
                            }
                        }
                    }
                }
                $whereCondition = array('categoryParentId' => $data['currentCategory']->categoryId, 'categoryIsActive' => 1);
                $data['categories'] = $this->category_model->get_records($whereCondition);
                $this->public_view('category_content', $data);
            }
        } else {
            //            redirect('', 'refresh');
        }
    }
}
