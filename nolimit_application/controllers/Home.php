<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['homeData'] = $this->home_data_model->get_record();

        $servicesWhereCondition = array('homeServiceIsActive' => 1);
        $data['services'] = $this->home_services_model->get_records($servicesWhereCondition);

        $featuresWhereCondition = array('featureIsActive' => 1);
        $data['features'] = $this->feature_model->get_records($featuresWhereCondition);

        $this->public_view('home', $data);
    }

    public function logout()
    {
        if ($this->user) {
            $this->session->unset_userdata('loginId');
        }
        redirect('', 'refresh');
    }
}
