<?php

defined('BASEPATH') or exit('No direct script access allowed');

class About extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_about_content()
    {
        $data['page'] = ABOUT_CONTENT;

        $data['homeData'] = $this->home_data_model->get_record();
        $this->admin_view('about/show_about', $data);
    }

    public function show_about_counters()
    {
        $data['page'] = ABOUT_COUNTERS;
        $data['addCounterItemButton'] = true;

        $data['counters'] = $this->about_counter_model->get_records();
        $this->admin_view('about/about_counters', $data);
    }

    public function show_add_new_counter_item()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = ABOUT_COUNTERS;
        $data['backCounterItemButton'] = true;


        $data['services'] = $this->home_services_model->get_records();
        $this->admin_view('about/add_new_counter_item', $data);
    }

    public function edit_counter_item($counterId)
    {
        $whereCondition = array('counterId' => $counterId);
        $data['counter'] = $this->about_counter_model->get_record($whereCondition);
        if ($data['counter']) {
            $data['page'] = ABOUT_COUNTERS;
            $data['backCounterItemButton'] = true;
            $this->admin_view('about/edit_counter_item', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/about_counters', 'refresh');
        }
    }
}
