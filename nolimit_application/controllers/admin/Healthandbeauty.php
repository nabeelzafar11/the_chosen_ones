<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Healthandbeauty extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_list() {
        $data['addHealthandBeautyItem'] = true;
        $data['page'] = HEALTHANDBEAUTY_LIST;
        $whereCondition = array("user_id" => 0, "isDeleted" => 0);
        $data['healthandbeauty'] = $this->healthandbeauty_model->get_records($whereCondition);
        $this->admin_view('healthandbeauty/show_list', $data);
    }

    public function add_new_item() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = HEALTHANDBEAUTY_ADD_ITEM;
        $data['backHealthandBeautyButton'] = true;
        $this->admin_view('healthandbeauty/add_new_item', $data);
    }

    public function edit_item($id) {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = HEALTHANDBEAUTY_EDIT_ITEM;
        $whereCondition = array('id' => $id);
        $data["healthandbeauty"] = $this->healthandbeauty_model->get_record($whereCondition);
        $data['backHealthandBeautyButton'] = true;
        $this->admin_view('healthandbeauty/edit_item', $data);
    }

}
