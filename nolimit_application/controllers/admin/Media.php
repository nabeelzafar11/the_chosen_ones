<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function show_image_media_listings()
    {
        $data['page'] = IMAGE_MEDIA_LISTINGS;
        $data['addImageMediaCategoryButton'] = true;
        $whereCondition = array('mediaType' => 1);
        $data['medias'] = $this->media_model->get_records($whereCondition);
        $this->admin_view('media/image_media_listing', $data);
    }

    public function show_new_image_media_form()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = IMAGE_MEDIA_LISTINGS;
        $data['backImageMediaCategoryButton'] = true;
        $this->admin_view('media/add_new_image_media', $data);
    }

    public function show_document_media_listings()
    {
        $data['page'] = DOCUMENT_MEDIA_LISTINGS;
        $data['addDocumentMediaCategoryButton'] = true;
        $whereCondition = array('mediaType' => 2);
        $data['medias'] = $this->media_model->get_records($whereCondition);
        $this->admin_view('media/document_media_listing', $data);
    }

    public function show_new_document_media_form()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = DOCUMENT_MEDIA_LISTINGS;
        $data['backDocumentMediaCategoryButton'] = true;
        $this->admin_view('media/add_new_document_media', $data);
    }

}
