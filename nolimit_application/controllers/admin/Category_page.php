<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_page extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function show_category_page_listings($pageId = 0)
    {
        $data['page'] = CATEGORY_PAGE_LISTINGS;
        $data['addCategoryPageButton'] = true;
        $whereCondition = array('categoryPageType' => 3,'categoryPageType' => 6);
        $data['categoryPages'] = $this->category_model->get_records();
        $this->admin_view('category_pages/category_page_listing', $data);
    }

    public function show_new_category_page_form()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = CATEGORY_PAGE_LISTINGS;
        $data['backCategoryPageButton'] = true;
        $data['categories'] = $this->category_model->get_records();
        $categoryPages = $this->category_page_model->get_records();
        foreach ($categoryPages as $categoryPage) {
            foreach ($data['categories'] as $key => $category) {
                if ($category->categoryId == $categoryPage->categoryPageCategoryId) {
                    unset($data['categories'][$key]);
                    break;
                }
            }
        }
        $this->admin_view('category_pages/add_new_category_page', $data);
    }

    public function show_edit_category_page_form($pageId)
    {
        $whereCondition = array('pageId' => $pageId);
        $data['page'] = $this->page_model->get_record($whereCondition);
        if ($data['page']) {
            $data['page'] = CATEGORY_PAGE_LISTINGS;
            $data['backCategoryPageButton'] = true;
            $whereCondition = array('pageId' != $data['page']->pageParentId);
            $data['pages'] = $this->page_model->get_records($whereCondition);
            $this->admin_view('category_pages/edit_page', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/page_listings', 'refresh');
        }
    }
}
