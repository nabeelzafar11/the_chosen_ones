<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Donate extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_donate_content()
    {
        $data['page'] = DONATE_CONTENT;

        $data['homeData'] = $this->home_data_model->get_record();
        $this->admin_view('donate', $data);
    }
}
