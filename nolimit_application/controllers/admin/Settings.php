<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();

    }

    public function show_header()
    {
        $data['page'] = HEADER;
        $this->admin_view('settings/header', $data);
    }

    public function show_footer()
    {
        $data['page'] = FOOTER;
        $this->admin_view('settings/footer', $data);
    }

}
