<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Contact extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_contact_content()
    {
        $data['page'] = CONTACT_CONTENT;

        $data['homeData'] = $this->home_data_model->get_record();
        $this->admin_view('contact/show_contact_content', $data);
    }

    public function show_contact_form()
    {
        $data['page'] = CONTACT_FORM;

        $data['homeData'] = $this->home_data_model->get_record();
        $this->admin_view('contact/contact_form', $data);
    }
}
