<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_dashboard() {
        $data['page'] = DASHBOARD;
        $this->admin_view('dashboard', $data);
    }

    public function show_app_dashoboard_listing() {
        $data['page'] = DASHBOARD_LISTINGS;
//        $data['addDashboardButton'] = true;
        $data['dashboard'] = $this->dashboard_model->get_records();
        $this->admin_view('dashboard/dashboard_listing', $data);
    }

    public function add_new_item() {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = DASHBOARD_ADD_ITEM;
        $data['backDashboardButton'] = true;
        $this->admin_view('dashboard/add_new_item', $data);
    }

    public function edit_dashboard_item($id) {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = DASHBOARD_EDIT_ITEM;
        $whereCondition = array('id' => $id);
        $data["dashboard"] = $this->dashboard_model->get_record($whereCondition);
        $data['backDashboardButton'] = true;
        $this->admin_view('dashboard/edit_dashboard_item', $data);
    }

}
