<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admin_login_check();
    }

    public function show_home_first_section()
    {
        $data['page'] = HOME_FIRST_SECTION;

        $data['categories'] = $this->category_model->get_records();
        $data['homeData'] = $this->home_data_model->get_record();
        $this->admin_view('home/home_first_section', $data);
    }

    public function show_home_servies_listings()
    {
        $data['page'] = HOME_SERVICES;
        $data['addHomeServiceButton'] = true;

        $data['services'] = $this->home_services_model->get_records();
        $this->admin_view('home/home_services', $data);
    }

    public function show_home_what_we_do()
    {
        $data['page'] = HOME_WHAT_WE_DO;
        $data['addFeaturesButton'] = true;

        $data['homeData'] = $this->home_data_model->get_record();
        $data['features'] = $this->feature_model->get_records();

        $this->admin_view('home/home_what_we_do', $data);
    }

    public function show_new_home_service_form()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = HOME_SERVICES;
        $data['backHomeServiceButton'] = true;
        $this->admin_view('home/add_new_home_service', $data);
    }

    public function show_edit_home_service_form($homeServiceId)
    {
        $whereCondition = array('homeServiceId' => $homeServiceId);
        $data['homeService'] = $this->home_services_model->get_record($whereCondition);
        if ($data['homeService']) {
            $data['page'] = HOME_SERVICES;
            $data['backHomeServiceButton'] = true;
            $this->admin_view('home/edit_home_service', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/home_servies_listings', 'refresh');
        }
    }

    public function show_new_feature_form()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }

        $data['page'] = HOME_WHAT_WE_DO;
        $data['backFeaturesButton'] = true;
        $this->admin_view('home/add_new_feature', $data);
    }

    public function show_edit_feature_form($featureId)
    {
        $whereCondition = array('featureId' => $featureId);
        $data['feature'] = $this->feature_model->get_record($whereCondition);
        if ($data['feature']) {
            $data['page'] = HOME_WHAT_WE_DO;
            $data['backFeaturesButton'] = true;
            $this->admin_view('home/edit_feature', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/home_what_we_do', 'refresh');
        }
    }

    public function show_home_contact_section()
    {
        $data['page'] = CONTACT_SECTION;

        $data['categories'] = $this->category_model->get_records();
        $data['homeData'] = $this->home_data_model->get_record();
        $this->admin_view('home/home_contact_section', $data);
    }

    public function show_home_video_section()
    {
        $data['page'] = VIDEO_SECTION;

        $data['homeData'] = $this->home_data_model->get_record();
        $this->admin_view('home/home_video_section', $data);
    }

    public function show_home_slider_listings()
    {
        $data['page'] = HOME_SLIDER;
        $data['addHomeSlideButton'] = true;

        $data['homeSliders'] = $this->home_slider_model->get_records();
        $this->admin_view('home/home_slider_listings', $data);
    }

    public function show_new_home_slider_form()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = HOME_SLIDER;
        $data['backHomeSliderButton'] = true;
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('home/add_new_home_slider', $data);
    }

    public function show_edit_home_slider_form($homeSliderId)
    {
        $whereCondition = array('homeSliderId' => $homeSliderId);
        $data['homeSlider'] = $this->home_slider_model->get_record($whereCondition);
        if ($data['homeSlider']) {
            $data['page'] = HOME_SLIDER;
            $data['backHomeSliderButton'] = true;
            $data['categories'] = $this->category_model->get_records();
            $this->admin_view('home/edit_home_slider', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/home_slider_listings', 'refresh');
        }
    }

    public function show_home_blog_section()
    {
        $data['page'] = HOME_BLOG_SECTION;
        $data['homeData'] = $this->home_data_model->get_record();
        $data['blogs'] = $this->blog_model->get_records();
        $this->admin_view('home/home_blog_section', $data);
    }

    public function show_home_about_section_one()
    {
        $data['page'] = HOME_ABOUT_SECTION;
        $data['homeData'] = $this->home_data_model->get_record();
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('home/home_about_section', $data);
    }

    public function show_home_about_section_two()
    {
        $data['page'] = HOME_ABOUT_SECTION_TWO;
        $data['homeData'] = $this->home_data_model->get_record();
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('home/home_about_section_two', $data);
    }

    public function show_home_media_section()
    {
        $data['page'] = HOME_MEDIA_SECTION;
        $data['homeData'] = $this->home_data_model->get_record();
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('home/home_media_section', $data);
    }

    public function show_home_interested_section()
    {
        $data['page'] = HOME_INTERESTED_SECTION;
        $data['homeData'] = $this->home_data_model->get_record();
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('home/home_interested_section', $data);
    }

    public function show_home_testimonial_section()
    {
        $data['page'] = HOME_TESTIMONIAL_SECTION;
        $data['homeData'] = $this->home_data_model->get_record();
        $data['categories'] = $this->category_model->get_records();
        $this->admin_view('home/home_testimonial_section', $data);
    }

    public function show_home_third_section()
    {
        $data['page'] = HOME_FOURTH_SECTION;
        $data['categories'] = $this->category_model->get_records();
        $data['homeData'] = $this->home_data_model->get_record();
        $this->admin_view('home/home_fourth_section', $data);
    }

    public function show_home_testimonial_listings()
    {
        $data['page'] = HOME_TESTIMONIAL;
        $data['addHomeTestmonialButton'] = true;

        $data['testimonials'] = $this->testimonial_model->get_records();
        $this->admin_view('home/home_testimonial_listings', $data);
    }

    public function show_add_new_testimonial_form()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = HOME_TESTIMONIAL;
        $data['backHomeTestmonialButton'] = true;
        $this->admin_view('home/add_new_testimonial', $data);
    }

    public function show_edit_testimonial_form($testimonialId)
    {
        $whereCondition = array('testimonialId' => $testimonialId);
        $data['testimonial'] = $this->testimonial_model->get_record($whereCondition);
        if ($data['testimonial']) {
            $data['page'] = HOME_TESTIMONIAL;
            $data['backHomeTestmonialButton'] = true;
            $this->admin_view('home/edit_testimonial', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/home_testimonial_listings', 'refresh');
        }
    }

    public function show_home_third_section_listings()
    {
        $data['page'] = HOME_TESTIMONIAL_THIRD_SECTION;
        $data['addHomethirdSectionlButton'] = true;

        $data['testimonials'] = $this->thirdsection_model->get_records();
        $this->admin_view('home/home_thirdsection_listings', $data);
    }

    public function show_add_new_third_section_form()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['page'] = HOME_TESTIMONIAL_THIRD_SECTION;
        $data['backHomethirdsectionButton'] = true;
        $this->admin_view('home/add_new_thirdsection', $data);
    }

    public function show_edit_third_section_form($testimonialId)
    {
        $whereCondition = array('testimonialId' => $testimonialId);
        $data['testimonial'] = $this->thirdsection_model->get_record($whereCondition);
        if ($data['testimonial']) {
            $data['page'] = HOME_TESTIMONIAL_THIRD_SECTION;
            $data['backHomethirdsectionButton'] = true;
            $this->admin_view('home/edit_thirdsection', $data);
        } else {
            $notificationData = array(
                'notification' => array(
                    'status' => 'danger',
                    'message' => INVALID_REQUEST
                )
            );
            $this->session->set_flashdata($notificationData);
            redirect('admin/home_thirdsection_listings', 'refresh');
        }
    }

    public function home_facebook_section()
    {
        if ($this->session->flashdata('redirection')) {
            $this->session->keep_flashdata('notification');
            redirect($this->session->flashdata('redirection'), 'refresh');
        }
        $data['homeData'] = $this->home_data_model->get_record();
        $data['page'] = HOME_TESTIMONIAL;
        $this->admin_view('home/home_facebook_section', $data);
    }
}
