<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Footer_model extends CI_Model
{
    private $table = "footer";

    public function insert_record($recordData)
    {
        $this->db->insert($this->table, $recordData);
    }

    public function get_record()
    {
        $query = $this->db->get($this->table);
        if (!$query->row()) {
            $headerData = array(
                'footerLogo' => '',
            );
            $this->insert_record($headerData);
            $query = $this->db->get($this->table);
        }
        return $query->row();
    }

    public function update_record($updateData)
    {
        $query = $this->db->update($this->table, $updateData);
        if ($query) {
            return true;
        } else
            return false;
    }
}