<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Footermenu_model extends CI_Model
{
    private $table = "footer_menus";


    public function insert_record($recordData) {
        $this->db->insert($this->table, $recordData);
        return $this->db->insert_id();
    }

    public function get_record($whereConditionArray = null) {
        if ($whereConditionArray)
            $this->db->where($whereConditionArray);
        $this->db->where('footerMenuIsDeleted' , '0');
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function get_records($whereConditionArray = null) {
        if ($whereConditionArray)
            $this->db->where($whereConditionArray);
        $this->db->where('footerMenuIsDeleted' , '0');
        $this->db->order_by('footerMenuSortOrder');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function get_records_with_join($whereConditionArray = null) {
        if ($whereConditionArray)
            $this->db->where($whereConditionArray);
        $this->db->where('footerMenuIsDeleted' , '0');
        $this->db->join('categories', 'menus.menuCategoryId = categories.categoryId');
        $this->db->order_by('footerMenuSortOrder');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function update_record($whereConditionArray, $updateData) {
        $this->db->where($whereConditionArray);
        $query = $this->db->update($this->table, $updateData);
        if ($query) {
            return true;
        } else
            return false;
    }
}