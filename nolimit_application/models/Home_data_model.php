<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home_data_model extends CI_Model
{
    private $table = "home_data";
    private $tableId = "homeDataId";


    public function insert_record($recordData)
    {
        $this->db->insert($this->table, $recordData);
        return $this->db->insert_id();
    }

    public function get_record($whereConditionArray = null)
    {
        if ($whereConditionArray)
            $this->db->where($whereConditionArray);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function get_records($whereConditionArray = null)
    {
        if ($whereConditionArray)
            $this->db->where($whereConditionArray);
        $this->db->where('homeSliderIsDeleted' , '0');
        $this->db->order_by('homeSliderSortOrder');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function update_record($whereConditionArray, $updateData)
    {
        $this->db->where($whereConditionArray);
        $query = $this->db->update($this->table, $updateData);
        if ($query) {

            return true;
        } else
            return false;
    }
}