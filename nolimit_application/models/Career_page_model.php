<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Career_page_model extends CI_Model {

    public $table = 'career_page';
    
    public function insert_record($recordData) {
        $this->db->insert($this->table, $recordData);
        return $this->db->insert_id();
    }

    public function get_record() {
        $query = $this->db->get($this->table);
        return $query->row();  
    }

    public function update_record($updateData) {

        $query = $this->db->update($this->table, $updateData);

        if ($query)
            return true;
        else
            return false;
    }
}