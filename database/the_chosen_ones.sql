-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 03, 2020 at 01:53 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `the_chosen_ones`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_counters`
--

CREATE TABLE `about_counters` (
  `counterId` int(11) NOT NULL,
  `counterTitle` text NOT NULL,
  `counterImage` text NOT NULL,
  `counterCount` varchar(255) NOT NULL,
  `counterOperator` varchar(50) NOT NULL,
  `counterSortOrder` int(11) NOT NULL DEFAULT '0',
  `counterIsActive` int(11) NOT NULL DEFAULT '1',
  `counterIsDeleted` int(11) NOT NULL DEFAULT '0',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_counters`
--

INSERT INTO `about_counters` (`counterId`, `counterTitle`, `counterImage`, `counterCount`, `counterOperator`, `counterSortOrder`, `counterIsActive`, `counterIsDeleted`, `createdAt`, `modifiedAt`) VALUES
(1, 'Trusted Client’s', '1585648076.png', '876', '+', 1, 1, 0, '2020-03-31 08:57:04', '2020-04-01 07:40:52'),
(2, 'Support Work', '1585648060.png', '85', '%', 2, 1, 0, '2020-03-31 08:58:23', '2020-04-01 07:40:23'),
(3, 'Cup Coffee', '1585648149.png', '9879', '', 3, 1, 0, '2020-03-31 09:49:09', '2020-04-01 07:41:51'),
(4, 'Client Satisfaction', '1585648185.png', '100', '%', 4, 1, 0, '2020-03-31 09:49:45', '2020-04-01 08:52:50');

-- --------------------------------------------------------

--
-- Table structure for table `career_page`
--

CREATE TABLE `career_page` (
  `careerPageId` int(11) NOT NULL,
  `careerPageJoinNowHeading` varchar(255) NOT NULL,
  `careerPageJoinNowText` text NOT NULL,
  `careerPageJoinNowBtnText` varchar(255) NOT NULL,
  `careerPageJoinNowImage` varchar(255) NOT NULL,
  `careerPageSupportHeading` varchar(255) NOT NULL,
  `careerPageSupportRole1` varchar(255) NOT NULL,
  `careerPageSupportRole2` varchar(255) NOT NULL,
  `careerPageSupportRole3` varchar(255) NOT NULL,
  `careerPageSupportRole4` varchar(255) NOT NULL,
  `careerPageSupportRole5` varchar(255) NOT NULL,
  `careerPageSupportRole6` varchar(255) NOT NULL,
  `careerPageCreatedUserId` int(11) NOT NULL,
  `careerPageWorkHeading` varchar(255) NOT NULL,
  `careerPageWorkText` text NOT NULL,
  `careerPageWorkExpHeading` varchar(255) NOT NULL,
  `careerPageWorkExpText` text NOT NULL,
  `careerPageWorkExpImage` varchar(255) NOT NULL,
  `careerPageResourcesSection` text NOT NULL,
  `careerPageLastModifiedUserId` int(11) NOT NULL,
  `careerPageCreaterAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `careerPageModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `career_page`
--

INSERT INTO `career_page` (`careerPageId`, `careerPageJoinNowHeading`, `careerPageJoinNowText`, `careerPageJoinNowBtnText`, `careerPageJoinNowImage`, `careerPageSupportHeading`, `careerPageSupportRole1`, `careerPageSupportRole2`, `careerPageSupportRole3`, `careerPageSupportRole4`, `careerPageSupportRole5`, `careerPageSupportRole6`, `careerPageCreatedUserId`, `careerPageWorkHeading`, `careerPageWorkText`, `careerPageWorkExpHeading`, `careerPageWorkExpText`, `careerPageWorkExpImage`, `careerPageResourcesSection`, `careerPageLastModifiedUserId`, `careerPageCreaterAt`, `careerPageModifiedAt`) VALUES
(1, 'Join our Team', 'A beakthrough can seem like the smallest thing, but it can make the world of difference to someone ont the autism spectrum or someone who has a disability. So find the role for you, and help make everyday more often.', 'Join Now', '1553096333.jpg', 'Support Worker Roles', 'It&#039;s not easy, and its not for everyone. But as a support worker, the rewards can be so much bigger than the challanges', 'Our fantastic induction programme and continuing learning and development opportunities help all of aou staff become experts in the field', 'You&#039;ll gain an in-depth knowledge of autism or disability and most importantly, uou&#039;ll learn through real experiences and crave your own career in social care.', 'There is a huge variety of roles on offer, form helping a student make the most of university life to working in the community, in one of our residential or respite homes', 'Many of those who join us have on previous knowlwdge of the NDIS, autism or support work', 'Take a look at what&#039;s on offer on the back of this leaflet', 0, 'Why work for us?', 'As part of our team, you&#039;ll be helping autistic or disabld people do things they never thought thet could, while gaining priceless experience and enjoying great benefits too. MAKING DREAMS COME TRUE. From our extensive learning and development programme to supportive working environment, as well as the usual benefits you would expect.\nMost of work flexibly. It fits in with  our family or other commitments. Please talk us about your preffered working pattern.', 'What can you expect when join us?', 'We provide a full one year induction pathway and training programme, regular one to ones, annual appraisals, and the support you need to develop a career in social care.\nWe&#039;ll also tailor you learning to make sure you have all the skills you need to support people with autism or those who have a disability. \nWhen you join our organization you will be allocated a peer buddy &amp; assigned a learning mentor.', '1553143993.jpg', '<div class=\"specialBox withIcon\" title=\"\">\n<h1>Resources</h1>\n\n<p>&nbsp; &nbsp;&nbsp;<a href=\"/nolimit/public/assets/uploaded_media/1553768153.pdf\" target=\"_blank\">Join us </a></p>\n\n<p><span></span></p>\n</div>', 0, '2019-03-20 14:57:17', '2019-03-20 14:57:17'),
(2, 'Join our Team', 'A beakthrough can seem like the smallest thing, but it can make the world of difference to someone ont the autism spectrum or someone who has a disability. So find the role for you, and help make everyday more often.', 'Join Now', '1553096333.jpg', 'Support Worker Roles', 'It&#039;s not easy, and its not for everyone. But as a support worker, the rewards can be so much bigger than the challanges', 'Our fantastic induction programme and continuing learning and development opportunities help all of aou staff become experts in the field', 'You&#039;ll gain an in-depth knowledge of autism or disability and most importantly, uou&#039;ll learn through real experiences and crave your own career in social care.', 'There is a huge variety of roles on offer, form helping a student make the most of university life to working in the community, in one of our residential or respite homes', 'Many of those who join us have on previous knowlwdge of the NDIS, autism or support work', 'Take a look at what&#039;s on offer on the back of this leaflet', 0, 'Why work for us?', 'As part of our team, you&#039;ll be helping autistic or disabld people do things they never thought thet could, while gaining priceless experience and enjoying great benefits too. MAKING DREAMS COME TRUE. From our extensive learning and development programme to supportive working environment, as well as the usual benefits you would expect.\nMost of work flexibly. It fits in with  our family or other commitments. Please talk us about your preffered working pattern.', 'What can you expect when join us?', 'We provide a full one year induction pathway and training programme, regular one to ones, annual appraisals, and the support you need to develop a career in social care.\nWe&#039;ll also tailor you learning to make sure you have all the skills you need to support people with autism or those who have a disability. \nWhen you join our organization you will be allocated a peer buddy &amp; assigned a learning mentor.', '1553143993.jpg', '<div class=\"specialBox withIcon\" title=\"\">\n<h1>Resources</h1>\n\n<p>&nbsp; &nbsp;&nbsp;<a href=\"/nolimit/public/assets/uploaded_media/1553768153.pdf\" target=\"_blank\">Join us </a></p>\n\n<p><span></span></p>\n</div>', 0, '2019-03-20 15:14:26', '2019-03-20 15:14:26'),
(3, 'Join our Team', 'A beakthrough can seem like the smallest thing, but it can make the world of difference to someone ont the autism spectrum or someone who has a disability. So find the role for you, and help make everyday more often.', 'Join Now', '1553096333.jpg', 'Support Worker Roles', 'It&#039;s not easy, and its not for everyone. But as a support worker, the rewards can be so much bigger than the challanges', 'Our fantastic induction programme and continuing learning and development opportunities help all of aou staff become experts in the field', 'You&#039;ll gain an in-depth knowledge of autism or disability and most importantly, uou&#039;ll learn through real experiences and crave your own career in social care.', 'There is a huge variety of roles on offer, form helping a student make the most of university life to working in the community, in one of our residential or respite homes', 'Many of those who join us have on previous knowlwdge of the NDIS, autism or support work', 'Take a look at what&#039;s on offer on the back of this leaflet', 0, 'Why work for us?', 'As part of our team, you&#039;ll be helping autistic or disabld people do things they never thought thet could, while gaining priceless experience and enjoying great benefits too. MAKING DREAMS COME TRUE. From our extensive learning and development programme to supportive working environment, as well as the usual benefits you would expect.\nMost of work flexibly. It fits in with  our family or other commitments. Please talk us about your preffered working pattern.', 'What can you expect when join us?', 'We provide a full one year induction pathway and training programme, regular one to ones, annual appraisals, and the support you need to develop a career in social care.\nWe&#039;ll also tailor you learning to make sure you have all the skills you need to support people with autism or those who have a disability. \nWhen you join our organization you will be allocated a peer buddy &amp; assigned a learning mentor.', '1553143993.jpg', '<div class=\"specialBox withIcon\" title=\"\">\n<h1>Resources</h1>\n\n<p>&nbsp; &nbsp;&nbsp;<a href=\"/nolimit/public/assets/uploaded_media/1553768153.pdf\" target=\"_blank\">Join us </a></p>\n\n<p><span></span></p>\n</div>', 0, '2019-03-20 15:14:39', '2019-03-20 15:14:39'),
(4, 'Join our Team', 'A beakthrough can seem like the smallest thing, but it can make the world of difference to someone ont the autism spectrum or someone who has a disability. So find the role for you, and help make everyday more often.', 'Join Now', '1553096333.jpg', 'Support Worker Roles', 'It&#039;s not easy, and its not for everyone. But as a support worker, the rewards can be so much bigger than the challanges', 'Our fantastic induction programme and continuing learning and development opportunities help all of aou staff become experts in the field', 'You&#039;ll gain an in-depth knowledge of autism or disability and most importantly, uou&#039;ll learn through real experiences and crave your own career in social care.', 'There is a huge variety of roles on offer, form helping a student make the most of university life to working in the community, in one of our residential or respite homes', 'Many of those who join us have on previous knowlwdge of the NDIS, autism or support work', 'Take a look at what&#039;s on offer on the back of this leaflet', 0, 'Why work for us?', 'As part of our team, you&#039;ll be helping autistic or disabld people do things they never thought thet could, while gaining priceless experience and enjoying great benefits too. MAKING DREAMS COME TRUE. From our extensive learning and development programme to supportive working environment, as well as the usual benefits you would expect.\nMost of work flexibly. It fits in with  our family or other commitments. Please talk us about your preffered working pattern.', 'What can you expect when join us?', 'We provide a full one year induction pathway and training programme, regular one to ones, annual appraisals, and the support you need to develop a career in social care.\nWe&#039;ll also tailor you learning to make sure you have all the skills you need to support people with autism or those who have a disability. \nWhen you join our organization you will be allocated a peer buddy &amp; assigned a learning mentor.', '1553143993.jpg', '<div class=\"specialBox withIcon\" title=\"\">\n<h1>Resources</h1>\n\n<p>&nbsp; &nbsp;&nbsp;<a href=\"/nolimit/public/assets/uploaded_media/1553768153.pdf\" target=\"_blank\">Join us </a></p>\n\n<p><span></span></p>\n</div>', 0, '2019-03-20 15:16:10', '2019-03-20 15:16:10'),
(5, 'Join our Team', 'A beakthrough can seem like the smallest thing, but it can make the world of difference to someone ont the autism spectrum or someone who has a disability. So find the role for you, and help make everyday more often.', 'Join Now', '1553096333.jpg', 'Support Worker Roles', 'It&#039;s not easy, and its not for everyone. But as a support worker, the rewards can be so much bigger than the challanges', 'Our fantastic induction programme and continuing learning and development opportunities help all of aou staff become experts in the field', 'You&#039;ll gain an in-depth knowledge of autism or disability and most importantly, uou&#039;ll learn through real experiences and crave your own career in social care.', 'There is a huge variety of roles on offer, form helping a student make the most of university life to working in the community, in one of our residential or respite homes', 'Many of those who join us have on previous knowlwdge of the NDIS, autism or support work', 'Take a look at what&#039;s on offer on the back of this leaflet', 0, 'Why work for us?', 'As part of our team, you&#039;ll be helping autistic or disabld people do things they never thought thet could, while gaining priceless experience and enjoying great benefits too. MAKING DREAMS COME TRUE. From our extensive learning and development programme to supportive working environment, as well as the usual benefits you would expect.\nMost of work flexibly. It fits in with  our family or other commitments. Please talk us about your preffered working pattern.', 'What can you expect when join us?', 'We provide a full one year induction pathway and training programme, regular one to ones, annual appraisals, and the support you need to develop a career in social care.\nWe&#039;ll also tailor you learning to make sure you have all the skills you need to support people with autism or those who have a disability. \nWhen you join our organization you will be allocated a peer buddy &amp; assigned a learning mentor.', '1553143993.jpg', '<div class=\"specialBox withIcon\" title=\"\">\n<h1>Resources</h1>\n\n<p>&nbsp; &nbsp;&nbsp;<a href=\"/nolimit/public/assets/uploaded_media/1553768153.pdf\" target=\"_blank\">Join us </a></p>\n\n<p><span></span></p>\n</div>', 0, '2019-03-20 15:16:52', '2019-03-20 15:16:52'),
(6, 'Join our Team', 'A beakthrough can seem like the smallest thing, but it can make the world of difference to someone ont the autism spectrum or someone who has a disability. So find the role for you, and help make everyday more often.', 'Join Now', '1553096333.jpg', 'Support Worker Roles', 'It&#039;s not easy, and its not for everyone. But as a support worker, the rewards can be so much bigger than the challanges', 'Our fantastic induction programme and continuing learning and development opportunities help all of aou staff become experts in the field', 'You&#039;ll gain an in-depth knowledge of autism or disability and most importantly, uou&#039;ll learn through real experiences and crave your own career in social care.', 'There is a huge variety of roles on offer, form helping a student make the most of university life to working in the community, in one of our residential or respite homes', 'Many of those who join us have on previous knowlwdge of the NDIS, autism or support work', 'Take a look at what&#039;s on offer on the back of this leaflet', 0, 'Why work for us?', 'As part of our team, you&#039;ll be helping autistic or disabld people do things they never thought thet could, while gaining priceless experience and enjoying great benefits too. MAKING DREAMS COME TRUE. From our extensive learning and development programme to supportive working environment, as well as the usual benefits you would expect.\nMost of work flexibly. It fits in with  our family or other commitments. Please talk us about your preffered working pattern.', 'What can you expect when join us?', 'We provide a full one year induction pathway and training programme, regular one to ones, annual appraisals, and the support you need to develop a career in social care.\nWe&#039;ll also tailor you learning to make sure you have all the skills you need to support people with autism or those who have a disability. \nWhen you join our organization you will be allocated a peer buddy &amp; assigned a learning mentor.', '1553143993.jpg', '<div class=\"specialBox withIcon\" title=\"\">\n<h1>Resources</h1>\n\n<p>&nbsp; &nbsp;&nbsp;<a href=\"/nolimit/public/assets/uploaded_media/1553768153.pdf\" target=\"_blank\">Join us </a></p>\n\n<p><span></span></p>\n</div>', 0, '2019-03-20 15:20:29', '2019-03-20 15:20:29');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categoryId` int(11) NOT NULL,
  `categoryName` varchar(255) NOT NULL,
  `categorySlug` varchar(255) NOT NULL,
  `categoryIcon` varchar(255) NOT NULL,
  `categoryImage` varchar(255) NOT NULL,
  `categoryShortDescription` text NOT NULL,
  `categoryDescription` text NOT NULL,
  `categoryParentId` int(11) NOT NULL,
  `categoryIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `categoryIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `categorySortOrder` int(11) NOT NULL DEFAULT '0',
  `categoryPageType` int(11) NOT NULL,
  `categoryPageContent` text NOT NULL,
  `categoryInterestedOneCategoryId` int(11) NOT NULL,
  `categoryInterestedTwoCategoryId` int(11) NOT NULL,
  `categoryInterestedThreeCategoryId` int(11) NOT NULL,
  `categoryPageTitle` text NOT NULL,
  `categorySeoTitle` text NOT NULL,
  `categorySeoKeywords` text NOT NULL,
  `categorySeoDescription` text NOT NULL,
  `categoryCreatedUserId` int(11) NOT NULL,
  `categoryModifiedUserId` int(11) NOT NULL,
  `categoryCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `categoryModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categoryId`, `categoryName`, `categorySlug`, `categoryIcon`, `categoryImage`, `categoryShortDescription`, `categoryDescription`, `categoryParentId`, `categoryIsActive`, `categoryIsDeleted`, `categorySortOrder`, `categoryPageType`, `categoryPageContent`, `categoryInterestedOneCategoryId`, `categoryInterestedTwoCategoryId`, `categoryInterestedThreeCategoryId`, `categoryPageTitle`, `categorySeoTitle`, `categorySeoKeywords`, `categorySeoDescription`, `categoryCreatedUserId`, `categoryModifiedUserId`, `categoryCreatedAt`, `categoryModifiedAt`) VALUES
(174, 'Services', 'services', '', '1583326401.png', '&lt;div class=&quot;col-lg-3 col-md-12&quot;&gt;\n&lt;div class=&quot;about-text&quot;&gt;\n&lt;h5&gt;History&lt;/h5&gt;\n\n&lt;h1&gt;how we&lt;span class=&quot;d-block&quot;&gt;&lt;/span&gt;Founded&lt;/h1&gt;\n&lt;/div&gt;\n&lt;/div&gt;\n', '&lt;p&gt;&lt;span style=&quot;font-size:12pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;Being a woman is not easy. The expectations that are placed on us vastly differ from expectations placed on men. There are impossible standards that women are expected to live up to, to have it all. As a result, we beat up on ourselves to do a good job and find ourselves going above and beyond.&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n\n&lt;p&gt;&lt;span style=&quot;font-size:12pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;I believe, as women we are multifaceted, and we can do anything we want. There are absolutely no limits on what we can achieve, but we need to support and empower not only each other, but also the next generation.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n\n&lt;p&gt;&lt;span style=&quot;font-size:12pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;I have realized while trying to do and be everything, we end up neglecting ourselves. That is the reason why I came up with this App. I have included elements that I use and practice in my life and truly find them to not only uplift me, but also create focus, joy, love, abundance and empowerment. I hope that you will also be uplifted and enjoy every aspect of this App and remember to accept and appreciate your uniqueness.&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:12pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n\n&lt;p&gt;&lt;span style=&quot;font-size:12pt&quot;&gt;&lt;span style=&quot;font-family:Calibri,sans-serif&quot;&gt;So make yourself a priority today and every day. Because you deserve it!&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;', 0, 1, 0, 0, 3, '', 0, 0, 0, 'Services', 'Services', 'Services', 'Being a woman is not easy. The expectations that are placed on us vastly differ from expectations placed on men. There are impossible standards that women are expected to live up to, to have it all. As a result, we beat up on ourselves to do a good job and find ourselves going above and beyond.', 0, 0, '2020-03-04 12:53:22', '2020-04-01 06:16:03'),
(177, 'Test PAGE', 'test-page', '', '', '&lt;p&gt;Test PAGE&lt;/p&gt;\n', '&lt;p&gt;Test PAGE&lt;/p&gt;', 0, 1, 1, 0, 3, '', 0, 0, 0, 'Test PAGE', 'Test PAGE', 'Test PAGE', 'Test PAGE', 0, 0, '2020-04-01 08:44:28', '2020-04-01 08:44:42');

-- --------------------------------------------------------

--
-- Table structure for table `category_pages`
--

CREATE TABLE `category_pages` (
  `categoryPageId` int(11) NOT NULL,
  `categoryPageCategoryId` int(11) NOT NULL,
  `categoryPageTitle` varchar(255) NOT NULL,
  `categoryPageContent` text NOT NULL,
  `categoryPageIsLeftNav` tinyint(4) NOT NULL DEFAULT '0',
  `categoryPageLeftNavType` tinyint(4) NOT NULL DEFAULT '1',
  `categoryPageLeftNavContent` text NOT NULL,
  `categoryPageIsRightNav` tinyint(4) NOT NULL DEFAULT '0',
  `categoryPageRightNavType` tinyint(4) NOT NULL DEFAULT '1',
  `categoryPageRightNavContent` text NOT NULL,
  `categoryPageType` int(11) NOT NULL,
  `categoryPagePageTitle` text NOT NULL,
  `categoryPageSeoTitle` text NOT NULL,
  `categoryPageSeoDescription` text NOT NULL,
  `categoryPageSeoKeywords` text NOT NULL,
  `categoryPageIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `categoryPageIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `categoryPageCreatedUserId` int(11) NOT NULL,
  `categoryPageModifiedUserId` int(11) NOT NULL,
  `categoryPageCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `categoryPageModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard`
--

CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `popupMessage` text,
  `popupMessageMobile` text NOT NULL,
  `seo_title` text NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dashboard`
--

INSERT INTO `dashboard` (`id`, `name`, `image`, `popupMessage`, `popupMessageMobile`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(1, 'Calendar 2020', '1579738428.png', '&lt;p&gt;&lt;strong&gt;The calendar &lt;/strong&gt;is set up to remind you to love, spoil and make yourself a priority, even if it&amp;rsquo;s for 5-10 minutes. You have the option to choose from a list of items from the Self Love/Self Care section to schedule &amp;ldquo;me&amp;rdquo; time. You can choose items for every day of the week or several times a week or a month, your choice. So, go ahead and love yourself.&lt;/p&gt;', 'The calendar is set up to remind you to love, spoil and make yourself a priority, even if it&rsquo;s for 5-10 minutes. You have the option to choose from a list of items from the Self Love/Self Care section to schedule &ldquo;me&rdquo; time. You can choose items for every day of the week or several times a week or a month, your choice. So, go ahead and love yourself.', 'Calendar 2020', 'calendar 2020, test', 'just some dummy description just some dummy description just some dummy description just some dummy description'),
(2, 'Self Love/Self Care List', '1579735983.png', '&lt;p&gt;A simple yet important list of items to help you make yourself a priority. Self Love/Self Care is important to all of us. Each month offers a list of items that we hope you choose from to spoil yourself.&lt;/p&gt;', 'A simple yet important list of items to help you make yourself a priority. Self Love/Self Care is important to all of us. Each month offers a list of items that we hope you choose from to spoil yourself.', 'test', 'test', 'test'),
(3, 'Positive Affirmations', '1579736077.png', '&lt;p&gt;Affirmations are positive statements that can counteract some of our negative thoughts and habits, resonating with the alpha brain waves and enabling us to achieve empowerment. Positive affirmations can make us feel better about ourselves and help us focus on our goals. Scientific research suggests that they also produce chemical changes in the brain and create tangible benefits for those who use them. Say it and Believe it.&lt;/p&gt;', 'Affirmations are positive statements that can counteract some of our negative thoughts and habits, resonating with the alpha brain waves and enabling us to achieve empowerment. Positive affirmations can make us feel better about ourselves and help us focus on our goals. Scientific research suggests that they also produce chemical changes in the brain and create tangible benefits for those who use them. Say it and Believe it.', 'test', 'test', 'test'),
(4, 'Gratitude Journal', '1579736270.png', '&lt;p&gt;The text needs to be uploaded here&lt;/p&gt;', 'The text needs to be uploaded here', 'test', 'test', 'test'),
(5, 'Bucket List', '1579736368.png', '&lt;p&gt;Have you been thinking about creating a bucket list for yourself? Would you consider sharing your bucket list with your family/friends? We&amp;rsquo;ve listed some popular items that people have in their Bucket List. You can also create your own.&lt;/p&gt;', 'Have you been thinking about creating a bucket list for yourself? Would you consider sharing your bucket list with your family/friends? We&rsquo;ve listed some popular items that people have in their Bucket List. You can also create your own.', 'test', 'test', 'test'),
(6, 'Highest Good of All', '1579736596.png', '&lt;p&gt;Client needs to update message from admin panel&lt;/p&gt;', 'Client needs to update message from admin panel', 'test', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

CREATE TABLE `directors` (
  `directorId` int(11) NOT NULL,
  `directorName` varchar(255) NOT NULL,
  `directorDescription` text NOT NULL,
  `directorImage` varchar(255) NOT NULL,
  `directorSortOrder` int(11) NOT NULL DEFAULT '0',
  `directorIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `directorIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `directorCreatedUserId` int(11) NOT NULL,
  `directorModifiedUserId` int(11) NOT NULL,
  `directorCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `directorModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `featureId` int(11) NOT NULL,
  `featureTitle` text NOT NULL,
  `featureText` text NOT NULL,
  `featureImage` text NOT NULL,
  `featureSortOrder` int(11) NOT NULL DEFAULT '0',
  `featureIsActive` int(11) NOT NULL DEFAULT '1',
  `featureIsDeleted` int(11) DEFAULT '0',
  `featureCreatedUserId` int(11) NOT NULL DEFAULT '0',
  `featureModifiedUserId` int(11) NOT NULL DEFAULT '0',
  `featureCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `featureModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`featureId`, `featureTitle`, `featureText`, `featureImage`, `featureSortOrder`, `featureIsActive`, `featureIsDeleted`, `featureCreatedUserId`, `featureModifiedUserId`, `featureCreatedAt`, `featureModified`) VALUES
(1, 'Spiritual Service', 'Lorem Ipsum Dummy Content Lorem Ipsum Dummy Content Lorem Ipsum Dummy Content', '1585389844.png', 0, 1, 0, 0, 0, '2020-03-28 08:50:18', '2020-03-28 16:37:52'),
(2, 'ftest', 'ftest11', '1585387724.png', 0, 1, 1, 0, 0, '2020-03-28 09:28:44', '2020-03-28 09:42:19'),
(3, 'Relief Service', 'Lorem Ipsum Dummy Content Lorem Ipsum Dummy Content Lorem Ipsum Dummy Content', '1585387755.png', 0, 1, 0, 0, 0, '2020-03-28 09:29:15', '2020-03-28 16:38:19'),
(4, 'Medical Service', 'Lorem Ipsum Dummy Content Lorem Ipsum Dummy Content Lorem Ipsum Dummy Content', '1585387947.png', 0, 1, 0, 0, 0, '2020-03-28 09:32:27', '2020-03-29 09:21:47'),
(5, 'Cultural Services', 'Lorem Ipsum Dummy Content Lorem Ipsum Dummy Content Lorem Ipsum Dummy Content', '', 0, 1, 0, 0, 0, '2020-03-28 17:19:40', '2020-03-29 10:48:52'),
(6, 'Temple Security', 'Lorem Ipsum Dummy Content Lorem Ipsum Dummy Content Lorem Ipsum Dummy Content', '', 0, 1, 0, 0, 0, '2020-03-29 10:49:16', '2020-04-01 07:38:10');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `fileId` int(11) NOT NULL,
  `fileName` varchar(255) NOT NULL,
  `fileUrl` varchar(255) NOT NULL,
  `fileSlug` varchar(255) NOT NULL,
  `fileIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `fileIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `fileCreatedUserId` int(11) NOT NULL,
  `fileModifiedUserId` int(11) NOT NULL,
  `fileCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fileModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE `footer` (
  `footerId` int(11) NOT NULL,
  `footerLogo` text NOT NULL,
  `addressText` text NOT NULL,
  `address` text NOT NULL,
  `relatedLinksLeft` text NOT NULL,
  `relatedLinksRight` text NOT NULL,
  `subscribeNewsletterText` text NOT NULL,
  `emailPlaceholder` text NOT NULL,
  `copyright` text NOT NULL,
  `facebook` text NOT NULL,
  `twitter` text NOT NULL,
  `google` text NOT NULL,
  `linkedin` text NOT NULL,
  `youtube` text NOT NULL,
  `skype` text NOT NULL,
  `vimeo` text NOT NULL,
  `instagram` text NOT NULL,
  `pinterest` text NOT NULL,
  `dribbble` text NOT NULL,
  `rss` text NOT NULL,
  `behance` text NOT NULL,
  `github` text NOT NULL,
  `modifiedUserId` int(11) NOT NULL,
  `modifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`footerId`, `footerLogo`, `addressText`, `address`, `relatedLinksLeft`, `relatedLinksRight`, `subscribeNewsletterText`, `emailPlaceholder`, `copyright`, `facebook`, `twitter`, `google`, `linkedin`, `youtube`, `skype`, `vimeo`, `instagram`, `pinterest`, `dribbble`, `rss`, `behance`, `github`, `modifiedUserId`, `modifiedAt`) VALUES
(1, '1585415059.png', 'Address', '431 Block B, California, USA', 'Related Links', 'Related Links', 'Subscribe to our Newsletter', 'Enter Email', 'All rights reserved © Stomv %YEAR%', '#', '#', '', '', '#', '', '', '#', '', '', '', '', '', 6, '2020-03-28 17:04:19');

-- --------------------------------------------------------

--
-- Table structure for table `footer_menus`
--

CREATE TABLE `footer_menus` (
  `footerMenuId` int(11) NOT NULL,
  `footerMenuName` varchar(255) NOT NULL,
  `footerMenuHref` text NOT NULL,
  `footerMenuCategoryId` int(11) NOT NULL,
  `footerMenuSortOrder` int(11) NOT NULL,
  `footerMenuIsDeleted` int(11) NOT NULL,
  `footerMenuIsActive` int(11) NOT NULL,
  `footerMenuCreatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `footerMenuCreatedUserId` int(11) NOT NULL,
  `footerMenuModifiedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `footerMenuModifiedUserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `galleryId` int(11) NOT NULL,
  `galleryImage` varchar(255) NOT NULL,
  `galleryTitle` varchar(255) NOT NULL,
  `gallerySlug` text NOT NULL,
  `gallerySortOrder` int(11) NOT NULL DEFAULT '0',
  `galleryIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `galleryIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `galleryCreatedUserId` int(11) NOT NULL,
  `galleryModifiedUserId` int(11) NOT NULL,
  `galleryCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `galleryModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gratitude_journal`
--

CREATE TABLE `gratitude_journal` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `h1_title` text NOT NULL,
  `h1_message` text NOT NULL,
  `h1_date` date NOT NULL,
  `h2_title` text NOT NULL,
  `h2_message` text NOT NULL,
  `h2_date` date NOT NULL,
  `h3_title` text NOT NULL,
  `h3_message` text NOT NULL,
  `h3_date` date NOT NULL,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gratitude_journal`
--

INSERT INTO `gratitude_journal` (`id`, `user_id`, `h1_title`, `h1_message`, `h1_date`, `h2_title`, `h2_message`, `h2_date`, `h3_title`, `h3_message`, `h3_date`, `updatedAt`) VALUES
(5, 8, 'Things I am greateful for today', 'Just some random dummy text for First Section message', '2018-02-27', 'Things I am grateful for about my body', 'Just some random dummy text for Second Section message Just some random dummy text for Second Section messageJust some random dummy text for Second Section messageJust some random dummy text for Second Section messageJust some random dummy text for Second Section messageJust some random dummy text for Second Section messageJust some random dummy text for Second Section messageJust some random dummy text for Second Section messageJust some random dummy text for Second Section message', '2018-01-27', 'Things I am grateful for about my family', 'Just some random dummy text for Third Section message for TESTING', '2017-01-28', '2020-02-12 04:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `header`
--

CREATE TABLE `header` (
  `headerId` int(11) NOT NULL,
  `headerLogo` text NOT NULL,
  `headerDarkLogo` text NOT NULL,
  `menuButtonText` text NOT NULL,
  `menuButtonCategoryId` int(11) NOT NULL,
  `favicon` text NOT NULL,
  `siteTitle` text NOT NULL,
  `seoTitle` text NOT NULL,
  `seoDescription` text NOT NULL,
  `seoKeywords` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `host` text NOT NULL,
  `port` text NOT NULL,
  `encryption` text NOT NULL,
  `gratitude_web` text NOT NULL,
  `gratitude_mobile` text NOT NULL,
  `positiveaffirmation_web` text NOT NULL,
  `positiveaffirmation_mobile` text NOT NULL,
  `highestgoodofall_web` text NOT NULL,
  `highestgoodofall_mobile` text NOT NULL,
  `login_web` text NOT NULL,
  `login_mobile` text NOT NULL,
  `modifiedUserId` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `header`
--

INSERT INTO `header` (`headerId`, `headerLogo`, `headerDarkLogo`, `menuButtonText`, `menuButtonCategoryId`, `favicon`, `siteTitle`, `seoTitle`, `seoDescription`, `seoKeywords`, `phone`, `email`, `password`, `host`, `port`, `encryption`, `gratitude_web`, `gratitude_mobile`, `positiveaffirmation_web`, `positiveaffirmation_mobile`, `highestgoodofall_web`, `highestgoodofall_mobile`, `login_web`, `login_mobile`, `modifiedUserId`, `createdAt`, `modifiedAt`) VALUES
(1, '1585415098.png', '1585420827.png', 'DONATE', 171, '1585420788.png', 'The Chosen Ones', 'The Chosen Ones', 'The Chosen Ones', 'The Chosen Ones', '', 'bookmystudio.fr@gmail.com', 's4323399', 'smtp.gmail.com', '587', 'tls', '', '', '', '', '', '', '', '', 6, '2018-10-16 19:15:22', '2020-04-01 07:47:01');

-- --------------------------------------------------------

--
-- Table structure for table `header_menus`
--

CREATE TABLE `header_menus` (
  `footerMenuId` int(11) NOT NULL,
  `footerMenuName` varchar(255) NOT NULL,
  `footerMenuHref` text NOT NULL,
  `footerMenuCategoryId` int(11) NOT NULL,
  `footerMenuSortOrder` int(11) NOT NULL,
  `footerMenuIsDeleted` int(11) NOT NULL,
  `footerMenuIsActive` int(11) NOT NULL,
  `footerMenuCreatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `footerMenuCreatedUserId` int(11) NOT NULL,
  `footerMenuModifiedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `footerMenuModifiedUserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `header_menus`
--

INSERT INTO `header_menus` (`footerMenuId`, `footerMenuName`, `footerMenuHref`, `footerMenuCategoryId`, `footerMenuSortOrder`, `footerMenuIsDeleted`, `footerMenuIsActive`, `footerMenuCreatedAt`, `footerMenuCreatedUserId`, `footerMenuModifiedAt`, `footerMenuModifiedUserId`) VALUES
(11, 'Home', 'home', 0, 1, 0, 1, '2020-03-04 16:49:10', 0, '2020-03-04 16:49:10', 0),
(12, 'About', 'about', 0, 3, 0, 1, '2020-03-04 16:49:27', 0, '2020-03-04 16:49:27', 0),
(13, 'Contact', 'contact', 0, 4, 0, 1, '2020-03-09 12:40:42', 0, '2020-03-09 12:40:42', 0),
(19, 'Services', '', 174, 2, 0, 1, '2020-04-01 11:16:53', 0, '2020-04-01 11:16:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_data`
--

CREATE TABLE `home_data` (
  `homeDataId` int(11) NOT NULL,
  `firstSectionImage` text NOT NULL,
  `firstSectionText` text NOT NULL,
  `firstButtonText` text NOT NULL,
  `firstButtonLink` text NOT NULL,
  `whatWeDO` text NOT NULL,
  `homeDataBlogBlogOne` int(11) NOT NULL,
  `homeDataBlogBlogTwo` int(11) NOT NULL,
  `homeDataBlogBlogThree` int(11) NOT NULL,
  `homeDataAboutTitle` text NOT NULL,
  `homeDataAboutSubTitle` text NOT NULL,
  `homeDataAboutDescription` text NOT NULL,
  `homeDataAboutCategoryOne` int(11) NOT NULL,
  `homeDataAboutCategoryTwo` int(11) NOT NULL,
  `homeDataAboutCategoryThree` int(11) NOT NULL,
  `homeDataAboutCategoryFour` int(11) NOT NULL,
  `homeDataAboutTwoImage` text NOT NULL,
  `homeDataAboutTwoTitleOne` text NOT NULL,
  `homeDataAboutTwoTitleTwo` text NOT NULL,
  `homeDataAboutTwoButtonText` text NOT NULL,
  `homeDataAboutTwoButtonCategoryId` int(11) NOT NULL,
  `homeDataMediaTitle` text NOT NULL,
  `homeDataMediaSubTitle` text NOT NULL,
  `homeDataMediaDescription` text NOT NULL,
  `homeDataMediaButtonText` text NOT NULL,
  `homeDataMediaButtonCategoryId` int(11) NOT NULL,
  `homeDataInterestedTitle` text NOT NULL,
  `homeDataInterestedCategoryOne` int(11) NOT NULL,
  `homeDataInterestedCategoryTwo` int(11) NOT NULL,
  `homeDataInterestedCategoryThree` int(11) NOT NULL,
  `homeDataTestimonialImage` text NOT NULL,
  `homeDataTestimonialTitle` text NOT NULL,
  `homeDataTestimonialSubTitle` text NOT NULL,
  `homepage_video` varchar(100) NOT NULL,
  `facebook_script` text NOT NULL,
  `home_fourth_section_image_main` text NOT NULL,
  `home_fourth_section_image` text NOT NULL,
  `home_fourth_section_title` text NOT NULL,
  `home_fourth_section_text` text NOT NULL,
  `home_fourth_section_btnText` text NOT NULL,
  `home_fourth_section_href` text NOT NULL,
  `home_contact_main_text` text NOT NULL,
  `home_contact_small_text` text NOT NULL,
  `home_contact_btnText` text NOT NULL,
  `home_contact_btnLink` text NOT NULL,
  `home_contact_subscribeNewsletterText` text NOT NULL,
  `home_contact_emailPlaceholder` text NOT NULL,
  `home_contact_btnSubcribeText` text NOT NULL,
  `home_video_play_icon` text NOT NULL,
  `home_video_link` text NOT NULL,
  `home_video_title` text NOT NULL,
  `home_video_text` text NOT NULL,
  `aboutTitle` text NOT NULL,
  `aboutDescription` text NOT NULL,
  `aboutDetailofText` text NOT NULL,
  `aboutVideoLink` text NOT NULL,
  `contact_address` text NOT NULL,
  `contact_phone1` text NOT NULL,
  `contact_phone2` text NOT NULL,
  `contact_mail1` text NOT NULL,
  `contact_mail2` text NOT NULL,
  `contact_mapLoc` text NOT NULL,
  `contact_formTitle` text NOT NULL,
  `contact_submitBtn` text NOT NULL,
  `contact_name_l` text NOT NULL,
  `contact_name_p` text NOT NULL,
  `contact_email_l` text NOT NULL,
  `contact_email_p` text NOT NULL,
  `contact_phone_l` text NOT NULL,
  `contact_phone_p` text NOT NULL,
  `contact_message_l` text NOT NULL,
  `contact_message_p` text NOT NULL,
  `donate_header` text NOT NULL,
  `donate_title` text NOT NULL,
  `donate_description` text NOT NULL,
  `homeDataModifiedUserId` int(11) NOT NULL,
  `homeDataModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_data`
--

INSERT INTO `home_data` (`homeDataId`, `firstSectionImage`, `firstSectionText`, `firstButtonText`, `firstButtonLink`, `whatWeDO`, `homeDataBlogBlogOne`, `homeDataBlogBlogTwo`, `homeDataBlogBlogThree`, `homeDataAboutTitle`, `homeDataAboutSubTitle`, `homeDataAboutDescription`, `homeDataAboutCategoryOne`, `homeDataAboutCategoryTwo`, `homeDataAboutCategoryThree`, `homeDataAboutCategoryFour`, `homeDataAboutTwoImage`, `homeDataAboutTwoTitleOne`, `homeDataAboutTwoTitleTwo`, `homeDataAboutTwoButtonText`, `homeDataAboutTwoButtonCategoryId`, `homeDataMediaTitle`, `homeDataMediaSubTitle`, `homeDataMediaDescription`, `homeDataMediaButtonText`, `homeDataMediaButtonCategoryId`, `homeDataInterestedTitle`, `homeDataInterestedCategoryOne`, `homeDataInterestedCategoryTwo`, `homeDataInterestedCategoryThree`, `homeDataTestimonialImage`, `homeDataTestimonialTitle`, `homeDataTestimonialSubTitle`, `homepage_video`, `facebook_script`, `home_fourth_section_image_main`, `home_fourth_section_image`, `home_fourth_section_title`, `home_fourth_section_text`, `home_fourth_section_btnText`, `home_fourth_section_href`, `home_contact_main_text`, `home_contact_small_text`, `home_contact_btnText`, `home_contact_btnLink`, `home_contact_subscribeNewsletterText`, `home_contact_emailPlaceholder`, `home_contact_btnSubcribeText`, `home_video_play_icon`, `home_video_link`, `home_video_title`, `home_video_text`, `aboutTitle`, `aboutDescription`, `aboutDetailofText`, `aboutVideoLink`, `contact_address`, `contact_phone1`, `contact_phone2`, `contact_mail1`, `contact_mail2`, `contact_mapLoc`, `contact_formTitle`, `contact_submitBtn`, `contact_name_l`, `contact_name_p`, `contact_email_l`, `contact_email_p`, `contact_phone_l`, `contact_phone_p`, `contact_message_l`, `contact_message_p`, `donate_header`, `donate_title`, `donate_description`, `homeDataModifiedUserId`, `homeDataModifiedAt`) VALUES
(1, '1585304153.jpg', 'Lorem Ipsum Content Lorem Ipsum Content Lorem', 'Explore', 'services', 'What We Do', 3, 4, 5, '&lt;h1&gt;&lt;strong&gt;What is Uplifting Women?&lt;/strong&gt;&lt;/h1&gt;', 'We support people with disability of all ages and needs in their homes and the community with quality services', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&amp;#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', 174, 174, 174, 174, '1550167686.jpg', 'No limit community service’s free guide on the National Disability Insurance Scheme (NDIS) for your child', 'This easy to follow guide is based on No Limits&#039; experience with the NDIS and working with families and children.', 'Read More', 52, 'News &amp; Media', 'Read the latest news, blogs, statistics', 'Learn about disability from the personal experiences of people with disability, carers and the wider community.', 'READ MORE', 16, 'You Might Be Interested In', 52, 52, 52, '', 'Testimonials', 'What people say about us', '1554579479.mp4', '   <div class=\"fb-page\" data-href=\"https://www.facebook.com/nolimitscommunity/\" data-tabs=\"timeline\" data-small-header=\"false\" data-adapt-container-width=\"true\" data-hide-cover=\"false\" data-show-facepile=\"true\"><blockquote cite=\"https://www.facebook.com/nolimitscommunity/\" class=\"fb-xfbml-parse-ignore\"><a href=\"https://www.facebook.com/nolimitscommunity/\">No Limits Community Services - providing lifelong support . your way</a></blockquote></div>', '1585420486.jpg', '1585420486.png', 'History of The Chosen Ones', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 'Discover', 'services', 'God never said that the journey would be easy, but He did say that the arrival would be worthwhile', '- Max Lucado', 'Contact us', 'services', 'Signup to our newsletter &amp; Get all update', 'Enter E-mail', 'Subcribe', '1585470580.png', 'https://www.youtube.com/watch?v=d7jAiAYusUg', 'Best speech on religious life', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 'The Chosen Ones', '&lt;p&gt;There are many &lt;strong&gt;variations&lt;/strong&gt; of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&amp;#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&amp;#39;t anything embarrassing hidden in the middle of text.&lt;/p&gt;', 'Detail of The Chosen Ones', 'https://www.youtube.com/watch?v=d7jAiAYusUg', '431 Block B, California, USA', '+ 123 456 789', '+ 123 456 789', 'dummy@mail.com', 'dummy@mail.com', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13612.629611276596!2d74.296352!3d31.464856!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8644e395db3dd421!2sZEIKH%20Technologies!5e0!3m2!1sen!2s!4v1585725370070!5m2!1sen!2s', 'Contact Form', 'Submit', 'Name', 'Enter your name', 'Email', 'Enter your email', 'Phone', 'Enter your phone', 'Message', 'Enter your message', 'Donate', 'Donate', '&lt;p&gt;Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum&lt;/p&gt;\n\n&lt;div&gt;\n&lt;div class=&quot;specialBox withIcon&quot; title=&quot;&quot;&gt;&lt;em&gt;&amp;nbsp;&lt;/em&gt;\n\n&lt;p style=&quot;text-align:right&quot;&gt;&lt;a class=&quot;button primary-button rounded-button wow fadeInUp&quot; data-resource-category=&quot;&quot; href=&quot;/the_chosen_ones/public/assets/uploaded_media/1585733658.pdf&quot; target=&quot;_blank&quot;&gt;Download: Bank Statement&lt;/a&gt;&lt;span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;/div&gt;\n&lt;/div&gt;', 0, '2020-04-01 13:14:26');

-- --------------------------------------------------------

--
-- Table structure for table `home_services`
--

CREATE TABLE `home_services` (
  `homeServiceId` int(11) NOT NULL,
  `homeServiceTitle` text NOT NULL,
  `homeServiceText` text NOT NULL,
  `homeServiceImage` text NOT NULL,
  `homeServiceSortOrder` int(11) NOT NULL DEFAULT '0',
  `homeServiceIsActive` int(11) NOT NULL DEFAULT '1',
  `homeServiceIsDeleted` int(11) NOT NULL DEFAULT '0',
  `homeServiceCreatedUserId` int(11) NOT NULL DEFAULT '0',
  `homeServiceModifiedUserId` int(11) NOT NULL DEFAULT '0',
  `homeServiceCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `homeServiceModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_services`
--

INSERT INTO `home_services` (`homeServiceId`, `homeServiceTitle`, `homeServiceText`, `homeServiceImage`, `homeServiceSortOrder`, `homeServiceIsActive`, `homeServiceIsDeleted`, `homeServiceCreatedUserId`, `homeServiceModifiedUserId`, `homeServiceCreatedAt`, `homeServiceModified`) VALUES
(1, 'Service 1', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', '1585380051.png', 1, 1, 0, 0, 0, '2020-03-27 13:09:13', '2020-04-01 09:46:36'),
(2, 'Service 3', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', '1585380133.png', 3, 1, 0, 0, 0, '2020-03-27 14:52:56', '2020-03-28 16:32:29'),
(3, 'Service 2', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', '1585380168.png', 2, 1, 0, 0, 0, '2020-03-28 06:54:47', '2020-03-28 16:32:17'),
(4, 'Service 5', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', '1585413048.png', 5, 1, 0, 0, 0, '2020-03-28 16:30:48', '2020-03-29 09:24:47'),
(5, 'Service 4', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', '1585413114.png', 4, 1, 0, 0, 0, '2020-03-28 16:31:54', '2020-03-28 16:32:17');

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `loginId` int(11) NOT NULL,
  `loginUserId` int(11) NOT NULL,
  `loginSource` varchar(100) NOT NULL,
  `loginDevice` varchar(20) NOT NULL,
  `loginIsLogout` tinyint(4) NOT NULL,
  `loginCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `loginModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`loginId`, `loginUserId`, `loginSource`, `loginDevice`, `loginIsLogout`, `loginCreatedAt`, `loginModifiedAt`) VALUES
(59, 1, 'form', 'web', 0, '2020-01-22 01:44:49', '2020-01-22 01:44:49'),
(61, 1, 'form', 'web', 0, '2020-01-22 01:46:59', '2020-01-22 01:46:59'),
(62, 1, 'form', 'web', 0, '2020-01-22 22:19:12', '2020-01-22 22:19:12'),
(63, 8, '', 'app', 0, '2020-01-23 00:16:43', '2020-01-23 00:16:43'),
(64, 1, 'form', 'web', 0, '2020-01-23 03:40:30', '2020-01-23 03:40:30'),
(65, 1, 'form', 'web', 0, '2020-01-24 10:04:59', '2020-01-24 10:04:59'),
(66, 1, 'form', 'web', 0, '2020-01-24 21:53:20', '2020-01-24 21:53:20'),
(67, 1, 'form', 'web', 0, '2020-01-25 09:07:25', '2020-01-25 09:07:25'),
(68, 1, 'form', 'web', 0, '2020-01-25 21:25:34', '2020-01-25 21:25:34'),
(69, 1, 'form', 'web', 0, '2020-01-27 11:31:22', '2020-01-27 11:31:22'),
(70, 1, 'form', 'web', 0, '2020-01-27 16:08:21', '2020-01-27 16:08:21'),
(71, 1, 'form', 'web', 0, '2020-01-28 13:05:30', '2020-01-28 13:05:30'),
(72, 1, 'form', 'web', 0, '2020-01-28 13:35:00', '2020-01-28 13:35:00'),
(73, 1, 'form', 'web', 0, '2020-01-30 12:02:07', '2020-01-30 12:02:07'),
(74, 1, 'form', 'web', 0, '2020-01-30 18:17:51', '2020-01-30 18:17:51'),
(75, 1, 'form', 'web', 0, '2020-02-03 19:33:10', '2020-02-03 19:33:10'),
(76, 1, 'form', 'web', 0, '2020-02-04 12:19:15', '2020-02-04 12:19:15'),
(77, 1, 'form', 'web', 0, '2020-02-11 10:45:30', '2020-02-11 10:45:30'),
(78, 1, 'form', 'web', 0, '2020-02-11 18:05:20', '2020-02-11 18:05:20'),
(79, 1, 'form', 'web', 0, '2020-02-11 23:06:02', '2020-02-11 23:06:02'),
(80, 1, 'form', 'web', 0, '2020-02-12 15:40:59', '2020-02-12 15:40:59'),
(81, 1, 'form', 'web', 0, '2020-02-17 18:08:56', '2020-02-17 18:08:56'),
(82, 1, 'form', 'web', 0, '2020-02-17 21:26:53', '2020-02-17 21:26:53'),
(83, 1, 'form', 'web', 0, '2020-02-19 12:09:50', '2020-02-19 12:09:50'),
(84, 1, 'form', 'web', 0, '2020-03-04 10:34:18', '2020-03-04 10:34:18'),
(85, 1, 'form', 'web', 0, '2020-03-05 10:42:09', '2020-03-05 10:42:09'),
(86, 1, 'form', 'web', 0, '2020-03-09 07:32:12', '2020-03-09 07:32:12'),
(87, 1, 'form', 'web', 0, '2020-03-10 09:45:12', '2020-03-10 09:45:12'),
(88, 8, '', 'web', 0, '2020-03-10 12:55:19', '2020-03-10 12:55:19'),
(89, 1, '', 'web', 0, '2020-03-10 13:11:23', '2020-03-10 13:11:23'),
(90, 8, '', 'web', 0, '2020-03-10 13:26:56', '2020-03-10 13:26:56'),
(91, 29, '', 'web', 0, '2020-03-10 16:39:07', '2020-03-10 16:39:07'),
(92, 29, '', 'web', 0, '2020-03-10 16:39:21', '2020-03-10 16:39:21'),
(93, 8, '', 'web', 0, '2020-03-10 18:28:17', '2020-03-10 18:28:17'),
(94, 29, '', 'web', 0, '2020-03-10 19:08:46', '2020-03-10 19:08:46'),
(95, 8, '', 'web', 0, '2020-03-10 19:10:57', '2020-03-10 19:10:57'),
(96, 1, 'form', 'web', 0, '2020-03-10 19:11:26', '2020-03-10 19:11:26'),
(97, 1, 'form', 'web', 0, '2020-03-10 19:11:44', '2020-03-10 19:11:44'),
(98, 8, '', 'web', 0, '2020-03-10 19:23:26', '2020-03-10 19:23:26'),
(99, 8, '', 'web', 0, '2020-03-10 19:36:15', '2020-03-10 19:36:15'),
(100, 8, '', 'web', 0, '2020-03-10 20:37:33', '2020-03-10 20:37:33'),
(101, 8, '', 'web', 0, '2020-03-10 20:47:43', '2020-03-10 20:47:43'),
(102, 8, '', 'web', 0, '2020-03-10 21:11:02', '2020-03-10 21:11:02'),
(103, 8, '', 'web', 0, '2020-03-10 21:12:15', '2020-03-10 21:12:15'),
(104, 8, '', 'web', 0, '2020-03-10 21:14:16', '2020-03-10 21:14:16'),
(105, 8, '', 'web', 0, '2020-03-11 13:21:38', '2020-03-11 13:21:38'),
(106, 8, '', 'web', 0, '2020-03-11 16:40:22', '2020-03-11 16:40:22'),
(107, 8, '', 'web', 0, '2020-03-11 22:22:58', '2020-03-11 22:22:58'),
(108, 8, '', 'web', 0, '2020-03-12 17:50:19', '2020-03-12 17:50:19'),
(109, 1, 'form', 'web', 0, '2020-03-12 20:19:18', '2020-03-12 20:19:18'),
(110, 8, '', 'web', 0, '2020-03-13 13:06:51', '2020-03-13 13:06:51'),
(111, 8, '', 'web', 0, '2020-03-13 13:57:03', '2020-03-13 13:57:03'),
(112, 8, '', 'web', 0, '2020-03-15 22:39:47', '2020-03-15 22:39:47'),
(113, 8, '', 'web', 0, '2020-03-16 08:16:55', '2020-03-16 08:16:55'),
(114, 8, '', 'web', 0, '2020-03-17 05:24:42', '2020-03-17 05:24:42'),
(115, 8, '', 'web', 0, '2020-03-18 05:50:55', '2020-03-18 05:50:55'),
(116, 8, '', 'web', 0, '2020-03-18 12:15:20', '2020-03-18 12:15:20'),
(117, 1, 'form', 'web', 0, '2020-03-18 12:15:49', '2020-03-18 12:15:49'),
(118, 8, '', 'web', 0, '2020-03-18 13:39:24', '2020-03-18 13:39:24'),
(119, 1, 'form', 'web', 0, '2020-03-18 13:51:13', '2020-03-18 13:51:13'),
(120, 8, '', 'web', 0, '2020-03-18 13:52:12', '2020-03-18 13:52:12'),
(121, 1, 'form', 'web', 0, '2020-03-18 15:37:55', '2020-03-18 15:37:55'),
(122, 8, '', 'web', 0, '2020-03-18 17:42:47', '2020-03-18 17:42:47'),
(123, 1, 'form', 'web', 0, '2020-03-18 19:13:52', '2020-03-18 19:13:52'),
(124, 8, '', 'web', 0, '2020-03-20 07:37:38', '2020-03-20 07:37:38'),
(125, 8, '', 'web', 0, '2020-03-20 10:59:40', '2020-03-20 10:59:40'),
(126, 1, 'form', 'web', 0, '2020-03-20 11:52:19', '2020-03-20 11:52:19'),
(127, 8, '', 'web', 0, '2020-03-23 08:23:08', '2020-03-23 08:23:08'),
(128, 8, '', 'web', 0, '2020-03-23 08:26:13', '2020-03-23 08:26:13'),
(129, 1, 'form', 'web', 0, '2020-03-23 09:46:39', '2020-03-23 09:46:39'),
(130, 1, 'form', 'web', 0, '2020-03-27 07:07:57', '2020-03-27 07:07:57'),
(131, 1, 'form', 'web', 0, '2020-03-27 09:18:55', '2020-03-27 09:18:55'),
(132, 1, 'form', 'web', 0, '2020-03-27 11:40:52', '2020-03-27 11:40:52'),
(133, 1, 'form', 'web', 0, '2020-03-27 15:37:05', '2020-03-27 15:37:05'),
(134, 1, 'form', 'web', 0, '2020-03-28 05:57:16', '2020-03-28 05:57:16'),
(135, 1, 'form', 'web', 0, '2020-03-28 15:37:34', '2020-03-28 15:37:34'),
(136, 1, 'form', 'web', 0, '2020-03-29 06:33:43', '2020-03-29 06:33:43'),
(137, 1, 'form', 'web', 0, '2020-03-31 05:23:06', '2020-03-31 05:23:06'),
(138, 1, 'form', 'web', 0, '2020-04-01 05:36:35', '2020-04-01 05:36:35'),
(139, 1, 'form', 'web', 0, '2020-04-01 08:15:43', '2020-04-01 08:15:43'),
(140, 1, 'form', 'web', 0, '2020-04-01 11:20:19', '2020-04-01 11:20:19'),
(141, 1, 'form', 'web', 0, '2020-04-01 13:11:23', '2020-04-01 13:11:23'),
(142, 1, 'form', 'web', 0, '2020-04-01 16:49:01', '2020-04-01 16:49:01');

-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

CREATE TABLE `medias` (
  `mediaId` int(11) NOT NULL,
  `mediaName` text NOT NULL,
  `mediaHref` text NOT NULL,
  `mediaSortOrder` int(11) NOT NULL DEFAULT '0',
  `mediaType` tinyint(4) NOT NULL DEFAULT '1',
  `mediaIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `mediaCreatedUserId` int(11) NOT NULL,
  `mediaModifiedUserId` int(11) NOT NULL,
  `mediaCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mediaModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medias`
--

INSERT INTO `medias` (`mediaId`, `mediaName`, `mediaHref`, `mediaSortOrder`, `mediaType`, `mediaIsDeleted`, `mediaCreatedUserId`, `mediaModifiedUserId`, `mediaCreatedAt`, `mediaModifiedAt`) VALUES
(174, 'contacts', '1585733658.pdf', 0, 2, 0, 0, 0, '2020-04-01 09:34:18', '2020-04-01 09:34:18');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `pageId` int(11) NOT NULL,
  `pageCategoryId` int(11) NOT NULL,
  `pageTitle` varchar(255) NOT NULL,
  `pageContent` text NOT NULL,
  `pageIsLeftNav` tinyint(4) NOT NULL DEFAULT '0',
  `pageLeftNavType` tinyint(4) NOT NULL DEFAULT '1',
  `pageLeftNavContent` text NOT NULL,
  `pageIsRightNav` tinyint(4) NOT NULL DEFAULT '0',
  `pageRightNavType` tinyint(4) NOT NULL DEFAULT '1',
  `pageRightNavContent` text NOT NULL,
  `pagePageTitle` text NOT NULL,
  `pageSeoTitle` text NOT NULL,
  `pageSeoDescription` text NOT NULL,
  `pageSeoKeywords` text NOT NULL,
  `pageIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `pageIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `pageCreatedUserId` int(11) NOT NULL,
  `pageModifiedUserId` int(11) NOT NULL,
  `pageCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pageModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `page_sections`
--

CREATE TABLE `page_sections` (
  `pageSectionId` int(11) NOT NULL,
  `pageSectionTitle` varchar(255) NOT NULL,
  `pageSectionCategoryId` int(11) NOT NULL,
  `pageSectionSortOrder` int(11) NOT NULL DEFAULT '0',
  `pageSectionIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `pageSectionIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `pageSectionCreatedUserId` int(11) NOT NULL,
  `pageSectionModifiedUserId` int(11) NOT NULL,
  `pageSectionCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pageSectionModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_sections`
--

INSERT INTO `page_sections` (`pageSectionId`, `pageSectionTitle`, `pageSectionCategoryId`, `pageSectionSortOrder`, `pageSectionIsActive`, `pageSectionIsDeleted`, `pageSectionCreatedUserId`, `pageSectionModifiedUserId`, `pageSectionCreatedAt`, `pageSectionModifiedAt`) VALUES
(1, 'Sections1', 34, 0, 0, 1, 0, 0, '2019-02-13 14:51:12', '2019-02-13 14:54:07'),
(2, 'daasdasda', 34, 2, 0, 0, 0, 0, '2019-02-13 14:54:13', '2019-02-13 14:56:48'),
(3, 'sadasdasdasdsd554', 34, 1, 0, 1, 0, 0, '2019-02-13 14:54:17', '2019-02-13 16:16:16'),
(4, 'Support as a professional working with people with disability', 98, 0, 0, 1, 0, 0, '2019-02-15 07:56:09', '2019-02-15 08:13:18'),
(5, 'Support as a professional working with people with disability', 98, 0, 0, 0, 0, 0, '2019-02-15 08:13:55', '2019-02-15 08:13:55'),
(6, 'Support as a person with disability', 98, 0, 0, 0, 0, 0, '2019-02-15 08:16:26', '2019-02-15 08:16:26'),
(7, 'Support as a carer of people with disability', 98, 0, 0, 0, 0, 0, '2019-02-15 08:20:16', '2019-02-15 08:20:16'),
(8, 'You might be interested in', 98, 0, 0, 1, 0, 0, '2019-02-15 08:21:40', '2019-02-15 08:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `page_section_categories`
--

CREATE TABLE `page_section_categories` (
  `pageSectionCategoryId` int(11) NOT NULL,
  `pageSectionCategoryPageSectionId` int(11) NOT NULL,
  `pageSectionCategoryCategoryId` int(11) NOT NULL,
  `pageSectionCategorySortOrder` int(11) NOT NULL DEFAULT '0',
  `pageSectionCategoryIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `pageSectionCategoryIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `pageSectionCategoryCreatedUserId` int(11) NOT NULL,
  `pageSectionCategoryModifiedUserId` int(11) NOT NULL,
  `pageSectionCategoryCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pageSectionCategoryModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_section_categories`
--

INSERT INTO `page_section_categories` (`pageSectionCategoryId`, `pageSectionCategoryPageSectionId`, `pageSectionCategoryCategoryId`, `pageSectionCategorySortOrder`, `pageSectionCategoryIsActive`, `pageSectionCategoryIsDeleted`, `pageSectionCategoryCreatedUserId`, `pageSectionCategoryModifiedUserId`, `pageSectionCategoryCreatedAt`, `pageSectionCategoryModifiedAt`) VALUES
(1, 3, 5, 2, 0, 0, 0, 0, '2019-02-13 15:31:07', '2019-02-13 16:15:01'),
(2, 3, 6, 1, 0, 0, 0, 0, '2019-02-13 15:31:55', '2019-02-13 16:15:01'),
(3, 2, 5, 0, 0, 1, 0, 0, '2019-02-13 16:16:43', '2019-02-13 16:19:23'),
(4, 2, 5, 0, 0, 0, 0, 0, '2019-02-13 16:24:41', '2019-02-13 16:24:41'),
(5, 4, 86, 0, 0, 0, 0, 0, '2019-02-15 07:57:51', '2019-02-15 07:57:51'),
(6, 5, 142, 0, 0, 0, 0, 0, '2019-02-15 08:14:42', '2019-02-15 08:14:42'),
(7, 5, 141, 0, 0, 0, 0, 0, '2019-02-15 08:15:25', '2019-02-15 08:15:25'),
(8, 5, 86, 0, 0, 0, 0, 0, '2019-02-15 08:15:46', '2019-02-15 08:15:46'),
(9, 6, 41, 0, 0, 0, 0, 0, '2019-02-15 08:16:46', '2019-02-15 08:16:46'),
(10, 6, 45, 0, 0, 0, 0, 0, '2019-02-15 08:17:48', '2019-02-15 08:17:48'),
(11, 6, 48, 0, 0, 0, 0, 0, '2019-02-15 08:17:58', '2019-02-15 08:17:58'),
(12, 6, 38, 0, 0, 0, 0, 0, '2019-02-15 08:18:33', '2019-02-15 08:18:33'),
(13, 6, 42, 0, 0, 0, 0, 0, '2019-02-15 08:18:42', '2019-02-15 08:18:42'),
(14, 6, 46, 0, 0, 0, 0, 0, '2019-02-15 08:18:54', '2019-02-15 08:18:54'),
(15, 6, 49, 0, 0, 0, 0, 0, '2019-02-15 08:19:05', '2019-02-15 08:19:05'),
(16, 6, 39, 0, 0, 0, 0, 0, '2019-02-15 08:19:13', '2019-02-15 08:19:13'),
(17, 6, 40, 0, 0, 0, 0, 0, '2019-02-15 08:19:53', '2019-02-15 08:19:53'),
(18, 6, 44, 0, 0, 0, 0, 0, '2019-02-15 08:19:59', '2019-02-15 08:19:59'),
(19, 7, 43, 0, 0, 0, 0, 0, '2019-02-15 08:20:52', '2019-02-15 08:20:52'),
(20, 7, 143, 0, 0, 0, 0, 0, '2019-02-15 08:21:07', '2019-02-15 08:21:07'),
(21, 7, 47, 0, 0, 0, 0, 0, '2019-02-15 08:21:21', '2019-02-15 08:21:21'),
(22, 8, 129, 0, 0, 1, 0, 0, '2019-02-15 08:21:53', '2019-02-15 08:21:58'),
(23, 8, 52, 0, 0, 1, 0, 0, '2019-02-15 08:22:00', '2019-02-15 08:22:07'),
(24, 8, 136, 0, 0, 0, 0, 0, '2019-02-15 08:22:20', '2019-02-15 08:22:20'),
(25, 8, 108, 0, 0, 0, 0, 0, '2019-02-15 08:22:31', '2019-02-15 08:22:31');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `subscriberId` int(11) NOT NULL,
  `subscriberEmail` varchar(255) NOT NULL,
  `subscriberName` varchar(255) NOT NULL,
  `subscriberPostcode` varchar(255) NOT NULL,
  `subscriberIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `subscriberIsFake` tinyint(4) NOT NULL DEFAULT '0',
  `subscriberCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subscriberModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`subscriberId`, `subscriberEmail`, `subscriberName`, `subscriberPostcode`, `subscriberIsDeleted`, `subscriberIsFake`, `subscriberCreatedAt`, `subscriberModifiedAt`) VALUES
(1, 'shajeelriaz65@gmail.com', 'shajeel', '54000', 0, 0, '2019-03-21 17:34:33', '2019-03-21 17:34:33'),
(2, 'shajeelriaz@gmail.com', 'shajeel', '45000', 0, 0, '2019-03-21 17:39:47', '2019-03-21 17:39:47'),
(3, 'test@gmail.com', '', '', 0, 0, '2020-03-31 13:25:10', '2020-03-31 13:25:10'),
(4, 'test1@gmail.com', '', '', 0, 0, '2020-04-01 08:45:01', '2020-04-01 08:45:01');

-- --------------------------------------------------------

--
-- Table structure for table `thirdsection`
--

CREATE TABLE `thirdsection` (
  `testimonialId` int(11) NOT NULL,
  `testimonialName` text NOT NULL,
  `testimonialImage` text NOT NULL,
  `testimonialComment` text NOT NULL,
  `testimonialSortOrder` int(11) NOT NULL DEFAULT '0',
  `testimonialIsActive` tinyint(4) NOT NULL DEFAULT '0',
  `testimonialCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `testimonialModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `testimonialIsDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `testimonialOccupation` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `thirdsection`
--

INSERT INTO `thirdsection` (`testimonialId`, `testimonialName`, `testimonialImage`, `testimonialComment`, `testimonialSortOrder`, `testimonialIsActive`, `testimonialCreatedAt`, `testimonialModifiedAt`, `testimonialIsDeleted`, `testimonialOccupation`) VALUES
(1, 'Lorem Ipsum', '1583406792.png', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 4, 1, '2020-03-05 11:13:12', '2020-03-05 11:15:25', 0, '#'),
(2, 'Lorem Ipsum', '1583406813.png', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 1, 1, '2020-03-05 11:13:33', '2020-03-05 11:15:48', 0, '#'),
(3, 'Lorem Ipsum', '1583406847.png', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 2, 1, '2020-03-05 11:14:07', '2020-03-05 11:15:24', 0, '#'),
(4, 'Lorem Ipsum', '1583406869.png', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 5, 1, '2020-03-05 11:14:29', '2020-03-05 11:15:25', 0, '#'),
(5, 'Lorem Ipsum', '1583406909.png', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum', 3, 1, '2020-03-05 11:15:09', '2020-03-05 11:15:25', 0, '#');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '0',
  `image` text NOT NULL,
  `typeId` tinyint(4) NOT NULL DEFAULT '3',
  `isActive` tinyint(4) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `resetToken` varchar(255) NOT NULL,
  `activationToken` text NOT NULL,
  `createdUserId` int(11) NOT NULL,
  `fcm-tokenid` text NOT NULL,
  `modifiedUserId` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `email`, `password`, `firstName`, `lastName`, `phone`, `gender`, `image`, `typeId`, `isActive`, `isDeleted`, `resetToken`, `activationToken`, `createdUserId`, `fcm-tokenid`, `modifiedUserId`, `createdAt`, `modifiedAt`) VALUES
(1, 'admin@gmail.com', 'fdcd9fc08a3e3aa80221f26f704c8563ac358f4edfe0a4ca1973b30a5e2adbbe371f748583a57f83aad51bd2bd290896a78ae17d19ffffceba92c7b37de9a3a3--1tUEBCyaiKakKWeSRycgQcxuhdmDKpAwpQivZ9po3Vc--2', '', '', '', 0, '', 1, 1, 0, '', '', 0, '', 0, '2019-01-24 13:13:22', '2020-02-17 23:01:49'),
(8, 'nabeelzafar.hsra@gmail.com', 'd5eabfae3b9f7c0aef723321d06635d5f6b7a5867b87f6c1c88b432ad17ce5fd2adf15d69baf87871febaa83e4663ed544d48f3c0850cfb21cd1118354ba5fa79ZKQT3iN--3ity6fvaiM--18lOZj2RjE76--1lfWQmbuQjAuA--2', 'Nabeel', 'Zafar', '', 0, '', 3, 1, 0, '', 'dbdda1dbef7b9f64ab8f8c89e629169dfdaafb006b0e6b6357ebee1b3cad86b8b7539a478bdfaa4e96b4cb4f88ac37e493dbc9cb8cc636efba224812888387dfefT7--1kFP3KlaDIroxUhOeEn4IlnDQoNt3P1ThJBwFm4--2', 0, '', 0, '2020-01-22 00:59:42', '2020-02-17 23:01:45'),
(29, 'nabeelzafar@zeikh.com', '899c1d4007a7b1f2b7e974386f2193731ebdc1daf494fae2d6a0e1cc19a4db99022d4bf080f7805b8eb05f0da72f42b7ebc779c3a2a35146ee2afd5d38fe000eugF6cpEJejHsmfAvgd--1omjT2h0NtruLSGo--3VhheZdvY--2', 'Nabeel', 'Zafar', '', 0, '', 3, 1, 0, '', 'ff3d5b7deb8313adfa9243001bdd6a036d73adb736bd49758c3f9bfee52fde2735cbbe09b799407a57a0dd34e2b738e0824d6bffe855c726a829a6f4e9c9493cB295GjZeyn83G4iZ5OAp63BoN8aGgg--3ZsrW1s4ndAbk--2', 0, '', 0, '2020-03-10 16:38:54', '2020-03-10 19:08:23');

-- --------------------------------------------------------

--
-- Table structure for table `vacancies`
--

CREATE TABLE `vacancies` (
  `vacancyId` int(11) NOT NULL,
  `vacancyName` varchar(255) NOT NULL,
  `vacancyExp` varchar(255) NOT NULL,
  `vacancyQual` varchar(255) NOT NULL,
  `vacancyApplyEmail` varchar(255) NOT NULL,
  `vacancyArea` varchar(255) NOT NULL,
  `vacancySkill1` varchar(255) NOT NULL,
  `vacancySkill2` varchar(255) NOT NULL,
  `vacancySkill3` varchar(255) NOT NULL,
  `vacancySkill4` varchar(255) NOT NULL,
  `vacancySkill5` varchar(255) NOT NULL,
  `vacancyIsActive` int(11) NOT NULL,
  `vacancyCreatedUserId` int(11) NOT NULL,
  `vacancyIsDeleted` int(11) NOT NULL,
  `vacancyOrderId` int(11) NOT NULL,
  `vacancyCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vacancyModifiedUserId` int(11) NOT NULL,
  `vacancyModifiedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacancies`
--

INSERT INTO `vacancies` (`vacancyId`, `vacancyName`, `vacancyExp`, `vacancyQual`, `vacancyApplyEmail`, `vacancyArea`, `vacancySkill1`, `vacancySkill2`, `vacancySkill3`, `vacancySkill4`, `vacancySkill5`, `vacancyIsActive`, `vacancyCreatedUserId`, `vacancyIsDeleted`, `vacancyOrderId`, `vacancyCreatedAt`, `vacancyModifiedUserId`, `vacancyModifiedAt`) VALUES
(1, 'mel', '2 years', 'BS', 'test@test.com', 'melbourne', 'skill 1', 'skill 2', 'skill 3', 'skill 4', 'skill 5', 1, 0, 0, 1, '2019-03-21 05:49:35', 0, '2019-03-21 05:49:35'),
(2, 'darwin', 'exp', 'qual', 'abc@xyz.com', 'darwin', '', '', '', '', '', 1, 0, 0, 2, '2019-03-21 08:29:26', 0, '2019-03-21 08:29:26'),
(3, 'tiwi', 'exp', 'MS', 'abc@xyz.com', 'tiwiIsland', '', '', '', '', '', 1, 0, 0, 0, '2019-03-21 09:58:31', 0, '2019-03-21 09:58:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_counters`
--
ALTER TABLE `about_counters`
  ADD PRIMARY KEY (`counterId`);

--
-- Indexes for table `career_page`
--
ALTER TABLE `career_page`
  ADD PRIMARY KEY (`careerPageId`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `category_pages`
--
ALTER TABLE `category_pages`
  ADD PRIMARY KEY (`categoryPageId`);

--
-- Indexes for table `dashboard`
--
ALTER TABLE `dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `directors`
--
ALTER TABLE `directors`
  ADD PRIMARY KEY (`directorId`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`featureId`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`fileId`);

--
-- Indexes for table `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`footerId`);

--
-- Indexes for table `footer_menus`
--
ALTER TABLE `footer_menus`
  ADD PRIMARY KEY (`footerMenuId`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`galleryId`);

--
-- Indexes for table `gratitude_journal`
--
ALTER TABLE `gratitude_journal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `header`
--
ALTER TABLE `header`
  ADD PRIMARY KEY (`headerId`);

--
-- Indexes for table `header_menus`
--
ALTER TABLE `header_menus`
  ADD PRIMARY KEY (`footerMenuId`);

--
-- Indexes for table `home_data`
--
ALTER TABLE `home_data`
  ADD PRIMARY KEY (`homeDataId`);

--
-- Indexes for table `home_services`
--
ALTER TABLE `home_services`
  ADD PRIMARY KEY (`homeServiceId`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`loginId`);

--
-- Indexes for table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`mediaId`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pageId`);

--
-- Indexes for table `page_sections`
--
ALTER TABLE `page_sections`
  ADD PRIMARY KEY (`pageSectionId`);

--
-- Indexes for table `page_section_categories`
--
ALTER TABLE `page_section_categories`
  ADD PRIMARY KEY (`pageSectionCategoryId`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`subscriberId`);

--
-- Indexes for table `thirdsection`
--
ALTER TABLE `thirdsection`
  ADD PRIMARY KEY (`testimonialId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `vacancies`
--
ALTER TABLE `vacancies`
  ADD PRIMARY KEY (`vacancyId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_counters`
--
ALTER TABLE `about_counters`
  MODIFY `counterId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `career_page`
--
ALTER TABLE `career_page`
  MODIFY `careerPageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT for table `category_pages`
--
ALTER TABLE `category_pages`
  MODIFY `categoryPageId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dashboard`
--
ALTER TABLE `dashboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `directors`
--
ALTER TABLE `directors`
  MODIFY `directorId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `featureId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `fileId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `footer`
--
ALTER TABLE `footer`
  MODIFY `footerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `footer_menus`
--
ALTER TABLE `footer_menus`
  MODIFY `footerMenuId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `galleryId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gratitude_journal`
--
ALTER TABLE `gratitude_journal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `header`
--
ALTER TABLE `header`
  MODIFY `headerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `header_menus`
--
ALTER TABLE `header_menus`
  MODIFY `footerMenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `home_data`
--
ALTER TABLE `home_data`
  MODIFY `homeDataId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_services`
--
ALTER TABLE `home_services`
  MODIFY `homeServiceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `loginId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `medias`
--
ALTER TABLE `medias`
  MODIFY `mediaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `pageId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page_sections`
--
ALTER TABLE `page_sections`
  MODIFY `pageSectionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `page_section_categories`
--
ALTER TABLE `page_section_categories`
  MODIFY `pageSectionCategoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `subscriberId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `thirdsection`
--
ALTER TABLE `thirdsection`
  MODIFY `testimonialId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `vacancies`
--
ALTER TABLE `vacancies`
  MODIFY `vacancyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
